﻿using ZztRemake.Models;

namespace ZztRemake;

public class Editor
{
    private readonly ZztData _data;
    private readonly TextWindow _textWindow;

    public Editor(ZztData data, TextWindow textWindow)
    {
        _data = data;
        _textWindow = textWindow;
    }

    public void HighScoresLoad()
    {
        try
        {
            using var stream = new FileStream($"{_data.World.Info.Name.Text}.HI", FileMode.Open);
            using var reader = new BinaryReader(stream);
            for (int index = 0; index < Constants.HighScoreCount; index++)
            {
                _data.HighScoreList[index].Name.TextLength = reader.ReadByte();
                _data.HighScoreList[index].Name.Bytes = reader.ReadBytes(50);
                _data.HighScoreList[index].Score = reader.ReadInt16();
            }

            reader.Close();
            stream.Close();
        }
        catch
        {
            for (var index = 0; index < Constants.HighScoreCount; index++)
            {
                _data.HighScoreList[index].Name.TextLength = 0;
                _data.HighScoreList[index].Name.Bytes = new byte[] {};
                _data.HighScoreList[index].Score = -1;
            }
        }
    }

    public void HighScoresSave()
    {
        try
        {
            using var stream = new FileStream($"{_data.World.Info.Name.Text}.HI", FileMode.Create);
            using var writer = new BinaryWriter(stream);
            for (var index = 0; index < Constants.HighScoreCount; index++)
            {
                writer.Write(_data.HighScoreList[index].Name.TextLength);
                writer.Write(_data.HighScoreList[index].Name.Bytes);
                writer.Write(Enumerable.Repeat((byte)0, 50 - _data.HighScoreList[index].Name.Bytes.Length).ToArray());
                writer.Write(_data.HighScoreList[index].Score);
            }

            writer.Close();
            stream.Close();

        }
        catch
        {
        }
    }

    public void HighScoresDisplay(short linePosition)
    {
        var textWindowState = new TextWindowState
        {
            LinePos = linePosition
        };
        HighScoresInitTextWindow(textWindowState);
        if (textWindowState.Lines.Count > 2)
        {
            textWindowState.Title = $"High scores for {_data.World.Info.Name.Text}";
            _textWindow.DrawOpen(textWindowState);
            _textWindow.Select(textWindowState, false, true);
            _textWindow.DrawClose();

        }
        _textWindow.Free(textWindowState);
    }

    private void HighScoresInitTextWindow(TextWindowState state)
    {
        _textWindow.InitializeState(state);
        _textWindow.Append(state, "Score  Name");
        _textWindow.Append(state, "-----  ----------------------------------");
        _data.HighScoreList
            .Where(hs => hs.Name.TextLength > 0)
            .Select(hs => new
            {
                Name = hs.Name.Text
                , ScoreText = $"     {hs.Score}"[^5..]
            })
            .Select(hs => $"{hs.ScoreText}  {hs.Name}").ToList()
            .ForEach(scoreLine => _textWindow.Append(state, scoreLine));
    }
}
