﻿using System.Text;
using ZztRemake.Models;

namespace ZztRemake;

public class Oop
{
    private readonly ZztData _data;
    private readonly TextWindow _textWindow;
    private readonly Random _random;
    private readonly Game _game;
    private readonly Sounds _sounds;

    public Oop(ZztData data, TextWindow textWindow, Random random, Game game, Sounds sounds)
    {
        _data = data;
        _textWindow = textWindow;
        _random = random;
        _game = game;
        _sounds = sounds;
    }

    public bool Send(short statId, string sendLabel, bool ignoreLock)
    {
        var returnValue = false;
        var respectSelfLock = false;
        if (statId < 0)
        {
            statId = (short)-statId;
            respectSelfLock = true;
        }

        var iStat = (short)0;
        var iCodePosition = (short)0;
        while (FindLabel(statId, sendLabel, ref iStat, ref iCodePosition, "\r:"))
        {
            if (_data.Board.StatusElements[iStat].Parameters[1] == 0 || (statId == iStat && false == respectSelfLock))
            {
                if (iStat == statId)
                {
                    returnValue = true;
                }
                _data.Board.StatusElements[iStat].CodePosition = iCodePosition;
            }
        }

        return returnValue;
    }

    private void ReadChar(short statId, ref short position)
    {
        var stat = _data.Board.StatusElements[statId];
        if (position >= 0 && position < stat.Code.Length)
        {
            _data.OopChar = stat.Code[position++];
        }
        else
        {
            _data.OopChar = '\0';
        }
    }

    private void ReadWord(short statId, ref short position)
    {
        _data.OopWord = "";
        do
        {
            ReadChar(statId, ref position);
        } while (_data.OopChar == ' ');
        _data.OopChar = char.ToUpperInvariant(_data.OopChar);
        if (_data.OopChar is < '0' or > '9')
        {
            while ((_data.OopChar is (>= 'A' and <= 'Z') or ':' or (>= '0' and <= '9') or '_'))
            {
                _data.OopWord += _data.OopChar;
                ReadChar(statId, ref position);
                _data.OopChar = char.ToUpperInvariant(_data.OopChar);
            }
        }

        if (position > 0)
        {
            position--;
        }
    }

    private void ReadValue(short statId, ref short position)
    {
        var builder = new StringBuilder("");
        do
        {
            ReadChar(statId, ref position);
        } while (_data.OopChar == ' ');

        _data.OopChar = char.ToUpperInvariant(_data.OopChar);
        while (_data.OopChar is >= '0' and <= '9')
        {
            builder.Append(_data.OopChar);
            ReadChar(statId, ref position);
            _data.OopChar = char.ToUpperInvariant(_data.OopChar);
        }

        if (position > 0)
        {
            position--;
        }

        if (builder.Length > 0)
        {
            if (short.TryParse(builder.ToString(), out var value))
            {
                _data.OopValue = value;
            }
            else
            {
                _data.OopValue = -1;
            }
        }
        else
        {
            _data.OopValue = -1;
        }
    }

    private short FindString(short statId, string s)
    {
        var stat = _data.Board.StatusElements[statId];
        var pos = (short)0;
        while (pos < stat.Code.Length)
        {
            var wordPos = (short)0;
            var cmpPos = pos;
            do
            {
                ReadChar(statId, ref cmpPos);
                if (char.ToUpperInvariant(s[wordPos]) != char.ToUpperInvariant(_data.OopChar))
                {
                    //TODO: rework goto!
                    goto NoMatch;
                }

                wordPos++;
            } while (wordPos < s.Length);

            ReadChar(statId, ref cmpPos);
            _data.OopChar = char.ToUpperInvariant(_data.OopChar);
            if ((_data.OopChar is < 'A' or > 'Z') && _data.OopChar != '_')
            {
                return pos;
            }

            NoMatch:
            pos++;
        }

        return -1;
    }

    private bool FindLabel(short statId, string sendLabel, ref short iStat, ref short iDataPos, string labelPrefix)
    {
        var foundStat = false;
        var objectMessage = "";
        var targetLookup = "";
        var targetSplitPos = sendLabel.IndexOf(":", StringComparison.Ordinal);
        if (targetSplitPos == -1)
        {
            if (iStat < statId)
            {
                objectMessage = sendLabel;
                iStat = statId;
                targetSplitPos = 0;
                foundStat = true;
            }
            else
            {
                return false;
            }
        }
        else
        {
            targetLookup = sendLabel[..targetSplitPos];
            objectMessage = sendLabel[(targetSplitPos + 1)..];
            foundStat = IterateStat(statId, ref iStat, targetLookup);
        }

        FindNextStat:
        if (foundStat)
        {
            if (objectMessage == "RESTART")
            {
                iDataPos = 0;
            }
            else
            {
                iDataPos = FindString(iStat, $"{labelPrefix}{objectMessage}");
                if (iDataPos < 0 && targetSplitPos > 0)
                {
                    foundStat = IterateStat(statId, ref iStat, targetLookup);
                    //TODO: rework goto!
                    goto FindNextStat;
                }
            }

            foundStat = iDataPos >= 0;
        }

        return foundStat;
    }

    private bool IterateStat(short statId, ref short iStat, string lookup)
    {
        iStat++;
        var found = false;

        switch (lookup)
        {
            case "ALL":
            {
                if (iStat < _data.Board.StatusElements.Count)
                {
                    found = true;
                }

                break;
            }
            case "OTHERS":
            {
                if (iStat < _data.Board.StatusElements.Count)
                {
                    if (iStat != statId)
                    {
                        found = true;
                    }
                    else
                    {
                        iStat++;
                        found = iStat < _data.Board.StatusElements.Count;
                    }
                }

                break;
            }
            case "SELF":
            {
                if (iStat > 0 && iStat <= statId)
                {
                    iStat = statId;
                    found = true;
                }

                break;
            }
            default:
            {
                while (iStat < _data.Board.StatusElements.Count && false == found)
                {
                    if (_data.Board.StatusElements[iStat].Code.Length > 0)
                    {
                        var pos = (short)0;
                        ReadChar(iStat, ref pos);
                        if (_data.OopChar == '@')
                        {
                            ReadWord(iStat, ref pos);
                            if (_data.OopWord == lookup)
                            {
                                found = true;
                            }
                        }
                    }

                    if (false == found)
                    {
                        iStat++;
                    }
                }

                break;
            }
        }

        return found;
    }

    public void Execute(short statId, ref short position, string name)
    {
        var stat = _data.Board.StatusElements[statId];
        var textWindowState = new TextWindowState();
        StartParsing:
        _textWindow.InitializeState(textWindowState);
        textWindowState.Selectable = false;
        var stopRunning = false;
        var repeatInsNextTick = false;
        var replaceStat = false;
        var replaceTile = new Tile();
        var endOfProgram = false;
        var insCount = (short)0;
        var lastPosition = (short)0;
        do
        {
            ReadInstruction:
            var lineFinished = true;
            lastPosition = position;
            ReadChar(statId, ref position);

            while (_data.OopChar == ':')
            {
                do
                {
                    ReadChar(statId, ref position);
                } while (_data.OopChar != '\0' && _data.OopChar != '\r');
                ReadChar(statId, ref position);
            }

            if (_data.OopChar == '\'')
            {
                SkipLine(statId, ref position);
            }
            else if (_data.OopChar == '@')
            {
                SkipLine(statId, ref position);
            }
            else if (_data.OopChar is '/' or '?')
            {
                if (_data.OopChar == '/')
                {
                    repeatInsNextTick = true;
                }

                ReadWord(statId, ref position);
                if (ParseDirection(statId, ref position, out var deltaX, out var deltaY))
                {
                    if (deltaX != 0 || deltaY != 0)
                    {
                        if (false == _data.ElementDefinitions[_data.Board.Tiles[stat.Location.X + stat.Step.X, stat.Location.Y + stat.Step.Y].Element].Walkable)
                        {
                            _game.PushablePush((short)(stat.Location.X + deltaX), (short)(stat.Location.Y + deltaY), deltaX, deltaY);
                        }

                        if (_data.ElementDefinitions[_data.Board.Tiles[(short)(stat.Location.X + deltaX), (short)(stat.Location.Y + deltaY)].Element].Walkable)
                        {
                            _game.MoveStat(statId, (short)(stat.Location.X + deltaX), (short)(stat.Location.Y + deltaY));
                            repeatInsNextTick = false;
                        }
                    }
                    else
                    {
                        repeatInsNextTick = false;
                    }

                    ReadChar(statId, ref position);
                    if (_data.OopChar != '\r')
                    {
                        position--;
                    }

                    stopRunning = true;
                }
                else
                {
                    Error(statId, "Bad direction");
                }
            }
            else if (_data.OopChar == '#')
            {
                ReadCommand:
                ReadWord(statId, ref position);
                if (_data.OopWord == "THEN")
                {
                    ReadWord(statId, ref position);
                }

                if (_data.OopWord.Length == 0)
                {
                    goto ReadInstruction;
                }
                insCount++;
                if (_data.OopWord.Length > 0)
                {
                    if (_data.OopWord == "GO")
                    {
                        ReadDirection(statId, ref position, out var deltaX, out var deltaY);

                        if (false == _data.ElementDefinitions[_data.Board.Tiles[stat.Location.X + deltaX, stat.Location.Y + deltaY].Element].Walkable)
                        {
                            _game.PushablePush((short)(stat.Location.X + deltaX), (short)(stat.Location.Y + deltaY), deltaX, deltaY);
                        }

                        if (_data.ElementDefinitions[_data.Board.Tiles[stat.Location.X + deltaX, stat.Location.Y + deltaY].Element].Walkable)
                        {
                            _game.MoveStat(statId, (short)(stat.Location.X + deltaX), (short)(stat.Location.Y + deltaY));
                        }
                        else
                        {
                            repeatInsNextTick = true;
                        }

                        stopRunning = true;
                    }
                    else if (_data.OopWord == "TRY")
                    {
                        ReadDirection(statId, ref position, out var deltaX, out var deltaY);

                        if (false == _data.ElementDefinitions[_data.Board.Tiles[stat.Location.X + deltaX, stat.Location.Y + deltaY].Element].Walkable)
                        {
                            _game.PushablePush((short)(stat.Location.X + deltaX), (short)(stat.Location.Y + deltaY), deltaX, deltaY);
                        }

                        if (_data.ElementDefinitions[_data.Board.Tiles[stat.Location.X + deltaX, stat.Location.Y + deltaY].Element].Walkable)
                        {
                            _game.MoveStat(statId, (short)(stat.Location.X + deltaX), (short)(stat.Location.Y + deltaY));
                            stopRunning = true;
                        }
                        else
                        {
                            goto ReadCommand;
                        }
                    }
                    else if (_data.OopWord == "WALK")
                    {
                        ReadDirection(statId, ref position, out var deltaX, out var deltaY);
                        stat.Step.X = deltaX;
                        stat.Step.Y = deltaY;
                    }
                    else if (_data.OopWord == "SET")
                    {
                        ReadWord(statId, ref position);
                        WorldSetFlag(_data.OopWord);
                    }
                    else if (_data.OopWord == "CLEAR")
                    {
                        ReadWord(statId, ref position);
                        WorldClearFlag(_data.OopWord);
                    }
                    else if (_data.OopWord == "IF")
                    {
                        ReadWord(statId, ref position);
                        if (CheckCondition(statId, ref position))
                        {
                            goto ReadCommand;
                        }
                    }
                    else if (_data.OopWord == "SHOOT")
                    {
                        ReadDirection(statId, ref position, out var deltaX, out var deltaY);
                        if (_game.BoardShoot(ElementIds.Bullet, stat.Location.X, stat.Location.Y, deltaX, deltaY, Constants.ShotSourceEnemy))
                        {
                            _sounds.Queue(2, 48, 1, 38, 1);
                        }
                        stopRunning = true;
                    }
                    else if (_data.OopWord == "THROWSTAR")
                    {
                        ReadDirection(statId, ref position, out var deltaX, out var deltaY);
                        _game.BoardShoot(ElementIds.Star, stat.Location.X, stat.Location.Y, deltaX, deltaY, Constants.ShotSourceEnemy);
                        stopRunning = true;
                    }
                    else if (_data.OopWord is "GIVE" or "TAKE")
                    {
                        var counterSubtract = _data.OopWord == "TAKE";

                        ReadWord(statId, ref position);
                        var addOopValueIfResultIsZeroOrMore = (ref short value) =>
                        {
                            if (value + _data.OopValue >= 0)
                            {
                                value += _data.OopValue;
                                return true;
                            }
                            return false;
                        };
                        Func<bool>? addOopValueToGameValueIfResultIsZeroOfMore;

                        if (_data.OopWord == "HEALTH")
                        {
                            addOopValueToGameValueIfResultIsZeroOfMore = () => addOopValueIfResultIsZeroOrMore(ref _data.World.Info.Health);
                        } else if (_data.OopWord == "AMMO")
                        {
                            addOopValueToGameValueIfResultIsZeroOfMore = () => addOopValueIfResultIsZeroOrMore(ref _data.World.Info.Ammo);
                        } else if (_data.OopWord == "GEMS")
                        {
                            addOopValueToGameValueIfResultIsZeroOfMore = () => addOopValueIfResultIsZeroOrMore(ref _data.World.Info.Gems);
                        } else if (_data.OopWord == "TORCHES")
                        {
                            addOopValueToGameValueIfResultIsZeroOfMore = () => addOopValueIfResultIsZeroOrMore(ref _data.World.Info.Torches);
                        } else if (_data.OopWord == "SCORE")
                        {
                            addOopValueToGameValueIfResultIsZeroOfMore = () => addOopValueIfResultIsZeroOrMore(ref _data.World.Info.Score);
                        } else if (_data.OopWord == "TIME")
                        {
                            addOopValueToGameValueIfResultIsZeroOfMore = () => addOopValueIfResultIsZeroOrMore(ref _data.World.Info.TimePassedInSeconds);
                        }
                        else
                        {
                            addOopValueToGameValueIfResultIsZeroOfMore = null;
                        }

                        if (addOopValueToGameValueIfResultIsZeroOfMore is not null)
                        {
                            ReadValue(statId, ref position);
                            if (_data.OopValue > 0)
                            {
                                if (counterSubtract)
                                {
                                    _data.OopValue = (short)-_data.OopValue;
                                }

                                if (false == addOopValueToGameValueIfResultIsZeroOfMore())
                                {
                                    goto ReadCommand;
                                }
                            }
                        }

                        _game.UpdateSidebar();
                    }
                    else if (_data.OopWord == "END")
                    {
                        position = -1;
                        _data.OopChar = '\0';
                    }
                    else if (_data.OopWord == "ENDGAME")
                    {
                        _data.World.Info.Health = 0;
                    }
                    else if (_data.OopWord == "IDLE")
                    {
                        stopRunning = true;
                    }
                    else if (_data.OopWord == "RESTART")
                    {
                        position = 0;
                        lineFinished = false;
                    }
                    else if (_data.OopWord == "ZAP")
                    {
                        ReadWord(statId, ref position);

                        var statusElementIdOfLabel = (short)0;
                        var codePositionOfLabel = (short)0;
                        while (FindLabel(statId, _data.OopWord, ref statusElementIdOfLabel, ref codePositionOfLabel, "\r:"))
                        {
                            _data.Board.StatusElements[statusElementIdOfLabel].Code =
                                new StringBuilder(_data.Board.StatusElements[statusElementIdOfLabel].Code)
                                {
                                    [codePositionOfLabel + 1] = '\''
                                }.ToString();
                        }
                    }
                    else if (_data.OopWord == "RESTORE")
                    {
                        ReadWord(statId, ref position);

                        var statusElementIdOfLabel = (short)0;
                        var codePositionOfLabel = (short)0;
                        while (FindLabel(statId, _data.OopWord, ref statusElementIdOfLabel, ref codePositionOfLabel, "\r'"))
                        {
                            do
                            {
                                _data.Board.StatusElements[statusElementIdOfLabel].Code =
                                    new StringBuilder(_data.Board.StatusElements[statusElementIdOfLabel].Code)
                                    {
                                        [codePositionOfLabel + 1] = ':'
                                    }.ToString();
                                codePositionOfLabel = FindString(statusElementIdOfLabel, $"\r'{_data.OopWord}\r");
                            } while (codePositionOfLabel >= 0);
                        }
                    }
                    else if (_data.OopWord == "LOCK")
                    {
                        stat.Parameters[1] = 1;
                    }
                    else if (_data.OopWord == "UNLOCK")
                    {
                        stat.Parameters[1] = 0;
                    }
                    else if (_data.OopWord == "SEND")
                    {
                        ReadWord(statId, ref position);
                        if (Send(statId, _data.OopWord, false))
                        {
                            lineFinished = false;
                        }
                    }
                    else if (_data.OopWord == "BECOME")
                    {
                        if (ParseTile(statId, ref position, out var argTile))
                        {
                            replaceStat = true;
                            replaceTile.Element = argTile.Element;
                            replaceTile.Color = argTile.Color;
                        }
                        else
                        {
                            Error(statId, "Bad #BECOME");
                        }
                    }
                    else if (_data.OopWord == "PUT")
                    {
                        ReadDirection(statId, ref position, out var deltaX, out var deltaY);
                        if (deltaX == 0 && deltaY == 0)
                        {
                            Error(statId, "Bad #PUT");
                        }
                        else if (false == ParseTile(statId, ref position, out var argTile))
                        {
                            Error(statId, "Bad #PUT");
                        }
                        else if (stat.Location.X + deltaX is > 0 and <= Constants.BoardWidth
                                 && stat.Location.Y + deltaY is > 0 and <= Constants.BoardHeight
                                )
                        {
                            if (false == _data.ElementDefinitions[_data.Board.Tiles[stat.Location.X + deltaX, stat.Location.Y + deltaY].Element].Walkable)
                            {
                                _game.PushablePush((short)(stat.Location.X + deltaX), (short)(stat.Location.Y + deltaY), deltaX, deltaY);
                            }
                            PlaceTile((short)(stat.Location.X + deltaX), (short)(stat.Location.Y + deltaY), ref argTile);
                        }
                    }
                    else if (_data.OopWord == "CHANGE")
                    {
                        if (false == ParseTile(statId, ref position, out var argTile))
                        {
                            Error(statId, "Bad #CHANGE");
                        }

                        if (false == ParseTile(statId, ref position, out var argTile2))
                        {
                            Error(statId, "Bad #CHANGE");
                        }

                        var ix = (short)0;
                        var iy = (short)1;
                        if (argTile2.Color == 0
                            && _data.ElementDefinitions[argTile2.Element].Color < Constants.ColorSpecialMinimum)
                        {
                            argTile2.Color = _data.ElementDefinitions[argTile2.Element].Color;
                        }

                        while (FindTileOnBoard(ref ix, ref iy, argTile))
                        {
                            PlaceTile(ix, iy, ref argTile2);
                        }
                    }
                    else if (_data.OopWord == "PLAY")
                    {
                        var textLine = _sounds.Parse(ReadLineToEnd(statId, ref position));
                        if (textLine.Length > 0)
                        {
                            _sounds.Queue(-1, textLine);
                        }
                        lineFinished = false;
                    }
                    else if (_data.OopWord == "CYCLE")
                    {

                        ReadValue(statId, ref position);
                        if (_data.OopValue > 0)
                        {
                            stat.Cycle = _data.OopValue;
                        }
                    }
                    else if (_data.OopWord == "CHAR")
                    {
                        ReadValue(statId, ref position);
                        if (_data.OopValue is > 0 and <= 255)
                        {
                            stat.Parameters[0] = (byte)_data.OopValue;
                            _game.BoardDrawTile(stat.Location.X, stat.Location.Y);
                        }
                    }
                    else if (_data.OopWord == "DIE")
                    {
                        replaceStat = true;
                        replaceTile.Element = ElementIds.Empty;
                        replaceTile.Color  = 0x0F;
                    }
                    else if (_data.OopWord == "BIND")
                    {
                        ReadWord(statId, ref position);
                        var bindStatId = (short)0;
                        if (IterateStat(statId, ref bindStatId, _data.OopWord))
                        {
                            stat.Code = _data.Board.StatusElements[bindStatId].Code;
                            position = 0;
                        }
                    }
                    else
                    {
                        var textLine = _data.OopWord;
                        if (Send(statId, _data.OopWord, false)) {
                            lineFinished = false;
                        }
                        else
                        {
                            if (textLine.IndexOf(":", StringComparison.Ordinal) == -1)
                            {
                                Error(statId, $"Bad command {textLine}");
                            }
                        }
                    }
                }

                if (lineFinished)
                {
                    SkipLine(statId, ref position);
                }
            }
            else if (_data.OopChar == '\r')
            {
                if (textWindowState.Lines.Count > 0)
                {
                    _textWindow.Append(textWindowState, "");
                }
            }
            else if (_data.OopChar == '\0')
            {
                endOfProgram = true;
            }
            else
            {
                var textLine = $"{_data.OopChar}{ReadLineToEnd(statId, ref position)}";
                _textWindow.Append(textWindowState, textLine);
            }
        } while (false == (endOfProgram || stopRunning || repeatInsNextTick || replaceStat || insCount > 32));

        if (repeatInsNextTick)
        {
            position = lastPosition;
        }

        if (_data.OopChar == '\0')
        {
            position = -1;
        }

        if (textWindowState.Lines.Count > 1)
        {
            var namePosition = (short)0;
            ReadChar(statId, ref namePosition);
            if (_data.OopChar == '@')
            {
                name = ReadLineToEnd(statId, ref namePosition);
            }

            if (name.Length == 0)
            {
                name = "Interaction";
            }
            textWindowState.Title = name;
            _textWindow.DrawOpen(textWindowState);
            _textWindow.Select(textWindowState, true, false);
            _textWindow.DrawClose();
            _textWindow.Free(textWindowState);

            if (textWindowState.Hyperlink.Length > 0)
            {
                if (Send(statId, textWindowState.Hyperlink, false))
                {
                    goto StartParsing;
                }
            }
        }
        else if (textWindowState.Lines.Count == 1)
        {
            _game.DisplayMessage(200, textWindowState.Lines[0]);
            _textWindow.Free(textWindowState);
        }

        if (replaceStat)
        {
            var ix = stat.Location.X;
            var iy = stat.Location.Y;
            _game.DamageStat(statId);
            PlaceTile(ix, iy, ref replaceTile);
        }
    }

    public void SkipLine(short statId, ref short position)
    {
        do
        {
            ReadChar(statId, ref position);
        } while (_data.OopChar != '\0'&& _data.OopChar != '\r');

    }

    public bool ParseDirection(short statId, ref short position, out short dx, out short dy)
    {
        var stat = _data.Board.StatusElements[statId];
        var returnValue = true;

        if (_data.OopWord is "N" or "NORTH")
        {
            dx = 0;
            dy = -1;
        }
        else if (_data.OopWord is "S" or "SOUTH")
        {
            dx = 0;
            dy = 1;
        }
        else if (_data.OopWord is "E" or "EAST")
        {
            dx = 1;
            dy = 0;
        }
        else if (_data.OopWord is "W" or "WEST")
        {
            dx = -1;
            dy = 0;
        }
        else if (_data.OopWord is "I" or "IDLE")
        {
            dx = 0;
            dy = 0;
        }
        else if (_data.OopWord == "SEEK")
        {
            var delta = _game.CalcDirectionSeek(stat.Location.X, stat.Location.Y);
            dx = delta.X;
            dy = delta.Y;
        }
        else if (_data.OopWord == "FLOW")
        {
            dx = stat.Step.X;
            dy = stat.Step.Y;
        }
        else if (_data.OopWord == "RND")
        {
            var delta = _game.CalcDirectionRnd();
            dx = delta.X;
            dy = delta.Y;
        }
        else if (_data.OopWord == "RNDNS")
        {
            dx = 0;
            dy = (short)(_random.Next(2) * 2 - 1);
        }
        else if (_data.OopWord == "RNDNE")
        {
            dx = (short)_random.Next(2);
            dy = (dx != 0) ? (short)0 : (short)-1;
        }
        else if (_data.OopWord == "CW")
        {
            ReadWord(statId, ref position);
            returnValue = ParseDirection(statId, ref position, out dy, out dx);
            dx = (short)-dx;
        }
        else if (_data.OopWord == "CCW")
        {
            ReadWord(statId, ref position);
            returnValue = ParseDirection(statId, ref position, out dy, out dx);
            dy = (short)-dy;
        }
        else if (_data.OopWord == "RNDP")
        {
            ReadWord(statId, ref position);
            returnValue = ParseDirection(statId, ref position, out dy, out dx);
            if (_random.Next(2) == 0)
            {
                dx = (short)-dx;
            }
            else
            {
                dy = (short)-dy;
            }
        }
        else if (_data.OopWord == "OPP")
        {
            ReadWord(statId, ref position);
            returnValue = ParseDirection(statId, ref position, out dx, out dy);
            dx = (short)-dx;
            dy = (short)-dy;
        }
        else
        {
            dx = 0;
            dy = 0;
            returnValue = false;
        }

        return returnValue;
    }

    private void Error(short statId, string message)
    {
        _game.DisplayMessage(200, $"ERR: {message}");
        _sounds.Queue(5, 80, 10);
        _data.Board.StatusElements[statId].CodePosition = -1;
    }

    private void ReadDirection(short statId, ref short position, out short dx, out short dy)
    {
        ReadWord(statId, ref position);
        if (false == ParseDirection(statId, ref position, out dx, out dy))
        {
            Error(statId, "Bad direction");
        }
    }

    private void WorldSetFlag(string name)
    {
        if (WorldGetFlagPosition(name) < 0)
        {
            var index = _data.World.Info.Flags.Select((f, i) => new { Flag = f, Index = i }).FirstOrDefault(fi => fi.Flag.Text.Length == 0)?.Index;
            if (false == index.HasValue)
            {
                index = Constants.MaximumFlags - 1;
            }
            _data.World.Info.Flags[index.Value].Text = name;
        }
    }

    private void WorldClearFlag(string name)
    {
        var index = WorldGetFlagPosition(name);
        if (index >= 0)
        {
            _data.World.Info.Flags[index].Text = "";
        }
    }

    private short WorldGetFlagPosition(string name)
    {
        var index = _data.World.Info.Flags.Select((f, i) => new { Flag = f, Index = i }).FirstOrDefault(fi => fi.Flag.Text == name)?.Index;
        return (short)(index ?? -1);
    }

    private bool CheckCondition(short statId, ref short position)
    {
        var stat = _data.Board.StatusElements[statId];
        if (_data.OopWord == "NOT")
        {
            ReadWord(statId, ref position);
            return false == CheckCondition(statId, ref position);
        }
        if (_data.OopWord == "ALLIGNED")
        {
            return (stat.Location.X == _data.Board.StatusElements[0].Location.X || stat.Location.Y == _data.Board.StatusElements[0].Location.Y);
        }
        if (_data.OopWord == "CONTACT")
        {
            return (_game.Square(stat.Location.X - _data.Board.StatusElements[0].Location.X) + _game.Square(stat.Location.Y - _data.Board.StatusElements[0].Location.Y)) == 1;
        }
        if (_data.OopWord == "BLOCKED")
        {
            ReadDirection(statId, ref position, out var deltaX, out var deltaY);
            return false == _data.ElementDefinitions[_data.Board.Tiles[stat.Location.X + deltaX, stat.Location.Y + deltaY].Element].Walkable;
        }
        if (_data.OopWord == "ENERGIZED")
        {
            return _data.World.Info.EnergizerTicks > 0;
        }
        if (_data.OopWord == "ANY")
        {
            if (false == ParseTile(statId, ref position, out Tile tile)) {
                Error(statId, "Bad object kind");
            }

            var ix = (short)0;
            var iy = (short)1;
            return FindTileOnBoard(ref ix, ref iy, tile);
        }
        else
        {
            return WorldGetFlagPosition(_data.OopWord) >= 0;
        }
    }

    private bool ParseTile(short statId, ref short position, out Tile tile)
    {
        tile = new Tile()
        {
            Color = 0
        };

        ReadWord(statId, ref position);
        for (var i = 0; i < Game.ColorNames.Length; i++)
        {
            if (_data.OopWord == StringToWord(Game.ColorNames[i]))
            {
                tile.Color = (byte)(i + 0x08);
                ReadWord(statId, ref position);
                goto ColorFound;
            }
        }

        ColorFound:
        for (var i = 0; i < Constants.MaximumElements; i++)
        {
            if (_data.OopWord == StringToWord(_data.ElementDefinitions[i].Name))
            {
                tile.Element = (byte)i;
                return true;

            }
        }

        return false;
    }

    private static string StringToWord(string input)
    {
        const string allowedCharacters = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
        var inputAllowedCharacters = input.ToUpperInvariant().Select(c => allowedCharacters.Contains(c));

        return string.Join("", inputAllowedCharacters);
    }

    private bool FindTileOnBoard(ref short x, ref short y, Tile tile)
    {
        while (true)
        {
            x++;
            if (x > Constants.BoardWidth)
            {
                x = 1;
                y++;
                if (y > Constants.BoardHeight)
                {
                    return false;
                }
            }

            if (_data.Board.Tiles[x, y].Element == tile.Element)
            {
                if (tile.Color == 0 || GetColorForTileMatch(_data.Board.Tiles[x, y]) == tile.Color)
                {
                    return true;
                }
            }
        }
    }

    private byte GetColorForTileMatch(Tile tile)
    {
        switch (_data.ElementDefinitions[tile.Element].Color)
        {
            case < Constants.ColorSpecialMinimum:
                return (byte)(_data.ElementDefinitions[tile.Element].Color & 0x07);
            case Constants.ColorWhiteOnChoice:
                return (byte)(((tile.Color >> 4) & 0x0f) + 8);
            default:
                return (byte)(tile.Color & 0x0F);
        }
    }

    private void PlaceTile(short x, short y, ref Tile tile)
    {
        if (_data.Board.Tiles[x, y].Element != ElementIds.Player)
        {
            var color = tile.Color;
            if (_data.ElementDefinitions[tile.Element].Color < Constants.ColorSpecialMinimum)
            {
                color = _data.ElementDefinitions[tile.Element].Color;
            }
            else
            {
                if (color == 0)
                {
                    color = _data.Board.Tiles[x, y].Color;
                }
                if (color == 0)
                {
                    color = 0x0F;
                }
                if (_data.ElementDefinitions[tile.Element].Color == Constants.ColorWhiteOnChoice)
                {
                    color = (byte)(((color - 8) * 0x10) + 0x0F);
                }
            }

            if (_data.Board.Tiles[x, y].Element == tile.Element)
            {
                _data.Board.Tiles[x, y].Color = color;
            }
            else
            {
                _game.BoardDamageTile(x, y);
                if (_data.ElementDefinitions[tile.Element].Cycle >= 0)
                {
                    _game.AddStat(x, y, tile.Element, color, _data.ElementDefinitions[tile.Element].Cycle, _data.StatusElementTemplateDefault);
                }
                else
                {
                    _data.Board.Tiles[x, y].Element = tile.Element;
                    _data.Board.Tiles[x, y].Color = color;
                }
            }
            _game.BoardDrawTile(x, y);
        }
    }

    private string ReadLineToEnd(short statId, ref short position)
    {
        var s = new StringBuilder("");
        ReadChar(statId, ref position);
        while (_data.OopChar is not '\0' and not '\r')
        {
            s.Append(_data.OopChar);
            ReadChar(statId, ref position);
        }
        return s.ToString();
    }
}
