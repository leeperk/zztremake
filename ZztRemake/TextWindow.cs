﻿using System.Drawing;
using System.Drawing.Printing;
using System.Text;
using ZztRemake.Interactors;
using ZztRemake.Models;

namespace ZztRemake;

public class TextWindow
{
    private readonly IConsoleInteractor _consoleInteractor;
    private readonly Input _input;
    private readonly Win32.CharInfo[][] _screenCopy;

    public string ResourceDataFileName { get; set; }
    public ResourceDataHeader ResourceDataHeader { get; set; }
    public short WindowX { get; set; }
    public short WindowY { get; set; }
    public short WindowWidth { get; set; }
    public short WindowHeight { get; set; }
    public string InnerEmpty { get; set; }
    public string Text { get; set; }
    public string InnerLine { get; set; }
    public string Top { get; set; }
    public string Bottom { get; set; }
    public string Separator { get; set; }
    public string InnerSeparator { get; set; }
    public string InnerArrows { get; set; }
    public bool WasRejected { get; set; }
    public string OrderPrintId { get; set; }


    public TextWindow(IConsoleInteractor consoleInteractor, Input input)
    {
        _consoleInteractor = consoleInteractor;
        _input = input;
        _screenCopy = new Win32.CharInfo[26][];
        for (var index = 0; index < 25; index++)
        {
            _screenCopy[index] = new Win32.CharInfo[80];
        }

        ResourceDataFileName = "";
        ResourceDataHeader = new ResourceDataHeader
        {
            EntryCount = 0
        };

        InnerEmpty = "";
        Text = "";
        InnerLine = "";
        Top = "";
        Bottom = "";
        Separator = "";
        InnerSeparator = "";
        InnerArrows = "";
        OrderPrintId = "";
    }

    public void Initialize(short x, short y, short width, short height)
    {
        WindowX = x;
        WindowWidth = width;
        WindowY = y;
        WindowHeight = height;
        InnerEmpty = "";
        InnerLine = "";
        for (var count = 1; count <= WindowWidth - 5; count++) {
            InnerEmpty  += ' ';
            InnerLine += '═';
        }
        Top = "╞╤" + InnerLine + "╤╡";
        Bottom = "╞╧" + InnerLine + "╧╡";
        Separator = " ╞" + InnerLine + "╡ ";
        Text = " │" + InnerEmpty + "│ ";
        InnerArrows = $"»{InnerEmpty[1..^1]}«";
        var builder = new StringBuilder(InnerEmpty);
        for (var index = 1; index < WindowWidth / 5; index++)
        {
            builder[index * 5 + ((WindowWidth % 5) / 2) - 1] = '•';
        }
        InnerSeparator = builder.ToString();
    }

    public void InitializeState(TextWindowState state)
    {
        state.Lines.Clear();
        state.LinePos = 0;
        state.LoadedFilename = "";
    }

    public void DisplayFile(string filename, string title)
    {
        var textWindowState = new TextWindowState()
        {
            Title = title
        };
        OpenFile(filename, textWindowState);
        textWindowState.Selectable = false;
        if (textWindowState.Lines.Count > 0)
        {
            DrawOpen(textWindowState);
            Select(textWindowState, false, true);
            DrawClose();
        }
        Free(textWindowState);
    }

    public void Append(TextWindowState state, string line)
    {
        state.Lines.Add(line);
    }

    public void DrawOpen(TextWindowState state)
    {
        for (var iy = 0; iy <= WindowHeight; iy++)
        {
            _consoleInteractor.VideoMove(WindowX, (short)(iy + WindowY), WindowWidth, _screenCopy[iy], false);
        }

        for (var iy = WindowHeight / 2; iy >= 0; iy--)
        {
            _consoleInteractor.DrawText(WindowX, WindowY + iy + 1, Text, 0x0F);
            _consoleInteractor.DrawText(WindowX, WindowY + WindowHeight - iy - 1, Text, 0x0F);
            _consoleInteractor.DrawText(WindowX, WindowY + iy, Top, 0x0F);
            _consoleInteractor.DrawText(WindowX, WindowY + WindowHeight - iy, Bottom, 0x0F);
            Thread.Sleep(Constants.TextWindowAnimationSpeed);
        }
        _consoleInteractor.DrawText(WindowX, WindowY + 2, Separator, 0x0F);
        DrawTitle(0x1E, state.Title);
    }

    public void DrawClose()
    {
        for (var iy = 0; iy <= WindowHeight / 2; iy++)
        {
            _consoleInteractor.DrawText(WindowX, (short)(WindowY + iy), Top, 0x0F);
            _consoleInteractor.DrawText(WindowX, (short)(WindowY + WindowHeight - iy), Bottom, 0x0F);
            Thread.Sleep(Constants.TextWindowAnimationSpeed * 3 / 4);
            _consoleInteractor.VideoMove(WindowX, (short)(WindowY + iy), WindowWidth, _screenCopy[iy], true);
            _consoleInteractor.VideoMove(WindowX, (short)(WindowY + WindowHeight - iy), WindowWidth, _screenCopy[WindowHeight - iy], true);
        }
    }

    public void Select(TextWindowState state, bool hyperlinkAsSelect, bool viewingFile)
    {
        WasRejected = false;
        state.Hyperlink = "";
        Draw(state, false, viewingFile);
        do
        {
            _input.Update();
            var newLinePos = state.LinePos;
            if (_input.DeltaY != 0)
            {
                newLinePos += _input.DeltaY;
            }
            else if (_input.KeyPressed.Key == ConsoleKey.Enter)
            {
                if (state.Lines[state.LinePos].Length > 0 && state.Lines[state.LinePos][0] == '!')
                {
                    var pointerStr = state.Lines[state.LinePos][1..];
                    var indexOfSemicolon = pointerStr.IndexOf(";", StringComparison.Ordinal);
                    if (indexOfSemicolon > -1)
                    {
                        pointerStr = pointerStr[..indexOfSemicolon];
                    }

                    if (pointerStr[0] == '-')
                    {
                        pointerStr = pointerStr[1..];
                        Free(state);
                        OpenFile(pointerStr, state);
                        if (state.Lines.Count == 0)
                        {
                            return;
                        }
                        else
                        {
                            viewingFile = true;
                            newLinePos = state.LinePos;
                            Draw(state, false, viewingFile);
                            _input.KeyPressed = new ConsoleKeyInfo();
                        }
                    }
                    else
                    {
                        if (hyperlinkAsSelect)
                        {
                            state.Hyperlink = pointerStr;
                        }
                        else
                        {
                            pointerStr = $":{pointerStr}";
                            var indexOfLabelLine =
                                (short)(state.Lines
                                    .Select((l, i) => new { Line = l, Index = i + 1 })
                                    .Where(li => li.Line.StartsWith(pointerStr, StringComparison.InvariantCultureIgnoreCase))
                                    .Select(li => li.Index)
                                    .FirstOrDefault() - 1);
                            if (indexOfLabelLine > -1)
                            {
                                newLinePos = indexOfLabelLine;
                                _input.KeyPressed = new ConsoleKeyInfo();
                            }
                        }
                    }
                }
            }
            else
            {
                if (_input.KeyPressed.Key == ConsoleKey.PageUp)
                {
                    newLinePos = (short)(state.LinePos - WindowHeight + 4);
                }
                else if (_input.KeyPressed.Key == ConsoleKey.PageDown)
                {
                    newLinePos = (short)(state.LinePos + WindowHeight - 4);
                }
                else if (_input.KeyPressed.Key == ConsoleKey.P && (_input.KeyPressed.Modifiers & ConsoleModifiers.Alt) != 0)
                {
                    Print(state);
                }
            }

            if (newLinePos < 0)
            {
                newLinePos = 0;
            }
            else if (newLinePos >= state.Lines.Count)
            {
                newLinePos = (short)(state.Lines.Count - 1);
            }

            if (newLinePos != state.LinePos)
            {
                state.LinePos = newLinePos;
                Draw(state, false, viewingFile);
                if (state.Lines[state.LinePos].Length > 0 && state.Lines[state.LinePos][0] == '!')
                {
                    if (hyperlinkAsSelect)
                    {
                        DrawTitle(0x1E, "«Press ENTER to select this»");
                    }
                    else
                    {
                        DrawTitle(0x1E, "«Press ENTER for more info»");
                    }
                }
            }
        } while (_input.KeyPressed.Key != ConsoleKey.Escape && _input.KeyPressed.Key != ConsoleKey.Enter);

        if (_input.KeyPressed.Key == ConsoleKey.Escape)
        {
            _input.KeyPressed = new ConsoleKeyInfo();
            WasRejected = true;
        }
    }

    public void Free(TextWindowState state)
    {
        state.Lines.Clear();
        state.LoadedFilename = "";
    }

    public void Draw(TextWindowState state, bool withoutFormatting, bool viewingFile)
    {
        for (var i = 0; i <= WindowHeight - 4; i++)
        {
            DrawLine(state, (short)(state.LinePos - (WindowHeight / 2) + i + 3), withoutFormatting, viewingFile);
        }
        DrawTitle(0x1E, state.Title);
    }

    private void OpenFile(string filename, TextWindowState state)
    {
        var retVal = false == filename.Contains('.');
        if (retVal)
        {
            filename = $"{filename}.HLP";
        }

        short entryPos;
        if (filename.StartsWith("*"))
        {
            filename = filename.Substring(1);
            entryPos = -2;
        }
        else
        {
            entryPos = -1;
        }

        InitState(state);
        state.LoadedFilename = filename.ToUpperInvariant();

        if (ResourceDataHeader.EntryCount == 0)
        {
            try
            {
                using var stream = new FileStream(ResourceDataFileName, FileMode.Open);
                using var reader = new BinaryReader(stream);
                ResourceDataHeader.EntryCount = reader.ReadInt16();
                for (var index = 0; index < Constants.MaximumResourceDataFiles; index++)
                {
                    ResourceDataHeader.Name[index].TextLength = reader.ReadByte();
                    ResourceDataHeader.Name[index].Bytes = reader.ReadBytes(50);
                }

                for (var index = 0; index < Constants.MaximumResourceDataFiles; index++)
                {
                    ResourceDataHeader.FileOffset[index] = reader.ReadInt32();
                }

                stream.Close();
            }
            catch
            {
                ResourceDataHeader.EntryCount = -1;
            }
        }

        if (entryPos == -1)
        {
            entryPos = (short)(
                ResourceDataHeader.Name
                    .Take(ResourceDataHeader.EntryCount)
                    .Select((n, i) => new { Name = n.Text.ToUpper(), Index = i })
                    .LastOrDefault(ni => ni.Name == filename.ToUpper())
                    ?.Index ?? 0
            );
        }

        if (entryPos < 0)
        {
            state.Lines = new List<string>(File.ReadAllLines(filename).Take(Constants.MaximumTextWindowLines));
        }
        else
        {
            try
            {
                using var stream = new FileStream(ResourceDataFileName, FileMode.Open);
                using var reader = new BinaryReader(stream);
                reader.BaseStream.Seek(ResourceDataHeader.FileOffset[entryPos], SeekOrigin.Begin);
                while (reader.BaseStream.Position != reader.BaseStream.Length)
                {
                    var length = reader.ReadByte();
                    state.Lines.Add(
                        length == 0
                            ? ""
                            : Encoding.Default.GetString(reader.ReadBytes(length))
                    );
                    if (state.Lines.Last() == "@")
                    {
                        state.Lines[^1] = "";
                        break;
                    }
                }

                stream.Close();
            }
            catch
            {
            }
        }
    }

    private static void InitState(TextWindowState state)
    {
        state.Lines.Clear();
        state.LinePos = 0;
        state.LoadedFilename = "";
    }

    private void DrawTitle(short color, string title)
    {
        _consoleInteractor.DrawText(WindowX + 2, WindowY + 1, InnerEmpty, (byte)color);
        _consoleInteractor.DrawText(WindowX + ((WindowWidth - title.Length) / 2), WindowY + 1, title, (byte)color);
    }

    private void DrawLine(TextWindowState state, short lpos, bool withoutFormatting, bool viewingFile)
    {
        var lineY = (short)(WindowY + lpos - state.LinePos + (WindowHeight / 2));
        if (lpos == state.LinePos + 1)
        {
            _consoleInteractor.DrawText(WindowX + 2, lineY, InnerArrows, 0x1C);
        }
        else
        {
            _consoleInteractor.DrawText(WindowX + 2, lineY, InnerEmpty, 0x1E);
        }

        if (lpos > 0 && lpos <= state.Lines.Count)
        {
            if (withoutFormatting)
            {
                _consoleInteractor.DrawText(WindowX + 4, lineY, state.Lines[lpos], 0x1E);
            }
            else
            {
                var textOffset = (short)0;
                var textColor = (short)0x1E;
                var textX = (short)(WindowX + 4);
                if (state.Lines[lpos - 1].Length > 0)
                {
                    switch (state.Lines[lpos - 1][0])
                    {
                        case '!':
                            textOffset = (short)(state.Lines[lpos - 1].IndexOf(";", StringComparison.InvariantCulture) + 1);
                            _consoleInteractor.DrawCharacter(textX + 2, lineY, '►', 0x1D);
                            textX += 5;
                            textColor = 0x1F;
                            break;
                        case ':':
                            textOffset = (short)(state.Lines[lpos - 1].IndexOf(";", StringComparison.InvariantCulture) + 1);
                            textColor = 0x1F;
                            break;
                        case '$':
                            textOffset = 1;
                            textColor = 0x1F;
                            textX = (short)(textX - 4 + (WindowWidth - state.Lines[lpos - 1].Length) / 2);
                            break;
                    }
                }
                _consoleInteractor.DrawText(textX, lineY, state.Lines[lpos - 1][textOffset..], (byte)textColor);
            }
        }
        else if (lpos == 0 || lpos == state.Lines.Count + 1)
        {
            _consoleInteractor.DrawText(WindowX + 2, lineY, InnerSeparator, 0x1E);
        }
        else if (lpos == -4 && viewingFile)
        {
            _consoleInteractor.DrawText(WindowX + 2, lineY, "   Use            to view text,", 0x1A);
            _consoleInteractor.DrawText(WindowX + 2 + 7, lineY, "↑ ↓, Enter", 0x1F);
        }
        else if (lpos == -3 && viewingFile)
        {
            _consoleInteractor.DrawText(WindowX + 2 + 1, lineY, "                 to print.", 0x1A);
            _consoleInteractor.DrawText(WindowX + 2 + 12, lineY, "Alt-P", 0x1F);
        }
    }

    private void Print(TextWindowState state)
    {
        var document = new PrintDocument();
        document.DocumentName = "About ZZT...";
        document.PrintPage += (_, ev) =>
        {
            OnPrint(ev, state);
        };

        document.Print();
    }

    private void OnPrint(PrintPageEventArgs printEvent, TextWindowState state)
    {
        var font = new Font("Lucida Console", 13, FontStyle.Regular);
        const float lineHeight = 30;
        float printY = 1;
        for (var iLine = 0; iLine < state.Lines.Count; iLine++)
        {
            var line = state.Lines[iLine];
            if (line.Length > 0)
            {
                switch (line[0])
                {
                    case '$':
                        line = line[1..];
                        line = $"{new string(' ', (80 - line.Length) / 2)}{line}";
                        break;
                    case '!':
                    case ':':
                        var indexOfSemicolon = line.IndexOf(";", StringComparison.Ordinal);
                        line = indexOfSemicolon >= 0 ? line[(indexOfSemicolon + 1)..] : "";
                        break;
                    default:
                        line = $"          {line}";
                        break;
                }
                printEvent.Graphics!.DrawString(line, font, Brushes.Black, 10, printY++ * lineHeight, new StringFormat());

            }
        }

        if (state.LoadedFilename == "ORDER.HLP")
        {
            printEvent.Graphics!.DrawString(OrderPrintId, font, Brushes.Black, 10, printY * lineHeight, new StringFormat());
        }
    }
}
