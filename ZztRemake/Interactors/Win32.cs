﻿using System.Runtime.InteropServices;
using Microsoft.Win32.SafeHandles;

namespace ZztRemake.Interactors;

public static class Win32
{
    [DllImport("Kernel32.dll", SetLastError = true, CharSet = CharSet.Auto)]
    public static extern SafeFileHandle CreateFile(string fileName, [MarshalAs(UnmanagedType.U4)] uint fileAccess, [MarshalAs(UnmanagedType.U4)] uint fileShare, IntPtr securityAttributes, [MarshalAs(UnmanagedType.U4)] FileMode creationDisposition, [MarshalAs(UnmanagedType.U4)] int flags, IntPtr template);

    [DllImport("kernel32.dll")]
    public static extern bool WriteConsoleOutputAttribute(SafeFileHandle hConsoleOutput, ushort[] lpAttribute, uint nLength, Coord dwWriteCoord, out uint lpNumberOfAttrsWritten);

    [DllImport("Kernel32.DLL", EntryPoint = "WriteConsoleOutputCharacter", CharSet = CharSet.Auto)]
    public static extern bool WriteConsoleOutputCharacter(SafeFileHandle hConsoleOutput, string lpCharacter, uint nLength, Coord dwWriteCoord, out uint lpNumberOfCharsWritten);

    [DllImport("kernel32.dll", SetLastError = true)]
    public static extern bool ReadConsoleOutput(SafeFileHandle hConsoleOutput, [Out] byte[] lpBuffer, Coord dwBufferSize, Coord dwBufferCoord, ref SmallRect lpReadRegion);

    [DllImport("kernel32.dll", SetLastError = true)]
    public static extern bool WriteConsoleOutputW(SafeFileHandle hConsoleOutput, [In] byte[] lpBuffer, Coord dwBufferSize, Coord dwBufferCoord, ref SmallRect lpWriteRegion);

    [DllImport("user32.dll")]
    public static extern int DeleteMenu(IntPtr hMenu, int nPosition, int wFlags);

    [DllImport("user32.dll")]
    public static extern IntPtr GetSystemMenu(IntPtr hWnd, bool bRevert);

    [DllImport("kernel32.dll", ExactSpelling = true)]
    public static extern IntPtr GetConsoleWindow();

    [DllImport("kernel32.dll", SetLastError = true)]
    [return: MarshalAs(UnmanagedType.Bool)]
    public static extern bool AllocConsole();

    [DllImport("kernel32.dll", SetLastError = true)]
    public static extern bool SetCurrentConsoleFontEx(SafeFileHandle hConsoleOutput, bool maximumWindow, ref ConsoleFontInfoEx consoleCurrentFontEx);

    [DllImport("gdi32.dll", EntryPoint = "AddFontResourceW")]
    public static extern int AddFontResource([In][MarshalAs(UnmanagedType.LPWStr)] string lpFileName);

    [DllImport("gdi32.dll", EntryPoint = "RemoveFontResourceW")]
    public static extern int RemoveFontResource([In][MarshalAs(UnmanagedType.LPWStr)] string lpFileName);

    [DllImport("kernel32.dll", SetLastError = true)]
    public static extern bool SetConsoleMode(SafeFileHandle hConsoleHandle, uint dwMode);

    public const int FontFamily_TrueType = 4;

    [StructLayout(LayoutKind.Sequential)]
    public struct Coord
    {
        public short X;
        public short Y;

        public Coord(short x, short y)
        {
            X = x;
            Y = y;
        }
    };

    [StructLayout(LayoutKind.Sequential)]
    public struct SmallRect
    {
        public short Left;
        public short Top;
        public short Right;
        public short Bottom;

        public SmallRect(short left, short top, short right, short bottom)
        {
            Left = left;
            Top = top;
            Right = right;
            Bottom = bottom;
        }
    }

    [StructLayout(LayoutKind.Explicit)]
    public struct CharInfo
    {
        [FieldOffset(0)]
        public char AsciiChar;
        [FieldOffset(2)]
        public ushort Attributes;
    }

    [StructLayout(LayoutKind.Explicit)]
    public struct CharInfoBuffer
    {
        [FieldOffset(0)]
        public byte[] Bytes;
        [FieldOffset(0)]
        public CharInfo[] CharInfos;

        public CharInfoBuffer(CharInfo[] charInfos)
        {
            Bytes = new byte[charInfos.Length * 4];
            CharInfos = charInfos;
        }
    }

    [StructLayout(LayoutKind.Sequential, CharSet = CharSet.Unicode)]
    public unsafe struct ConsoleFontInfoEx
    {
        internal uint cbSize;
        internal uint nFont;
        internal Coord dwFontSize;
        internal int FontFamily;
        internal int FontWeight;
        internal fixed char FaceName[32];
    }

}
