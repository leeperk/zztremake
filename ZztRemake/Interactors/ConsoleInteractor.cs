﻿using System.Diagnostics;
using System.IO.Pipes;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Text;
using Colorful;
using Microsoft.Win32.SafeHandles;
using ZztRemake.Models;
using Console = Colorful.Console;
// ReSharper disable IdentifierTypo

namespace ZztRemake.Interactors;

public class ConsoleInteractor : IConsoleInteractor
{
    private const int _mfByCommand = 0x00000000;
    private const int _scMaximize = 0xF030;
    private const int _scSize = 0xF000;
    private const string STDOUT = "CONOUT$";
    private const string STDIN = "CONIN$";
    private const uint GENERIC_READ = 0x80000000;
    private const uint GENERIC_WRITE = 0x40000000;
    private static readonly byte[,] _palette =
    {
        /* 00 */ {0,0,0},
        /* 01 */ {0,0,170},
        /* 02 */ {0,170,0},
        /* 03 */ {0,170,170},
        /* 04 */ {170,0,0},
        /* 05 */ {170,0,170},
        /* 06 */ {170,85,0},
        /* 07 */ {170,170,170},
        /* 08 */ {85,85,85},
        /* 09 */ {85,85,255},
        /* 10 */ {85,255,85},
        /* 11 */ {85,255,255},
        /* 12 */ {255,85,85},
        /* 13 */ {255,85,255},
        /* 14 */ {255,255,85},
        /* 15 */ {255,255,255}
    };

    private const short _fontSize = 32;

    private readonly Sounds _sounds;
    private bool _blinkIsInOnState;
    private List<BlinkCharacter> _blinkCharacters;
    private object _blinkLock;
    private NamedPipeClientStream? _debugConsolePipe;
    private Process? _debugConsoleProcess;
    private bool _disposed;

    private delegate bool ReadOrWriteConsoleOutputDelegate(SafeFileHandle hConsoleOuput, byte[] lpBuffer, Win32.Coord dwBufferSize, Win32.Coord dwBufferCoord, ref Win32.SmallRect lpConsoleRegion);

    public ConsoleInteractor(Sounds sounds)
    {
        _sounds = sounds;
        _blinkCharacters = new List<BlinkCharacter>();
        _blinkLock = new object();
        _disposed = false;
    }

    public bool InitializeConsoleWindow()
    {
        Win32.AllocConsole();
        Console.Clear();
        Console.OutputEncoding = Encoding.UTF8;
        InstallFont("DOSIBMEGA8x14.ttf");
        if (false == SetConsoleSize())
        {
            return false;
        }
        InitializePalette();

        var handle = Win32.GetConsoleWindow();
        var sysMenu = Win32.GetSystemMenu(handle, false);

        if (handle != IntPtr.Zero)
        {
            //Win32.DeleteMenu(sysMenu, _scClose, _mfByCommand);
            //Win32.DeleteMenu(sysMenu, _scMinimize, _mfByCommand);
            Win32.DeleteMenu(sysMenu, _scMaximize, _mfByCommand);
            Win32.DeleteMenu(sysMenu, _scSize, _mfByCommand);
        }

        _sounds.RegisterTickHandler(5, BlinkHandler);
        return true;
    }

    public void DeinitializeConsoleWindow()
    {
        Console.ReplaceAllColorsWithDefaults();
        Console.Clear();
        Console.CursorVisible = true;
        UninstallFont("DOSIBMEGA8x14.ttf");
    }

    public void InstallFont(string fontFilename)
    {
        var result = Win32.AddFontResource(fontFilename);
        if (result == 0)
        {
            throw new Exception("Could not install font");
        }
    }

    public void UninstallFont(string fontFilename)
    {
        Win32.RemoveFontResource(fontFilename);
    }

    private bool SetConsoleSize()
    {
        var size = new Coordinate((short)(_fontSize * 1.5), _fontSize);
        while (true)
        {
            try
            {
                SetConsoleFont("DOSIBMEGA8x14", size.X, size.Y, 400);
                Console.SetBufferSize(Math.Max(Console.BufferWidth, 80), Math.Max(Console.BufferHeight, 25));
                Console.SetWindowSize(80, 25);
                Console.SetBufferSize(80, 25);
                Console.CursorVisible = false;
                SetConsoleMode();
                return true;
            }
            catch
            {
                size.Y -= 2;
                size.X = (short)(size.Y * 4 / 7);
                if (size.Y < 8)
                {
                    return false;
                }
            }
        }

    }
    private void SetConsoleFont(string fontName, short width, short height, int weight)
    {
        unsafe
        {
            var h = Win32.CreateFile(STDOUT, GENERIC_READ | GENERIC_WRITE, 2, IntPtr.Zero, FileMode.Open, 0, IntPtr.Zero);
            var fontInfo = new Win32.ConsoleFontInfoEx();
            fontInfo.cbSize = (uint)Marshal.SizeOf(fontInfo);
            fontInfo.FontFamily = Win32.FontFamily_TrueType;
            var faceNamePointer = new IntPtr(fontInfo.FaceName);
            Marshal.Copy(fontName.ToCharArray(), 0, faceNamePointer, fontName.Length);
            fontInfo.dwFontSize = new Win32.Coord(width, height);
            fontInfo.FontWeight = weight;
            Win32.SetCurrentConsoleFontEx(h, false, ref fontInfo);
        }
    }
    
    private void SetConsoleMode()
    {
        using (var h = Win32.CreateFile(STDOUT, GENERIC_READ | GENERIC_WRITE, 2, IntPtr.Zero, FileMode.Open, 0, IntPtr.Zero))
        {
            Win32.SetConsoleMode(h, 0);
        }
        using (var h = Win32.CreateFile(STDIN, GENERIC_READ | GENERIC_WRITE, 2, IntPtr.Zero, FileMode.Open, 0, IntPtr.Zero))
        {
            Win32.SetConsoleMode(h, 0);
        }
    }

    private void InitializePalette()
    {
        var colorRefs = new COLORREF[16];
        for (var index = 0; index < colorRefs.Length; index++)
        {
            object boxedColorRef = colorRefs[index];
            typeof(COLORREF)
                .GetField("ColorDWORD", BindingFlags.Instance | BindingFlags.NonPublic)
                ?.SetValue(boxedColorRef, (uint)(_palette[index, 0] | (_palette[index, 1] << 8) | (_palette[index, 2] << 16)));
            colorRefs[index] = (COLORREF)boxedColorRef;
        }

        var bufferColors = new Dictionary<string, COLORREF>
        {
            { "black", colorRefs[0] }
            , { "darkBlue", colorRefs[1] }
            , { "darkGreen", colorRefs[2] }
            , { "darkCyan", colorRefs[3] }
            , { "darkRed", colorRefs[4] }
            , { "darkMagenta", colorRefs[5] }
            , { "darkYellow", colorRefs[6] }
            , { "gray", colorRefs[7] }
            , { "darkGray", colorRefs[8] }
            , { "blue", colorRefs[9] }
            , { "green", colorRefs[10] }
            , { "cyan", colorRefs[11] }
            , { "red", colorRefs[12] }
            , { "magenta", colorRefs[13] }
            , { "yellow", colorRefs[14] }
            , { "white", colorRefs[15] }
        };
        new ColorMapper().SetBatchBufferColors(bufferColors);
    }

    public void DrawCharacter(int x, int y, char character, byte color)
    {
        if (Console.WindowWidth != 80 || Console.WindowHeight != 25)
        {
            Console.SetBufferSize(Math.Max(Console.BufferWidth, 80), Math.Max(Console.BufferHeight, 25));
            Console.SetWindowSize(80, 25);
            Console.SetBufferSize(80, 25);
            Console.CursorVisible = false;
            SetConsoleSize();
        }

        var isBlink = (color & 0x80) > 0;
        var isSameCharacter = false;
        lock (_blinkLock)
        {
            if (isBlink)
            {
                color ^= 0x80;
                var blinkCharacter = _blinkCharacters.FirstOrDefault(b => b.X == x && b.Y == y);
                if (blinkCharacter == null)
                {
                    _blinkCharacters.Add(new BlinkCharacter((short)x, (short)y, character, color));
                }
                else
                {
                    isSameCharacter = true;
                    blinkCharacter.Character = character;
                    blinkCharacter.Color = color;
                }
            }
            else
            {
                var index = _blinkCharacters.FindIndex(b => b.X == x && b.Y == y);
                if (index > -1)
                {
                    lock (_blinkLock)
                    {
                        _blinkCharacters.RemoveAt(index);
                    }
                }
            }
        }

        if (false == isSameCharacter)
        {
            var h = Win32.CreateFile(STDOUT, GENERIC_WRITE, 2, IntPtr.Zero, FileMode.Open, 0, IntPtr.Zero);
            var coordinate = new Win32.Coord((short)x, (short)y);
            Win32.WriteConsoleOutputAttribute(h, new ushort[] { color }, 1, coordinate, out _);
            Win32.WriteConsoleOutputCharacter(h, character.ToString(), 1, coordinate, out _);
        }
    }

    public void DrawText(int x, int y, string text, byte color)
    {
        var index = 0;
        while (x < 80 && index < text.Length)
        {
            DrawCharacter(x++, y, text[index++], color);
        }
    }

    public void VideoMove(short x, short y, short numberOfCharacters, Win32.CharInfo[] charInfos, bool toConsole)
    {
        var h = Win32.CreateFile(STDOUT, GENERIC_READ | GENERIC_WRITE, 2, IntPtr.Zero, FileMode.Open, 0, IntPtr.Zero);
        var sourceRect = new Win32.SmallRect(x, y, (short)(x + numberOfCharacters - 1), y);
        var buffer = new Win32.CharInfoBuffer(charInfos);
        ReadOrWriteConsoleOutputDelegate consoleBufferOperation
            = toConsole ? Win32.WriteConsoleOutputW : Win32.ReadConsoleOutput;
        consoleBufferOperation(h, buffer.Bytes, new Win32.Coord(numberOfCharacters, 1), new Win32.Coord(0, 0), ref sourceRect);
    }

    public void Clear(byte color)
    {
        var blank = new string(' ', 80);
        for (var y = 0; y < 25; y++)
        {
            DrawText(0, y, blank, color);
        }
    }

    public bool HasKeyBeenPressed()
    {
        return Console.KeyAvailable;
    }

    public ConsoleKeyInfo ReadKey()
    {
        return Console.ReadKey(false);
    }

    public void Test(short x, short y, string text)
    {
        var h = Win32.CreateFile(STDOUT, GENERIC_WRITE, 2, IntPtr.Zero, FileMode.Open, 0, IntPtr.Zero);
        if (false == h.IsInvalid)
        {
            var attributes = new ushort[256];
            for (ushort index = 0; index < 256; index++)
            {
                attributes[index] = index;
            }
            Win32.WriteConsoleOutputCharacter(h, new string('A', 256), (uint)text.Length, new Win32.Coord(x, y), out _);
            Win32.WriteConsoleOutputAttribute(h, attributes, 256, new Win32.Coord(x, y), out _);
        }
    }

    private void BlinkHandler(ushort timer20TimesPerSecondTicks)
    {
        _blinkIsInOnState = !_blinkIsInOnState;

        lock (_blinkLock)
        {
            foreach (var blinkCharacter in _blinkCharacters)
            {
                byte color;
                if (_blinkIsInOnState)
                {
                    color = blinkCharacter.Color;
                }
                else
                {
                    var backgroundColor = blinkCharacter.Color & 0xF0;
                    color = (byte)(backgroundColor | (backgroundColor >> 4));
                }

                var h = Win32.CreateFile(STDOUT, GENERIC_WRITE, 2, IntPtr.Zero, FileMode.Open, 0, IntPtr.Zero);
                var coordinate = new Win32.Coord(blinkCharacter.X, blinkCharacter.Y);
                Win32.WriteConsoleOutputAttribute(h, new ushort[] { color }, 1, coordinate, out _);
            }
        }
    }

    public void OpenDebugConsole()
    {
        var processPathAndFilename = Path.GetDirectoryName(Assembly.GetExecutingAssembly().ManifestModule.ToString());
        if (processPathAndFilename != null)
        {
            _debugConsoleProcess = Process.Start(new ProcessStartInfo()
            {
                FileName = Path.Combine(processPathAndFilename, "LogConsole.exe")
                , UseShellExecute = true
            });
            _debugConsolePipe = new NamedPipeClientStream(".", "ZztLog", PipeDirection.InOut);
            _debugConsolePipe.Connect();
            _debugConsolePipe.ReadMode = PipeTransmissionMode.Message;
            _debugConsolePipe.Write(Encoding.UTF8.GetBytes("ZZT connected to Log Console."));
        }
    }

    public void WriteToDebug(string message)
    {
        if (_debugConsolePipe == null)
        {
            OpenDebugConsole();
        }

        _debugConsolePipe?.Write(Encoding.UTF8.GetBytes(message));
    }

    public void CloseDebugConsole()
    {
        if (_debugConsolePipe != null)
        {
            _debugConsolePipe.Write(Encoding.UTF8.GetBytes("Quit"));
            _debugConsoleProcess?.Close();
            _debugConsolePipe.Dispose();
            _debugConsolePipe = null;
            _debugConsoleProcess = null;
        }
    }

    protected virtual void Dispose(bool disposing)
    {
        if (_disposed)
        {
            return;
        }

        if (disposing)
        {
            _sounds.Dispose();
            CloseDebugConsole();
        }

        _disposed = true;
    }

    public void Dispose()
    {
        Dispose(true);
        GC.SuppressFinalize(this);
    }
}
