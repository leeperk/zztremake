﻿namespace ZztRemake.Interactors;

public interface IConsoleInteractor: IDisposable
{
    bool InitializeConsoleWindow();
    void DeinitializeConsoleWindow();
    void DrawCharacter(int x, int y, char character, byte color);
    void DrawText(int x, int y, string text, byte color);
    void VideoMove(short x, short y, short numberOfCharacters, Win32.CharInfo[] buffer, bool toConsole);
    void Clear(byte color);
    bool HasKeyBeenPressed();
    ConsoleKeyInfo ReadKey();
    void OpenDebugConsole();
    void WriteToDebug(string message);
    void CloseDebugConsole();
}
