﻿using System.Text;
using ZztRemake.Interactors;
using ZztRemake.Models;
// ReSharper disable SwapViaDeconstruction

namespace ZztRemake;

public class Game
{
    private readonly IConsoleInteractor _consoleInteractor;
    private static readonly char[] _characters = {
        ' ', '☺', '☻', '♥', '♦', '♣', '♠', '•', '◘', '○', '◙', '♂', '♀', '♪', '♫', '☼',
        '►', '◄', '↕', '‼', '¶', '§', '▬', '↨', '↑', '↓', '→', '←', '∟', '↔', '▲', '▼',
        ' ', '!', '"', '#', '$', '%', '&', '\'', '(', ')', '*', '+', ',', '-', '.', '/',
        '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', ':', ';', '<', '=', '>', '?',
        '@', 'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O',
        'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z', '[', '\\', ']', '^', '_',
        '`', 'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o',
        'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', '{', '|', '}', '~', '⌂',
        'Ç', 'ü', 'é', 'â', 'ä', 'à', 'å', 'ç', 'ê', 'ë', 'è', 'ï', 'î', 'ì', 'Ä', 'Å',
        'É', 'æ', 'Æ', 'ô', 'ö', 'ò', 'û', 'ù', 'ÿ', 'Ö', 'Ü', '¢', '£', '¥', '₧', 'ƒ',
        'á', 'í', 'ó', 'ú', 'ñ', 'Ñ', 'ª', 'º', '¿', '⌐', '¬', '½', '¼', '¡', '«', '»',
        '░', '▒', '▓', '│', '┤', '╡', '╢', '╖', '╕', '╣', '║', '╗', '╝', '╜', '╛', '┐',
        '└', '┴', '┬', '├', '─', '┼', '╞', '╟', '╚', '╔', '╩', '╦', '╠', '═', '╬', '╧',
        '╨', '╤', '╥', '╙', '╘', '╒', '╓', '╫', '╪', '┘', '┌', '█', '▄', '▌', '▐', '▀',
        'α', 'ß', 'Γ', 'π', 'Σ', 'σ', 'µ', 'τ', 'Φ', 'Θ', 'Ω', 'δ', '∞', 'φ', 'ε', '∩',
        '≡', '±', '≥', '≤', '⌠', '⌡', '÷', '≈', '°', '∙', '·', '√', 'ⁿ', '²', '■', ' '
    };
    private static readonly byte[] _progressColors = { 0x14, 0x1C, 0x15, 0x1D, 0x16, 0x1E, 0x17, 0x1F };
    private static readonly string[] _progressCharacters = { "....|", "...*/", "..*.-", ".*..\\", "*...|", "..../", "....-", "....\\" };
    private const string _lineDrawCharacters = "∙╨╥║╡╝╗╣╞╚╔╠═╩╦╬";
    private const string _starAnimChars = "│/─\\";

    private static readonly Coordinate[] _diagonalDeltas = { new(-1, 1), new(0, 1), new(1, 1), new(1, 0), new(1, -1), new(0, -1), new(-1, -1), new(-1, 0) };
    private static readonly Coordinate[] _neighborDeltas = { new(0, -1), new(0, 1), new(-1, 0), new(1, 0) };
    private readonly (char? character, byte? color)[,] _previousTiles = new (char? character, byte? color)[Constants.BoardWidth + 2, Constants.BoardHeight + 2];
    private readonly ZztData _data;
    private readonly Oop _oop;
    private readonly TextWindow _textWindow;
    private readonly Input _input;
    private readonly Sounds _sounds;
    private readonly Editor _editor;
    private readonly Tile _tileBoardEdge;
    private readonly Tile _tileBorder;
    private readonly Random _random;

    public static readonly string[] ColorNames = new[] { "Blue", "Green", "Cyan", "Red", "Purple", "Yellow", "White" };

    public Game(IConsoleInteractor consoleInteractor, Sounds sounds, Random random, Input input, TextWindow textWindow)
    {
        _consoleInteractor = consoleInteractor;
        _sounds = sounds;
        _input = input;
        _random = random;
        _data = new ZztData();
        _textWindow = textWindow;
        _oop = new Oop(_data, _textWindow, _random, this, _sounds);
        _editor = new Editor(_data, _textWindow);
        _tileBoardEdge = new Tile() { Element = ElementIds.BoardEdge, Color = 0x00 };
        _tileBorder = new Tile() { Element = ElementIds.Normal, Color = 0x0E };
        InitializeElementDefinitions();
        InitializePreviousTiles();
    }

    public void Start(string[] args)
    {
        _data.WorldFileDescriptions["TOWN"] = "TOWN       The Town of ZZT";
        _data.WorldFileDescriptions["DEMO"] = "DEMO       Demo of the ZZT World Editor";
        _data.WorldFileDescriptions["CAVES"] = "CAVES      The Caves of ZZT";
        _data.WorldFileDescriptions["DUNGEONS"] = "DUNGEONS   The Dungeons of ZZT";
        _data.WorldFileDescriptions["CITY"] = "CITY       Underground City of ZZT";
        _data.WorldFileDescriptions["BEST"] = "BEST       The Best of ZZT";
        _data.WorldFileDescriptions["TOUR"] = "TOUR       Guided Tour ZZT's Other Worlds";

        _data.StartupWorldFileName = "TOWN";
        _textWindow.ResourceDataFileName = "ZZT.DAT";
        _data.IsGameTitleExitRequested = false;
        GameConfigure();
        ParseArguments(args);

        if (false == _data.IsGameTitleExitRequested)
        {
            _textWindow.OrderPrintId = _data.GameVersion;
            _consoleInteractor.Clear(0x1F);
            _textWindow.Initialize(5, 3, 50, 18);

            _data.TickSpeed = 5;
            _data.DebugEnabled = false;
            _data.SavedGameFileName = "SAVED";
            _data.SavedBoardFileName = "TEMP";
            GenerateTransitionTable();
            WorldCreate();

            GameTitleLoop();
        }

    }

    public void WorldCreate()
    {
        InitElementsGame();
        _data.World.CountOfBoardsExcludingFirstBoard = 0;
        InitEditorStats();
        ResetMessageNotShownFlags();
        BoardCreate();
        _data.World.Info.IsSave = false;
        _data.World.Info.CurrentBoard = 0;
        _data.World.Info.Ammo = 0;
        _data.World.Info.Gems = 0;
        _data.World.Info.Health = 100;
        _data.World.Info.EnergizerTicks = 0;
        _data.World.Info.Torches = 0;
        _data.World.Info.TorchTicks = 0;
        _data.World.Info.Score = 0;
        _data.World.Info.TimePassedInSeconds = 0;
        _data.World.Info.TimePassedInHundredthsOfSeconds = 0;
        for (var index = 0; index < 7; index++)
        {
            _data.World.Info.Keys[index] = false;
        }
        for (var index = 0; index < Constants.MaximumFlags; index++)
        {
            _data.World.Info.Flags[index].Bytes = new byte[] {};
        }
        BoardChange(0);
        _data.Board.Name = "Title screen";
        _data.LoadedGameFileName = "";
        _data.World.Info.Name.Bytes = new byte[] {};
    }

    public bool WorldLoad(string filename, string extension, bool titleOnly)
    {
        var loadProgress = (short)0;
        var sidebarAnimateLoading = () =>
        {
            _consoleInteractor.DrawText(69, 5, _progressCharacters[loadProgress], _progressColors[loadProgress]);
        };

        SidebarClearLine(4);
        SidebarClearLine(5);
        _consoleInteractor.DrawText(62, 5, "Loading.....", 0x1F);

        try
        {
            using var stream = new FileStream($"{filename}{extension}", FileMode.Open);
            using var reader = new BinaryReader(stream);
            WorldUnload();
            stream.Seek(0, SeekOrigin.Begin);
            _data.World.CountOfBoardsExcludingFirstBoard = reader.ReadInt16();
            if (_data.World.CountOfBoardsExcludingFirstBoard < 0)
            {
                if (_data.World.CountOfBoardsExcludingFirstBoard != -1)
                {
                    _consoleInteractor.DrawText(63, 5, "You need a newer", 0x1E);
                    _consoleInteractor.DrawText(63, 6, " version of ZZT!", 0x1E);
                    return false;
                }
                _data.World.Info.WorldType = _data.World.CountOfBoardsExcludingFirstBoard;
                _data.World.CountOfBoardsExcludingFirstBoard = reader.ReadInt16();
            }
            _data.World.Info.Ammo = reader.ReadInt16();
            _data.World.Info.Gems = reader.ReadInt16();
            for (var key = 0; key < 7; key++)
            {
                _data.World.Info.Keys[key] = reader.ReadByte() == 1;
            }
            _data.World.Info.Health = reader.ReadInt16();
            _data.World.Info.CurrentBoard = reader.ReadInt16();
            _data.World.Info.Torches = reader.ReadInt16();
            _data.World.Info.TorchTicks = reader.ReadInt16();
            _data.World.Info.EnergizerTicks = reader.ReadInt16();
            reader.ReadInt16();
            _data.World.Info.Score = reader.ReadInt16();
            _data.World.Info.Name.TextLength = reader.ReadByte();
            _data.World.Info.Name.Bytes = reader.ReadBytes(20);
            for (var index = 0; index <= 9; index++)
            {
                _data.World.Info.Flags[index].TextLength = reader.ReadByte();
                _data.World.Info.Flags[index].Bytes = reader.ReadBytes(20);
            }
            _data.World.Info.TimePassedInSeconds = reader.ReadInt16();
            _data.World.Info.TimePassedInHundredthsOfSeconds = reader.ReadInt16();
            _data.World.Info.IsSave = reader.ReadByte() == 1;
            if (titleOnly)
            {
                _data.World.CountOfBoardsExcludingFirstBoard = 0;
                _data.World.Info.CurrentBoard = 0;
                _data.World.Info.IsSave = true;
            }
            stream.Seek(512, SeekOrigin.Begin);
            for (var boardId = 0; boardId <= _data.World.CountOfBoardsExcludingFirstBoard; boardId++)
            {
                sidebarAnimateLoading();
                var board = new DataBoard();
                _data.World.Boards[boardId] = board;
                board.Size = reader.ReadInt16();
                var endOfBoardPosition = stream.Position + board.Size;
                board.Name.TextLength = reader.ReadByte();
                board.Name.Bytes = reader.ReadBytes(50);
                var elements = 0;
                while (elements < 1500)
                {
                    var tile = new DataRunLengthEncodedTile()
                    {
                        Count = reader.ReadByte()
                        ,
                        Tile = new Tile()
                        {
                            Element = reader.ReadByte()
                            ,
                            Color = reader.ReadByte()
                        }
                    };

                    board.RunLengthEncodedTiles.Add(tile);
                    elements += tile.Count;
                }
                board.Properties.MaxPlayShots = reader.ReadByte();
                board.Properties.IsDark = reader.ReadByte() == 1;
                board.Properties.NeighborBoards[0] = reader.ReadByte();
                board.Properties.NeighborBoards[1] = reader.ReadByte();
                board.Properties.NeighborBoards[2] = reader.ReadByte();
                board.Properties.NeighborBoards[3] = reader.ReadByte();
                board.Properties.RestartOnZap = reader.ReadByte() == 1;
                board.Properties.Message.TextLength = reader.ReadByte();
                board.Properties.Message.Bytes = reader.ReadBytes(58);
                board.Properties.PlayerEnter.X = reader.ReadByte();
                board.Properties.PlayerEnter.Y = reader.ReadByte();
                board.Properties.TimeLimit = reader.ReadInt16();
                stream.Seek(16, SeekOrigin.Current);
                board.Properties.StatElementCount = reader.ReadInt16();
                for (var statIndex = 0; statIndex < board.Properties.StatElementCount + 1; statIndex++)
                {
                    var statusElement = new DataStatusElement();
                    board.StatusElements.Add(statusElement);
                    statusElement.LocationX = reader.ReadByte();
                    statusElement.LocationY = reader.ReadByte();
                    statusElement.StepX = reader.ReadInt16();
                    statusElement.StepY = reader.ReadInt16();
                    statusElement.Cycle = reader.ReadInt16();
                    statusElement.Parameters[0] = reader.ReadByte();
                    statusElement.Parameters[1] = reader.ReadByte();
                    statusElement.Parameters[2] = reader.ReadByte();
                    statusElement.Follower = reader.ReadInt16();
                    statusElement.Leader = reader.ReadInt16();
                    statusElement.UnderId = reader.ReadByte();
                    statusElement.UnderColor = reader.ReadByte();
                    statusElement.Pointer = reader.ReadInt32();
                    statusElement.CurrentInstruction = reader.ReadInt16();
                    statusElement.Length = reader.ReadInt16();
                    stream.Seek(8, SeekOrigin.Current);
                    if (statusElement.Length > 0)
                    {
                        statusElement.CodeBytes = reader.ReadBytes(statusElement.Length);
                    }
                }
                stream.Seek(endOfBoardPosition, SeekOrigin.Begin);
            }
            BoardOpen(_data.World.Info.CurrentBoard);
            _data.LoadedGameFileName = filename;
            _editor.HighScoresLoad();
            SidebarClearLine(5);
            return true;
        }
        catch
        {
        }

        return false;
    }

    public bool GameWorldLoad(string extension)
    {
        var returnValue = false;
        var textWindowState = new TextWindowState();
        _textWindow.InitializeState(textWindowState);
        textWindowState.Title = extension == ".ZZT" ? "ZZT Worlds" : "Saved Games";
        textWindowState.Selectable = true;

        foreach (var filename in Directory.GetFiles(@".\", $"*{extension}"))
        {
            var entryName = Path.GetFileNameWithoutExtension(filename);
            if (_data.WorldFileDescriptions.ContainsKey(entryName))
            {
                entryName = _data.WorldFileDescriptions[entryName];
            }

            _textWindow.Append(textWindowState, entryName);
        }
        _textWindow.Append(textWindowState, "Exit");

        _textWindow.DrawOpen(textWindowState);
        _textWindow.Select(textWindowState, false, false);
        _textWindow.DrawClose();

        if (textWindowState.LinePos < textWindowState.Lines.Count && false ==  _textWindow.WasRejected) {
            var entryName = textWindowState.Lines[textWindowState.LinePos];
            if (entryName.IndexOf(' ') > -1) {
                entryName = entryName[0..entryName.IndexOf(' ')];
            }

            returnValue = WorldLoad(entryName, extension, false);
            TransitionDrawToFill('█', 0x44);
        }

        _textWindow.Free(textWindowState);

        return returnValue;
    }

    public Coordinate CalcDirectionRnd()
    {
        var x = (short)(_random.Next(3) - 1);
        var y = (short)(x == 0 ? _random.Next(3) - 1 : 0);
        return new Coordinate(x, y);
    }

    public Coordinate CalcDirectionSeek(short x, short y)
    {
        var deltaX = (short)0;
        var deltaY = (short)0;

        if (_random.Next(2) < 1 || _data.Board.StatusElements[0].Location.Y == y)
        {
            deltaX = Signum((short)(_data.Board.StatusElements[0].Location.X - x));
        }
        if (deltaX == 0)
        {
            deltaY = Signum((short)(_data.Board.StatusElements[0].Location.Y - y));
        }

        if (_data.World.Info.EnergizerTicks > 0)
        {
            deltaX = (short)(-deltaX);
            deltaY = (short)(-deltaY);
        }

        return new Coordinate(deltaX, deltaY);
    }

    public int Square(int value) => value * value;

    private void WorldUnload()
    {
        BoardClose();
        for (var i = 0; i <= _data.World.CountOfBoardsExcludingFirstBoard; i++)
        {
            _data.World.Boards[i] = null;
        }
    }

    public void BoardOpen(short boardId)
    {
        boardId = Math.Max(Math.Min(boardId, _data.World.CountOfBoardsExcludingFirstBoard), (short)0);
        InitializePreviousTiles();

        var board = _data.World.Boards[boardId]!;
        _data.Board.Name = board.Name.Text;
        _data.Board.Properties = new GameBoardProperties()
        {
            IsDark = board.Properties.IsDark
            , MaxShots = board.Properties.MaxPlayShots
            , NeighborBoards = new[]
                { board.Properties.NeighborBoards[0], board.Properties.NeighborBoards[1], board.Properties.NeighborBoards[2], board.Properties.NeighborBoards[3] }
            , Message = board.Properties.Message.Text
            , PlayerEnter = new Coordinate(board.Properties.PlayerEnter.X, board.Properties.PlayerEnter.Y)
            , ShouldRestartOnZap = board.Properties.RestartOnZap
            , TimeLimitInSeconds = board.Properties.TimeLimit
        };
        _data.Board.StatusElements = new List<GameStatusElement>(board.StatusElements.Select(s => new GameStatusElement()
        {
            Location = new Coordinate(s.LocationX, s.LocationY)
            , Step = new Coordinate(s.StepX, s.StepY)
            , Cycle = s.Cycle
            , Follower = s.Follower
            , Leader = s.Leader
            , UnderTile = new Tile() { Element = s.UnderId, Color = s.UnderColor }
            , Parameters = new[] { s.Parameters[0], s.Parameters[1], s.Parameters[2] }
            , Code = s.CodeText
            , CodePosition = s.CurrentInstruction
        }));
        LoadTiles(boardId);
        _data.World.Info.CurrentBoard = boardId;
    }

    public void PushablePush(short x, short y, short deltaX, short deltaY)
    {
        var tile = _data.Board.Tiles[x, y];
        if ((tile.Element == ElementIds.SliderNs && deltaX == 0)
            || (tile.Element == ElementIds.SliderEw && deltaY == 0)
            || _data.ElementDefinitions[tile.Element].Pushable
           )
        {
            if (_data.Board.Tiles[x + deltaX, y + deltaY].Element == ElementIds.Transporter)
            {
                TransporterMove(x, y, deltaX, deltaY);
            }
            else if (_data.Board.Tiles[x + deltaX, y + deltaY].Element != ElementIds.Empty)
            {
                PushablePush((short)(x + deltaX), (short)(y + deltaY), deltaX, deltaY);
            }

            if (false == _data.ElementDefinitions[_data.Board.Tiles[x + deltaX, y + deltaY].Element].Walkable
                && _data.ElementDefinitions[_data.Board.Tiles[x + deltaX, y + deltaY].Element].Destructible
                && _data.Board.Tiles[x + deltaX, y + deltaY].Element != ElementIds.Player
               )
            {
                BoardDamageTile((short)(x + deltaX), (short)(y + deltaY));
            }

            if (_data.ElementDefinitions[_data.Board.Tiles[x + deltaX, y + deltaY].Element].Walkable)
            {
                Move(x, y, (short)(x + deltaX), (short)(y + deltaY));
            }
        }
    }

    public void MoveStat(short statId, short newX, short newY)
    {
        var statusElement = _data.Board.StatusElements[statId];
        var iUnderElement = statusElement.UnderTile.Element;
        var iUnderColor = statusElement.UnderTile.Color;
        statusElement.UnderTile.Element = _data.Board.Tiles[newX, newY].Element;
        statusElement.UnderTile.Color = _data.Board.Tiles[newX, newY].Color;

        if (_data.Board.Tiles[statusElement.Location.X, statusElement.Location.Y].Element == ElementIds.Player)
        {
            _data.Board.Tiles[newX, newY].Color = _data.Board.Tiles[statusElement.Location.X, statusElement.Location.Y].Color;
        }
        else if (_data.Board.Tiles[newX, newY].Element == ElementIds.Empty)
        {
            _data.Board.Tiles[newX, newY].Color = (byte)(_data.Board.Tiles[statusElement.Location.X, statusElement.Location.Y].Color & 0x0F);
        }
        else
        {
            _data.Board.Tiles[newX, newY].Color = (byte)((_data.Board.Tiles[statusElement.Location.X, statusElement.Location.Y].Color & 0x0F) + (_data.Board.Tiles[newX, newY].Color & 0x7F0));
        }
        _data.Board.Tiles[newX, newY].Element = _data.Board.Tiles[statusElement.Location.X, statusElement.Location.Y].Element;
        _data.Board.Tiles[statusElement.Location.X, statusElement.Location.Y].Element = iUnderElement;
        _data.Board.Tiles[statusElement.Location.X, statusElement.Location.Y].Color = iUnderColor;

        var oldX = statusElement.Location.X;
        var oldY = statusElement.Location.Y;
        statusElement.Location.X = newX;
        statusElement.Location.Y = newY;
        BoardDrawTile(statusElement.Location.X, statusElement.Location.Y);
        BoardDrawTile(oldX, oldY);

        if (statId == 0 && _data.Board.Properties.IsDark && _data.World.Info.TorchTicks > 0)
        {
            if (Square(oldX - statusElement.Location.X) + Square(oldY - statusElement.Location.Y) == 1)
            {
                for (var ix = (short)(statusElement.Location.X - Constants.TorchDistanceX - 3); ix <= statusElement.Location.X + Constants.TorchDistanceX + 3; ix++)
                {
                    if (ix is >= 1 and <= Constants.BoardWidth)
                    {
                        for (var iy = (short)(statusElement.Location.Y - Constants.TorchDistanceY - 3); iy <= statusElement.Location.Y + Constants.TorchDistanceY + 3; iy++)
                        {
                            if (iy is >= 1 and <= Constants.BoardHeight)
                            {
                                if ((Square(ix - oldX) + (Square(iy - oldY) * 2) < Constants.TorchDistanceSquareRoot) ^
                                    (Square(ix - newX) + (Square(iy - newY) * 2) < Constants.TorchDistanceSquareRoot))
                                {
                                    BoardDrawTile(ix, iy);
                                }
                            }
                        }
                    }
                }
            }
            else
            {
                DrawPlayerSurroundings(oldX, oldY, 0);
                DrawPlayerSurroundings(statusElement.Location.X, statusElement.Location.Y, 0);
            }
        }
    }

    public void UpdateSidebar()
    {
        var worldHeader = _data.World.Info;
        var boardProperties = _data.Board.Properties;
        if (_data.GameStateElement == ElementIds.Player)
        {
            if (boardProperties.TimeLimitInSeconds > 0)
            {
                _consoleInteractor.DrawText(64, 6, "   Time:", 0x1E);
                var timeRemaining = boardProperties.TimeLimitInSeconds - worldHeader.TimePassedInSeconds;
                _consoleInteractor.DrawText(72, 6, $"{timeRemaining} ", 0x1E);
            }
            else
            {
                SidebarClearLine(6);
            }

            if (worldHeader.Health < 0)
            {
                worldHeader.Health = 0;
            }
            _consoleInteractor.DrawText(72, 7, $"{worldHeader.Health} ", 0x1E);
            _consoleInteractor.DrawText(72, 8, $"{worldHeader.Ammo} ", 0x1E);
            _consoleInteractor.DrawText(72, 9, $"{worldHeader.Torches} ", 0x1E);
            _consoleInteractor.DrawText(72, 10, $"{worldHeader.Gems} ", 0x1E);
            _consoleInteractor.DrawText(72, 11, $"{worldHeader.Score} ", 0x1E);
            if (worldHeader.TorchTicks == 0)
            {
                _consoleInteractor.DrawText(75, 9, "    ", 0x16);
            }
            else
            {
                for (var offset = 2; offset <= 5; offset++)
                {
                    if (offset < worldHeader.TorchTicks * 5 / Constants.TorchDuration)
                    {
                        _consoleInteractor.DrawCharacter(73 + offset, 9, '▒', 0x16);
                    }
                    else
                    {
                        _consoleInteractor.DrawCharacter(73 + offset, 9, '░', 0x16);
                    }
                }
            }

            for (var offset = 1; offset <= 7; offset++)
            {
                _consoleInteractor.DrawCharacter(
                    71 + offset
                    , 12
                    , worldHeader.Keys[offset]
                        ? _characters[_data.ElementDefinitions[ElementIds.Key].Character]
                        : ' '
                    , (byte)(worldHeader.Keys[offset] ? 0x18 + offset : 0x1F)
                );
            }

            _consoleInteractor.DrawText(65, 15, _sounds.IsEnabled ? " Be quiet" : " Be noisy", 0x1F);
        }
    }

    public void DisplayMessage(short time, string message)
    {
        if (GetStatIdAt(0, 0) != -1)
        {
            RemoveStat(GetStatIdAt(0, 0));
            BoardDrawBorder();
        }

        if (message.Length > 0)
        {
            AddStat(0, 0, ElementIds.MessageTimer, 0, 1, _data.StatusElementTemplateDefault);
            _data.Board.StatusElements.Last().Parameters[1] = (byte)(time / (_data.TickTimeDuration + 1));
            _data.Board.Properties.Message = message;
        }
    }

    public bool BoardShoot(byte element, short tx, short ty, short deltaX, short deltaY, short source)
    {
        if (_data.ElementDefinitions[_data.Board.Tiles[tx + deltaX, ty + deltaY].Element].Walkable
            || _data.Board.Tiles[tx + deltaX, ty + deltaY].Element == ElementIds.Water
           )
        {
            AddStat((short)(tx + deltaX), (short)(ty + deltaY), element, _data.ElementDefinitions[element].Color, 1, _data.StatusElementTemplateDefault);
            var stat = _data.Board.StatusElements[^1];
            stat.Parameters[0] = (byte)source;
            stat.Step.X = deltaX;
            stat.Step.Y = deltaY;
            stat.Parameters[1] = 100;
            return true;
        }
        if (_data.Board.Tiles[tx + deltaX, ty + deltaY].Element == ElementIds.Breakable
            || (
                _data.ElementDefinitions[_data.Board.Tiles[tx + deltaX, ty + deltaY].Element].Destructible
                && _data.Board.Tiles[tx + deltaX, ty + deltaY].Element == ElementIds.Player
                && _data.World.Info.EnergizerTicks <= 0
            )
           )
        {
            BoardDamageTile((short)(tx + deltaX), (short)(ty + deltaY));
            _sounds.Queue(2, $"{(char)16}{(char)1}");
            return true;
        }
        return false;
    }

    public void BoardDamageTile(short x, short y)
    {
        var statId = GetStatIdAt(x, y);
        if (statId != -1)
        {
            DamageStat(statId);
        }
        else
        {
            _data.Board.Tiles[x, y].Element = ElementIds.Empty;
            BoardDrawTile(x, y);
        }
    }

    public void AddStat(short tx, short ty, byte element, short color, short tcycle, GameStatusElement template)
    {
        if (_data.Board.StatusElements.Count < Constants.MaximumStatusElements)
        {
            var newStatusElement = template.Clone();
            _data.Board.StatusElements.Add(newStatusElement);
            newStatusElement.Location.X = tx;
            newStatusElement.Location.Y = ty;
            newStatusElement.Cycle = tcycle;
            newStatusElement.UnderTile.Element = _data.Board.Tiles[tx, ty].Element;
            newStatusElement.UnderTile.Color = _data.Board.Tiles[tx, ty].Color;
            newStatusElement.CodePosition = 0;
            newStatusElement.Code = template.Code;

            if (_data.ElementDefinitions[_data.Board.Tiles[tx, ty].Element].PlaceableOnTop)
            {
                _data.Board.Tiles[tx, ty].Color = (byte)((color & 0x0F) + (_data.Board.Tiles[tx, ty].Color & 0x70));
            }
            else
            {
                _data.Board.Tiles[tx, ty].Color = (byte)color;
            }
            _data.Board.Tiles[tx, ty].Element = element;
            if (ty > 0)
            {
                BoardDrawTile(tx, ty);
            }
        }
    }

    public void BoardDrawTile(short x, short y)
    {
        var tile = _data.Board.Tiles[x, y];

        if (false == _data.Board.Properties.IsDark
            || _data.ElementDefinitions[tile.Element].VisibleInDark
            || (_data.World.Info.TorchTicks > 0
                && (Square(_data.Board.StatusElements[0].Location.X - x) + Square(_data.Board.StatusElements[0].Location.Y - y) * 2) < Constants.TorchDistanceSquareRoot
            )
            || _data.ShouldForceDarknessOff
           )
        {
            if (tile.Element == ElementIds.Empty)
            {
                _consoleInteractor.DrawCharacter(x - 1, y - 1, ' ', 0x0F);
            }
            else if (_data.ElementDefinitions[tile.Element].HasGetCharacterProc)
            {
                _consoleInteractor.DrawCharacter(
                    x - 1
                    , y - 1
                    , _data.ElementDefinitions[tile.Element].GetCharacterProc!.Invoke(x, y)
                    , tile.Color
                );
            }
            else if (tile.Element < ElementIds.MinimumTextId)
            {
                _consoleInteractor.DrawCharacter(x - 1, y - 1, _characters[_data.ElementDefinitions[tile.Element].Character], tile.Color);
            }
            else
            {
                if (tile.Element == ElementIds.TextWhite)
                {

                    _consoleInteractor.DrawCharacter(x - 1, y - 1, _characters[tile.Color], 0x0F);
                }
                else
                {
                    _consoleInteractor.DrawCharacter(x - 1, y - 1, _characters[tile.Color], (byte)((((tile.Element - ElementIds.MinimumTextId) + 1) * 16) + 0xF));
                }
            }
        }
        else
        {
            _consoleInteractor.DrawCharacter(x - 1, y - 1, '░', 0x07);
        }
    }

    public void DamageStat(short attackedStatId)
    {
        var stat = _data.Board.StatusElements[attackedStatId];
        if (attackedStatId == 0)
        {
            if (_data.World.Info.Health > 0)
            {
                _data.World.Info.Health -= 10;
                UpdateSidebar();
                DisplayMessage(100, "Ouch!");
                _data.Board.Tiles[stat.Location.X, stat.Location.Y].Color = (byte)(0x70 + (_data.ElementDefinitions[ElementIds.Player].Color % 0x10));

                if (_data.World.Info.Health > 0)
                {
                    _data.World.Info.TimePassedInSeconds = 0;
                    if (_data.Board.Properties.ShouldRestartOnZap)
                    {
                        _sounds.Queue(4, _sounds.CreatePattern(32, 1, 35, 1, 39, 1, 48, 1, 16, 1));
                        _data.Board.Tiles[stat.Location.X, stat.Location.Y].Element = ElementIds.Empty;
                        BoardDrawTile(stat.Location.X, stat.Location.Y);
                        var oldX = stat.Location.X;
                        var oldY = stat.Location.Y;
                        stat.Location.X = _data.Board.Properties.PlayerEnter.X;
                        stat.Location.Y = _data.Board.Properties.PlayerEnter.Y;
                        DrawPlayerSurroundings(oldX, oldY, 0);
                        DrawPlayerSurroundings(stat.Location.X, stat.Location.Y, 0);
                        _data.IsGamePaused = true;
                    }
                    _sounds.Queue(4, _sounds.CreatePattern(16, 1, 32, 1, 19, 1, 35, 1));
                }
                else
                {
                    _sounds.Queue(4, _sounds.CreatePattern(32, 3, 35, 3, 39, 3, 48, 3, 39, 3, 42, 3, 50, 3, 55, 3, 53, 3, 56, 3, 64, 3, 69, 3, 16, 10));
                }
            }
        }
        else
        {
            switch (_data.Board.Tiles[stat.Location.X, stat.Location.Y].Element)
            {
                case ElementIds.Bullet:
                    _sounds.Queue(3, _sounds.CreatePattern(32, 1));
                    break;
                case ElementIds.Object:
                    break;
                default:
                    _sounds.Queue(3, _sounds.CreatePattern(64, 1, 16, 1, 80, 1, 48, 1));
                    break;
            }
            RemoveStat(attackedStatId);
        }
    }

    private void DrawSidebar()
    {
        SidebarClear();
        SidebarClearLine(0);
        SidebarClearLine(1);
        SidebarClearLine(2);
        _consoleInteractor.DrawText(61, 0, "    - - - - -      ", 0x1F);
        _consoleInteractor.DrawText(62, 1, "      ZZT      ", 0x70);
        _consoleInteractor.DrawText(61, 2, "    - - - - -      ", 0x1F);

        if (_data.GameStateElement == ElementIds.Player)
        {
            _consoleInteractor.DrawText(64, 7, " Health:", 0x1E);
            _consoleInteractor.DrawText(64, 8, "   Ammo:", 0x1E);
            _consoleInteractor.DrawText(64, 9, "Torches:", 0x1E);
            _consoleInteractor.DrawText(64, 10, "   Gems:", 0x1E);
            _consoleInteractor.DrawText(64, 11, "  Score:", 0x1E);
            _consoleInteractor.DrawText(64, 12, "   Keys:", 0x1E);
            _consoleInteractor.DrawCharacter(62, 7,  _characters[_data.ElementDefinitions[ElementIds.Player].Character], 0x1F);
            _consoleInteractor.DrawCharacter(62, 8,  _characters[_data.ElementDefinitions[ElementIds.Ammo].Character], 0x1B);
            _consoleInteractor.DrawCharacter(62, 9,  _characters[_data.ElementDefinitions[ElementIds.Torch].Character], 0x16);
            _consoleInteractor.DrawCharacter(62, 10, _characters[_data.ElementDefinitions[ElementIds.Gem].Character], 0x1B);
            _consoleInteractor.DrawCharacter(62, 12, _characters[_data.ElementDefinitions[ElementIds.Key].Character], 0x1F);
            _consoleInteractor.DrawText(62, 14, " T ", 0x70);
            _consoleInteractor.DrawText(65, 14, " Torch", 0x1F);
            _consoleInteractor.DrawText(62, 15, " B ", 0x30);
            _consoleInteractor.DrawText(62, 16, " H ", 0x70);
            _consoleInteractor.DrawText(65, 16, " Help", 0x1F);
            _consoleInteractor.DrawText(67, 18, " ↑↓→← ", 0x30);
            _consoleInteractor.DrawText(72, 18, " Move", 0x1F);
            _consoleInteractor.DrawText(61, 19, " Shift ↑↓→← ", 0x70);
            _consoleInteractor.DrawText(72, 19, " Shoot", 0x1F);
            _consoleInteractor.DrawText(62, 21, " S ", 0x70);
            _consoleInteractor.DrawText(65, 21, " Save game", 0x1F);
            _consoleInteractor.DrawText(62, 22, " P ", 0x30);
            _consoleInteractor.DrawText(65, 22, " Pause", 0x1F);
            _consoleInteractor.DrawText(62, 23, " Q ", 0x70);
            _consoleInteractor.DrawText(65, 23, " Quit", 0x1F);
        }
        else if (_data.GameStateElement == ElementIds.Monitor)
        {
            _data.TickSpeed = SidebarPromptSlider(false, 66, 21, "Game speed:;FS", _data.TickSpeed);
            _consoleInteractor.DrawText(62, 21, " S ", 0x70);
            _consoleInteractor.DrawText(62, 7, " W ", 0x30);
            _consoleInteractor.DrawText(65, 7, " World:", 0x1E);

            if (_data.World.Info.Name.Text.Length > 0)
            {
                _consoleInteractor.DrawText(69, 8, _data.World.Info.Name.Text, 0x1F);
            }
            else
            {
                _consoleInteractor.DrawText(69, 8, "Untitled", 0x1F);
            }
            _consoleInteractor.DrawText(62, 11, " P ", 0x70);
            _consoleInteractor.DrawText(65, 11, " Play", 0x1F);
            _consoleInteractor.DrawText(62, 12, " R ", 0x30);
            _consoleInteractor.DrawText(65, 12, " Restore game", 0x1E);
            _consoleInteractor.DrawText(62, 13, " Q ", 0x70);
            _consoleInteractor.DrawText(65, 13, " Quit", 0x1E);
            _consoleInteractor.DrawText(62, 16, " A ", 0x30);
            _consoleInteractor.DrawText(65, 16, " About ZZT!", 0x1F);
            _consoleInteractor.DrawText(62, 17, " H ", 0x70);
            _consoleInteractor.DrawText(65, 17, " High Scores", 0x1E);

            if (_data.IsEditorEnabled)
            {
                _consoleInteractor.DrawText(62, 18, " E ", 0x30);
                _consoleInteractor.DrawText(65, 18, " Board Editor", 0x1E);
            }
        }
    }

    private void SidebarClearLine(short y)
    {
        _consoleInteractor.DrawText(60, y, "│                   ", 0x11);
    }

    private void SidebarClear()
    {
        for (short y = 3; y <= 24; y++)
        {
            SidebarClearLine(y);
        }
    }

    private void RemoveStat(short statId)
    {
        if (statId < 0 || statId >= _data.Board.StatusElements.Count)
        {
            return;
        }
        var statToRemove = _data.Board.StatusElements[statId];
        if (statId < _data.CurrentStatTicked)
        {
            _data.CurrentStatTicked--;
        }

        _data.Board.Tiles[statToRemove.Location.X, statToRemove.Location.Y].Element = statToRemove.UnderTile.Element;
        _data.Board.Tiles[statToRemove.Location.X, statToRemove.Location.Y].Color = statToRemove.UnderTile.Color;
        if (statToRemove.Location.Y > 0)
        {
            BoardDrawTile(statToRemove.Location.X, statToRemove.Location.Y);
        }

        for (var i = 1; i < _data.Board.StatusElements.Count; i++)
        {
            var statusElement = _data.Board.StatusElements[i];
            if (statusElement.Follower >= statId)
            {
                if (statusElement.Follower == statId)
                {
                    statusElement.Follower = -1;
                }
                else
                {
                    statusElement.Follower--;
                }
            }
            if (statusElement.Leader >= statId)
            {
                if (statusElement.Leader == statId)
                {
                    statusElement.Leader = -1;
                }
                else
                {
                    statusElement.Leader--;
                }
            }
        }

        for (var i = statId + 1; i < _data.Board.StatusElements.Count; i++)
        {
            _data.Board.StatusElements[i - 1] = _data.Board.StatusElements[i].Clone();
        }
        _data.Board.StatusElements.RemoveAt(_data.Board.StatusElements.Count - 1);
    }

    private short GetStatIdAt(short x, short y)
    {
        return (short)_data.Board.StatusElements.FindIndex(s => s.Location.X == x && s.Location.Y == y);
    }

    private void GameAboutScreen()
    {
        _textWindow.DisplayFile("ABOUT.HLP", "About ZZT...");
    }

    private void GamePlayLoop(bool boardChanged)
    {
        var exitLoop = false;
        var pauseBlink = false;

        DrawSidebar();
        UpdateSidebar();

        if (_data.DidJustStart)
        {

            GameAboutScreen();
            if (_data.StartupWorldFileName.Length > 0)
            {
                SidebarClearLine(8);
                _consoleInteractor.DrawText(69, 8, _data.StartupWorldFileName, 0x1F);
                if (false == WorldLoad(_data.StartupWorldFileName, ".ZZT", true))
                {
                    WorldCreate();
                }
            }

            _data.ReturnBoardId = _data.World.Info.CurrentBoard;
            BoardChange(0);
            _data.DidJustStart = false;
        }

        _data.Board.Tiles[_data.Board.StatusElements[0].Location.X, _data.Board.StatusElements[0].Location.Y].Element = (byte)_data.GameStateElement;
        _data.Board.Tiles[_data.Board.StatusElements[0].Location.X, _data.Board.StatusElements[0].Location.Y].Color = _data.ElementDefinitions[_data.GameStateElement].Color;
        if (_data.GameStateElement == ElementIds.Monitor)
        {
            DisplayMessage(0, "");
            _consoleInteractor.DrawText(62, 5, "Pick a command:", 0x1B);
        }

        if (boardChanged)
        {
            TransitionDrawBoardChange();
        }

        _data.TickTimeDuration = (short)(_data.TickSpeed * 2);
        _data.IsGamePlayExitRequested = false;

        _data.CurrentTick = (short)_random.Next(100);
        _data.CurrentStatTicked = (short)(_data.Board.StatusElements.Count + 1);
        do
        {
            if (_data.IsGamePaused)
            {
                if (_sounds.HasTimeElapsed(ref _data.TickTimeCounter, 25))
                {
                    pauseBlink = !pauseBlink;
                }

                if (pauseBlink)
                {
                    _consoleInteractor.DrawCharacter(
                        _data.Board.StatusElements[0].Location.X - 1
                        , _data.Board.StatusElements[0].Location.Y - 1
                        , _characters[_data.ElementDefinitions[ElementIds.Player].Character]
                        , _data.ElementDefinitions[ElementIds.Player].Color
                    );
                }
                else
                {
                    if (_data.Board.Tiles[_data.Board.StatusElements[0].Location.X, _data.Board.StatusElements[0].Location.Y].Element == ElementIds.Player)
                    {
                        _consoleInteractor.DrawCharacter(_data.Board.StatusElements[0].Location.X - 1, _data.Board.StatusElements[0].Location.Y - 1, ' ', 0x0F);
                    }
                    else
                    {
                        BoardDrawTile(_data.Board.StatusElements[0].Location.X, _data.Board.StatusElements[0].Location.Y);
                    }

                    _consoleInteractor.DrawText(64, 5, "Pausing...", 0x1F);
                    _input.Update();
                    if (_input.KeyPressed.Key == ConsoleKey.Escape)
                    {
                        GamePromptEndPlay();
                    }

                    if (_input.DeltaX != 0 || _input.DeltaY != 0)
                    {
                        _data.ElementDefinitions[
                            _data.Board.Tiles[
                                _data.Board.StatusElements[0].Location.X + _input.DeltaX
                                , _data.Board.StatusElements[0].Location.Y + _input.DeltaY
                            ].Element
                        ].TouchProc?.Invoke(
                            (short)(_data.Board.StatusElements[0].Location.X + _input.DeltaX)
                            , (short)(_data.Board.StatusElements[0].Location.Y + _input.DeltaY)
                            , 0
                            , ref _input.DeltaX, ref _input.DeltaY
                        );
                    }

                    if ((_input.DeltaX != 0 || _input.DeltaY != 0)
                        && _data.ElementDefinitions[
                            _data.Board.Tiles[
                                _data.Board.StatusElements[0].Location.X + _input.DeltaX
                                , _data.Board.StatusElements[0].Location.Y + _input.DeltaY
                            ].Element
                        ].Walkable
                       )
                    {
                        if (_data.Board.Tiles[_data.Board.StatusElements[0].Location.X, _data.Board.StatusElements[0].Location.Y].Element == ElementIds.Player)
                        {
                            MoveStat(
                                0
                                , (short)(_data.Board.StatusElements[0].Location.X + _input.DeltaX)
                                , (short)(_data.Board.StatusElements[0].Location.Y + _input.DeltaY)
                            );
                        }
                        else
                        {
                            BoardDrawTile(_data.Board.StatusElements[0].Location.X, _data.Board.StatusElements[0].Location.Y);
                            _data.Board.StatusElements[0].Location.X = (short)(_data.Board.StatusElements[0].Location.X + _input.DeltaX);
                            _data.Board.StatusElements[0].Location.Y = (short)(_data.Board.StatusElements[0].Location.Y + _input.DeltaY);
                            _data.Board.Tiles[_data.Board.StatusElements[0].Location.X, _data.Board.StatusElements[0].Location.Y].Element = ElementIds.Player;
                            _data.Board.Tiles[_data.Board.StatusElements[0].Location.X, _data.Board.StatusElements[0].Location.Y].Color =
                                _data.ElementDefinitions[ElementIds.Player].Color;
                            BoardDrawTile(_data.Board.StatusElements[0].Location.X, _data.Board.StatusElements[0].Location.Y);
                            DrawPlayerSurroundings(_data.Board.StatusElements[0].Location.X, _data.Board.StatusElements[0].Location.Y, 0);
                            DrawPlayerSurroundings(
                                (short)(_data.Board.StatusElements[0].Location.X - _input.DeltaX)
                                , (short)(_data.Board.StatusElements[0].Location.Y - _input.DeltaY)
                                , 0
                            );
                        }

                        _data.IsGamePaused = false;
                        SidebarClearLine(5);
                        _data.CurrentTick = (short)_random.Next(100);
                        _data.CurrentStatTicked = (short)(_data.Board.StatusElements.Count + 1);
                        _data.World.Info.IsSave = true;
                    }
                }
            }
            else
            {
                if (_data.CurrentStatTicked < _data.Board.StatusElements.Count)
                {
                    var stat = _data.Board.StatusElements[_data.CurrentStatTicked];
                    if (stat.Cycle != 0 && (_data.CurrentTick % stat.Cycle) == (_data.CurrentStatTicked % stat.Cycle))
                    {
                        _data.ElementDefinitions[_data.Board.Tiles[stat.Location.X, stat.Location.Y].Element].TickProc!(_data.CurrentStatTicked);
                    }

                    _data.CurrentStatTicked++;
                }
            }

            if (_data.CurrentStatTicked >= _data.Board.StatusElements.Count && false == _data.IsGamePlayExitRequested)
            {
                if (_sounds.HasTimeElapsed(ref _data.TickTimeCounter, _data.TickTimeDuration))
                {
                    _data.CurrentTick++;
                    if (_data.CurrentTick > 420)
                    {
                        _data.CurrentTick = 0;
                    }

                    _data.CurrentStatTicked = 0;
                    _input.Update();
                }
            }
        } while ((false == exitLoop && false == _data.IsGamePlayExitRequested) || false == _data.IsGamePlayExitRequested);

        _sounds.ClearQueue();
        if (_data.GameStateElement == ElementIds.Player)
        {
            if (_data.World.Info.Health <= 0)
            {
                HighScoresAdd(_data.World.Info.Score);
            }
        } else if (_data.GameStateElement == ElementIds.Monitor)
        {
            SidebarClearLine(5);
        }

        var playerStat = _data.Board.StatusElements[0];
        _data.Board.Tiles[playerStat.Location.X, playerStat.Location.Y].Element = ElementIds.Player;
        _data.Board.Tiles[playerStat.Location.X, playerStat.Location.Y].Color = _data.ElementDefinitions[ElementIds.Player].Color;
        _sounds.BlockQueueing = false;
    }

    private void GameTitleLoop()
    {
        var boardChanged = true;
        _data.IsGameTitleExitRequested = false;
        _data.DidJustStart = true;
        _data.ReturnBoardId = 0;

        do
        {
            BoardChange(0);
            do
            {
                var startPlay = false;
                _data.GameStateElement = ElementIds.Monitor;
                _data.IsGamePaused = false;
                GamePlayLoop(boardChanged);
                boardChanged = false;
                if (_input.KeyPressed.KeyChar > 0)
                {
                    var keyPressed = _input.KeyPressed;
                    switch (keyPressed.Key)
                    {
                        case ConsoleKey.W:
                            if (GameWorldLoad(".ZZT"))
                            {
                                _data.ReturnBoardId = _data.World.Info.CurrentBoard;
                                boardChanged = true;
                            }
                            break;
                        case ConsoleKey.P:
                            if (_data.World.Info.IsSave && false == _data.DebugEnabled)
                            {
                                startPlay = WorldLoad(_data.World.Info.Name.Text, ".ZZT", false);
                                _data.ReturnBoardId = _data.World.Info.CurrentBoard;
                            }
                            else
                            {
                                startPlay = true;
                            }
                            if (startPlay)
                            {
                                BoardChange(_data.ReturnBoardId);
                                //TODO: BoardEnter();
                            }
                            break;
                        case ConsoleKey.A:
                            GameAboutScreen();
                            break;
                        case ConsoleKey.E:
                            if (_data.IsEditorEnabled)
                            {
                                //TODO: EditorLoop();
                                _data.ReturnBoardId = _data.World.Info.CurrentBoard;
                                boardChanged = true;
                            }
                            break;
                        case ConsoleKey.S:
                            _data.TickSpeed = SidebarPromptSlider(true, 66, 21, "Game speed:;FS", _data.TickSpeed);
                            if (_consoleInteractor.HasKeyBeenPressed())
                            {
                                _consoleInteractor.ReadKey();
                            }
                            break;
                        case ConsoleKey.R:
                            if (GameWorldLoad(".SAV"))
                            {
                                _data.ReturnBoardId = _data.World.Info.CurrentBoard;
                                BoardChange(_data.ReturnBoardId);
                                startPlay = true;
                            }
                            break;
                        case ConsoleKey.H:
                            _editor.HighScoresLoad();
                            _editor.HighScoresDisplay(1);
                            break;
                        case ConsoleKey.Escape:
                        case ConsoleKey.Q:
                            _data.IsGameTitleExitRequested = SidebarPromptYesNo("Quit ZZT? ");
                            break;
                    }
                    if (keyPressed.KeyChar == '|')
                    {
                        //TODO: GameDebugPrompt();
                    }

                    if (startPlay)
                    {
                        _data.GameStateElement = ElementIds.Player;
                        _data.IsGamePaused = true;
                        GamePlayLoop(true);
                        boardChanged = true;
                    }
                }
            } while (false == boardChanged && false == _data.IsGameTitleExitRequested);

        } while (false == _data.IsGameTitleExitRequested);
    }

    private void BoardClose()
    {
        var boardId = _data.World.Info.CurrentBoard;

        var board = _data.World.Boards[boardId] ?? new DataBoard();
        board.Name = ConvertStringToStringBytes(_data.Board.Name, (byte)board.Name.Bytes.Length);
        board.Properties.IsDark = _data.Board.Properties.IsDark;
        board.Properties.NeighborBoards[0] = _data.Board.Properties.NeighborBoards[0];
        board.Properties.NeighborBoards[1] = _data.Board.Properties.NeighborBoards[1];
        board.Properties.NeighborBoards[2] = _data.Board.Properties.NeighborBoards[2];
        board.Properties.NeighborBoards[3] = _data.Board.Properties.NeighborBoards[3];
        board.Properties.MaxPlayShots = _data.Board.Properties.MaxShots;
        board.Properties.Message = ConvertStringToStringBytes(_data.Board.Properties.Message, (byte)board.Properties.Message.Bytes.Length);
        board.Properties.PlayerEnter.X = (byte)_data.Board.Properties.PlayerEnter.X;
        board.Properties.PlayerEnter.Y = (byte)_data.Board.Properties.PlayerEnter.Y;
        board.Properties.RestartOnZap = _data.Board.Properties.ShouldRestartOnZap;
        board.Properties.TimeLimit = board.Properties.TimeLimit;
        board.StatusElements.Clear();
        board.StatusElements.AddRange(
            _data.Board.StatusElements.Select(s => new DataStatusElement()
            {
                LocationX = (byte)s.Location.X
                , LocationY = (byte)s.Location.Y
                , StepX = s.Step.X
                , StepY = s.Step.Y
                , Cycle = s.Cycle
                , Follower = s.Follower
                , Leader = s.Leader
                , UnderId = s.UnderTile.Element
                , UnderColor = s.UnderTile.Color
                , Parameters = new[] { s.Parameters[0], s.Parameters[1], s.Parameters[2] }
                , CurrentInstruction = s.CodePosition
                , Length = Math.Min((short)s.Code.Length, short.MaxValue)
                , CodeBytes = Encoding.Default.GetBytes(s.Code[..Math.Min((short)s.Code.Length, short.MaxValue)])
            })
        );
        for (var statusIndex = 0; statusIndex < board.StatusElements.Count; statusIndex++)
        {
            if (board.StatusElements[statusIndex].Length > 0)
            {
                for (var duplicateIndex = 1; duplicateIndex < statusIndex; duplicateIndex++)
                {
                    if (board.StatusElements[duplicateIndex].CodeText == board.StatusElements[statusIndex].CodeText)
                    {
                        board.StatusElements[duplicateIndex].Length = (short)-duplicateIndex;
                        board.StatusElements[duplicateIndex].CodeBytes = Array.Empty<byte>();
                    }
                }
            }
        }
        board.RunLengthEncodedTiles.Clear();
        board.RunLengthEncodedTiles.AddRange(RunLengthEncodeTiles(_data.Board.Tiles));

        _data.World.Boards[boardId] = board;
    }

    private void InitializeTiles()
    {
        for (var x = 0; x <= Constants.BoardWidth + 1; x++)
        {
            _data.Board.Tiles[x, 0] = new Tile() { Element = _tileBoardEdge.Element, Color = _tileBoardEdge.Color };
            _data.Board.Tiles[x, Constants.BoardHeight + 1] = new Tile() { Element = _tileBoardEdge.Element, Color = _tileBoardEdge.Color };
        }
        for (var y = 0; y < Constants.BoardHeight + 1; y++)
        {
            _data.Board.Tiles[0, y] = new Tile() { Element = _tileBoardEdge.Element, Color = _tileBoardEdge.Color };
            _data.Board.Tiles[Constants.BoardWidth + 1, y] = new Tile() { Element = _tileBoardEdge.Element, Color = _tileBoardEdge.Color };
        }

        for (var x = 1; x <= Constants.BoardWidth; x++)
        {
            for (var y = 1; y <= Constants.BoardHeight; y++)
            {
                _data.Board.Tiles[x, y] = new Tile() { Element = ElementIds.Empty, Color = 0x00 };
            }
        }

        for (var x = 1; x <= Constants.BoardWidth; x++)
        {
            _data.Board.Tiles[x, 1] = new Tile() { Element = _tileBorder.Element, Color = _tileBorder.Color };
            _data.Board.Tiles[x, Constants.BoardHeight] = new Tile() { Element = _tileBorder.Element, Color = _tileBorder.Color };
        }

        for (var y = 1; y <= Constants.BoardHeight; y++)
        {
            _data.Board.Tiles[1, y] = new Tile() { Element = _tileBorder.Element, Color = _tileBorder.Color };
            _data.Board.Tiles[Constants.BoardWidth, y] = new Tile() { Element = _tileBorder.Element, Color = _tileBorder.Color };
        }
    }

    private void BoardChange(short boardId)
    {
        _data.Board.Tiles[_data.Board.StatusElements[0].Location.X, _data.Board.StatusElements[0].Location.Y].Element = ElementIds.Player;
        _data.Board.Tiles[_data.Board.StatusElements[0].Location.X, _data.Board.StatusElements[0].Location.Y].Color = _data.ElementDefinitions[ElementIds.Player].Color;
        BoardClose();
        BoardOpen(boardId);
    }

    private void GameConfigure()
    {
        _data.IsEditorEnabled = true;
        _data.ConfigRegistration = "";
        _data.ConfigWorldFile = "";
        _data.GameVersion = "3.2";
        if (false == File.Exists("zzt.cfg"))
        {
            throw new FileNotFoundException("The configuration file 'zzt.cfg' was not found", "zzt.cfg");
        }
        var config = File.ReadAllLines("zzt.cfg");
        if (config.Length >= 1)
        {
            _data.ConfigWorldFile = config[0];
        }
        if (config.Length >= 2)
        {
            _data.ConfigRegistration = config[1];
        }
        if (_data.ConfigWorldFile.StartsWith("* "))
        {
            _data.IsEditorEnabled = false;
            _data.ConfigWorldFile = _data.ConfigWorldFile[1..];
        }
        if (_data.ConfigWorldFile.Length > 0)
        {
            _data.StartupWorldFileName = _data.ConfigWorldFile;
        }

        _consoleInteractor.Clear(0x0F);
    }

    private void ParseArguments(string[] args)
    {
        foreach (var arg in args)
        {
            if (false == arg.StartsWith("/"))
            {
                _data.StartupWorldFileName = arg;
                if (_data.StartupWorldFileName.Length > 4 && _data.StartupWorldFileName[^4] == '.')
                {
                    _data.StartupWorldFileName = _data.StartupWorldFileName[..^4];
                }
            }
        }
    }

    private void GenerateTransitionTable()
    {
        var index = 0;
        for (short y = 1; y <= Constants.BoardHeight; y++)
        {
            for (short x = 1; x <= Constants.BoardWidth; x++)
            {
                _data.TransitionTable[index].X  = x;
                _data.TransitionTable[index++].Y = y;
            }
        }

        for (index = 0; index < _data.TransitionTable.Length; index++)
        {
            var swapIndex = (short)_random.Next(_data.TransitionTable.Length);
            var tempCoordinate = _data.TransitionTable[swapIndex];
            _data.TransitionTable[swapIndex] = _data.TransitionTable[index];
            _data.TransitionTable[index] = tempCoordinate;
        }
    }

    private void ResetMessageNotShownFlags()
    {
        _data.IsMessageAmmoNotShown = true;
        _data.IsMessageOutOfAmmoNotShown = true;
        _data.IsMessageNoShootingNotShown = true;
        _data.IsMessageTorchNotShown = true;
        _data.IsMessageOutOfTorchesNotShown = true;
        _data.IsMessageRoomNotDarkNotShown = true;
        _data.IsMessageHintTorchNotShown = true;
        _data.IsMessageForestNotShown = true;
        _data.IsMessageFakeNotShown = true;
        _data.IsMessageGemNotShown = true;
        _data.IsMessageEnergizerNotShown = true;
    }

    private void BoardCreate()
    {
        _data.Board.Name = "";
        _data.Board.Properties.Message = "";
        _data.Board.Properties.MaxShots = 255;
        _data.Board.Properties.IsDark = false;
        _data.Board.Properties.ShouldRestartOnZap = false;
        _data.Board.Properties.TimeLimitInSeconds = 0;

        for (var index = 0; index <= 3; index++)
        {
            _data.Board.Properties.NeighborBoards[index]  = 0;
        }

        InitializeTiles();

        _data.Board.Tiles[Constants.BoardWidth / 2, Constants.BoardHeight / 2].Element = ElementIds.Player;
        _data.Board.Tiles[Constants.BoardWidth / 2, Constants.BoardHeight / 2].Color = _data.ElementDefinitions[ElementIds.Player].Color;

        _data.Board.StatusElements.Clear();
        _data.Board.StatusElements.Add(new GameStatusElement()
        {
            Location = new Coordinate(Constants.BoardWidth / 2, Constants.BoardHeight / 2)
            , Cycle = 1
            , UnderTile = new Tile() { Element = ElementIds.Empty, Color = 0x00 }
            , Code = ""
        });
    }

    private StringBytes ConvertStringToStringBytes(string text, byte size)
    {
        if (text.Length > size)
        {
            text = text[..size];
        }
        var bytes = Encoding.Default.GetBytes(text);
        Array.Resize(ref bytes, size);

        return new StringBytes(size) { Bytes = bytes, TextLength = (byte)text.Length };
    }

    private static char DefaultGetCharacterProc(short x, short y)
    {
        return '?';
    }

    private static void DefaultTickProc(short statusElementId)
    {
    }

    private static void DefaultTouchProc(short x, short y, short sourceStatusElementId, ref short deltaX, ref short deltaY)
    {
    }

    private void InitEditorStats()
    {
        _data.PlayerDirection.X = 0;
        _data.PlayerDirection.Y = 0;

        for (var i = 0; i < Constants.MaximumElements; i++)
        {
            _data.World.EditorStatSettings[i].Parameters[0] = 4;
            _data.World.EditorStatSettings[i].Parameters[1] = 4;
            _data.World.EditorStatSettings[i].Parameters[2] = 0;
            _data.World.EditorStatSettings[i].Step.X = 0;
            _data.World.EditorStatSettings[i].Step.Y = -1;
        }

        _data.World.EditorStatSettings[ElementIds.Object].Parameters[0] = 1;
        _data.World.EditorStatSettings[ElementIds.Bear].Parameters[0] = 8;
    }

    private void InitElementsEditor()
    {
        InitializeElementDefinitions();
        _data.ElementDefinitions[28].Character = 176; // ░
        _data.ElementDefinitions[28].Color = Constants.ColorChoiceOnBlack;
        _data.ShouldForceDarknessOff = true;
    }

    private void InitElementsGame()
    {
        InitializeElementDefinitions();
        _data.ShouldForceDarknessOff = false;
    }

    private void InitializeElementDefinitions()
    {
        _data.ElementDefinitions.Clear();
        for (var index = 0; index < Constants.MaximumElements; index++)
        {
            _data.ElementDefinitions.Add(new ElementDefinition());
            _data.ElementDefinitions[index].Character = 0x20;
            _data.ElementDefinitions[index].Color = Constants.ColorChoiceOnBlack;
            _data.ElementDefinitions[index].Destructible = false;
            _data.ElementDefinitions[index].Pushable = false;
            _data.ElementDefinitions[index].VisibleInDark = false;
            _data.ElementDefinitions[index].PlaceableOnTop = false;
            _data.ElementDefinitions[index].Walkable = false;
            _data.ElementDefinitions[index].GetCharacterProc = null;
            _data.ElementDefinitions[index].Cycle = -1;
            _data.ElementDefinitions[index].TickProc = DefaultTickProc;
            _data.ElementDefinitions[index].HasGetCharacterProc = false;
            _data.ElementDefinitions[index].GetCharacterProc = DefaultGetCharacterProc;
            _data.ElementDefinitions[index].TouchProc = DefaultTouchProc;
            _data.ElementDefinitions[index].EditorCategory = 0;
            _data.ElementDefinitions[index].EditorShortcut = '\0';
            _data.ElementDefinitions[index].Name  = "";
            _data.ElementDefinitions[index].CategoryName = "";
            _data.ElementDefinitions[index].Parameter1Name = "";
            _data.ElementDefinitions[index].Parameter2Name = "";
            _data.ElementDefinitions[index].ParameterBulletTypeName = "";
            _data.ElementDefinitions[index].ParameterBoardName = "";
            _data.ElementDefinitions[index].ParameterDirectionName = "";
            _data.ElementDefinitions[index].ParameterTextName = "";
            _data.ElementDefinitions[index].ScoreValue = 0;
        }

        _data.ElementDefinitions[ElementIds.Empty].Character = 32;
        _data.ElementDefinitions[ElementIds.Empty].Color = 0x70;
        _data.ElementDefinitions[ElementIds.Empty].Pushable = true;
        _data.ElementDefinitions[ElementIds.Empty].Walkable = true;
        _data.ElementDefinitions[ElementIds.Empty].Name = "Empty";

        _data.ElementDefinitions[ElementIds.Monitor].Character = 32;
        _data.ElementDefinitions[ElementIds.Monitor].Color = 0x07;
        _data.ElementDefinitions[ElementIds.Monitor].Cycle = 1;
        _data.ElementDefinitions[ElementIds.Monitor].TickProc = OnMonitorTick;
        _data.ElementDefinitions[ElementIds.Monitor].Name = "Monitor";

        _data.ElementDefinitions[ElementIds.Water].Character = 176; // ░
        _data.ElementDefinitions[ElementIds.Water].Color = 0xF9;
        _data.ElementDefinitions[ElementIds.Water].PlaceableOnTop = true;
        _data.ElementDefinitions[ElementIds.Water].EditorCategory = Constants.CategoryTerrain;
        _data.ElementDefinitions[ElementIds.Water].TouchProc = OnWaterTouch;
        _data.ElementDefinitions[ElementIds.Water].EditorShortcut = 'W';
        _data.ElementDefinitions[ElementIds.Water].Name = "Water";
        _data.ElementDefinitions[ElementIds.Water].CategoryName = "Terrains";

        _data.ElementDefinitions[ElementIds.Forest].Character = 176; // ░
        _data.ElementDefinitions[ElementIds.Forest].Color = 0x20;
        _data.ElementDefinitions[ElementIds.Forest].Walkable = false;
        _data.ElementDefinitions[ElementIds.Forest].TouchProc = OnForestTouch;
        _data.ElementDefinitions[ElementIds.Forest].EditorCategory = Constants.CategoryTerrain;
        _data.ElementDefinitions[ElementIds.Forest].EditorShortcut = 'F';
        _data.ElementDefinitions[ElementIds.Forest].Name = "Forest";

        _data.ElementDefinitions[ElementIds.Player].Character = 2; // ☻
        _data.ElementDefinitions[ElementIds.Player].Color = 0x1F;
        _data.ElementDefinitions[ElementIds.Player].Destructible = true;
        _data.ElementDefinitions[ElementIds.Player].Pushable = true;
        _data.ElementDefinitions[ElementIds.Player].VisibleInDark = true;
        _data.ElementDefinitions[ElementIds.Player].Cycle = 1;
        _data.ElementDefinitions[ElementIds.Player].TickProc = OnPlayerTick;
        _data.ElementDefinitions[ElementIds.Player].EditorCategory = Constants.CategoryItem;
        _data.ElementDefinitions[ElementIds.Player].EditorShortcut = 'Z';
        _data.ElementDefinitions[ElementIds.Player].Name = "Player";
        _data.ElementDefinitions[ElementIds.Player].CategoryName = "Items";

        _data.ElementDefinitions[ElementIds.Lion].Character = 234; // α
        _data.ElementDefinitions[ElementIds.Lion].Color = 0x0C;
        _data.ElementDefinitions[ElementIds.Lion].Destructible = true;
        _data.ElementDefinitions[ElementIds.Lion].Pushable = true;
        _data.ElementDefinitions[ElementIds.Lion].Cycle = 2;
        _data.ElementDefinitions[ElementIds.Lion].TickProc = OnLionTick;
        _data.ElementDefinitions[ElementIds.Lion].TouchProc = OnDamagingTouch;
        _data.ElementDefinitions[ElementIds.Lion].EditorCategory = Constants.CategoryCreature;
        _data.ElementDefinitions[ElementIds.Lion].EditorShortcut = 'L';
        _data.ElementDefinitions[ElementIds.Lion].Name = "Lion";
        _data.ElementDefinitions[ElementIds.Lion].CategoryName = "Beasts";
        _data.ElementDefinitions[ElementIds.Lion].Parameter1Name = "Intelligence?";
        _data.ElementDefinitions[ElementIds.Lion].ScoreValue = 1;

        _data.ElementDefinitions[ElementIds.Tiger].Character = 227; // π
        _data.ElementDefinitions[ElementIds.Tiger].Color = 0x0B;
        _data.ElementDefinitions[ElementIds.Tiger].Destructible = true;
        _data.ElementDefinitions[ElementIds.Tiger].Pushable = true;
        _data.ElementDefinitions[ElementIds.Tiger].Cycle = 2;
        _data.ElementDefinitions[ElementIds.Tiger].TickProc = OnTigerTick;
        _data.ElementDefinitions[ElementIds.Tiger].TouchProc = OnDamagingTouch;
        _data.ElementDefinitions[ElementIds.Tiger].EditorCategory = Constants.CategoryCreature;
        _data.ElementDefinitions[ElementIds.Tiger].EditorShortcut = 'T';
        _data.ElementDefinitions[ElementIds.Tiger].Name = "Tiger";
        _data.ElementDefinitions[ElementIds.Tiger].Parameter1Name = "Intelligence?";
        _data.ElementDefinitions[ElementIds.Tiger].Parameter2Name = "Firing rate?";
        _data.ElementDefinitions[ElementIds.Tiger].ParameterBulletTypeName = "Firing type?";
        _data.ElementDefinitions[ElementIds.Tiger].ScoreValue = 2;

        _data.ElementDefinitions[ElementIds.CentipedeHead].Character = 233; // π
        _data.ElementDefinitions[ElementIds.CentipedeHead].Destructible = true;
        _data.ElementDefinitions[ElementIds.CentipedeHead].Cycle = 2;
        _data.ElementDefinitions[ElementIds.CentipedeHead].TickProc = OnCentipedeHeadTick;
        _data.ElementDefinitions[ElementIds.CentipedeHead].TouchProc = OnDamagingTouch;
        _data.ElementDefinitions[ElementIds.CentipedeHead].EditorCategory = Constants.CategoryCreature;
        _data.ElementDefinitions[ElementIds.CentipedeHead].EditorShortcut = 'H';
        _data.ElementDefinitions[ElementIds.CentipedeHead].Name = "Head";
        _data.ElementDefinitions[ElementIds.CentipedeHead].CategoryName = "Centipedes";
        _data.ElementDefinitions[ElementIds.CentipedeHead].Parameter1Name = "Intelligence?";
        _data.ElementDefinitions[ElementIds.CentipedeHead].Parameter2Name = "Deviance?";
        _data.ElementDefinitions[ElementIds.CentipedeHead].ScoreValue = 1;

        _data.ElementDefinitions[ElementIds.CentipedeSegment].Character = (byte)'O';
        _data.ElementDefinitions[ElementIds.CentipedeSegment].Destructible = true;
        _data.ElementDefinitions[ElementIds.CentipedeSegment].Cycle = 2;
        _data.ElementDefinitions[ElementIds.CentipedeSegment].TickProc = OnCentipedeSegmentTick;
        _data.ElementDefinitions[ElementIds.CentipedeSegment].TouchProc = OnDamagingTouch;
        _data.ElementDefinitions[ElementIds.CentipedeSegment].EditorCategory = Constants.CategoryCreature;
        _data.ElementDefinitions[ElementIds.CentipedeSegment].EditorShortcut = 'S';
        _data.ElementDefinitions[ElementIds.CentipedeSegment].Name = "Segment";
        _data.ElementDefinitions[ElementIds.CentipedeSegment].ScoreValue = 3;

        _data.ElementDefinitions[ElementIds.Bullet].Character = 248; // °
        _data.ElementDefinitions[ElementIds.Bullet].Color = 0x0F;
        _data.ElementDefinitions[ElementIds.Bullet].Destructible = true;
        _data.ElementDefinitions[ElementIds.Bullet].Cycle = 1;
        _data.ElementDefinitions[ElementIds.Bullet].TickProc = OnBulletTick;
        _data.ElementDefinitions[ElementIds.Bullet].TouchProc = OnDamagingTouch;
        _data.ElementDefinitions[ElementIds.Bullet].Name = "Bullet";

        _data.ElementDefinitions[ElementIds.Star].Character = (byte)'S';
        _data.ElementDefinitions[ElementIds.Star].Color = 0x0F;
        _data.ElementDefinitions[ElementIds.Star].Destructible = false;
        _data.ElementDefinitions[ElementIds.Star].Cycle = 1;
        _data.ElementDefinitions[ElementIds.Star].TickProc = OnStarTick;
        _data.ElementDefinitions[ElementIds.Star].TouchProc = OnDamagingTouch;
        _data.ElementDefinitions[ElementIds.Star].HasGetCharacterProc = true;
        _data.ElementDefinitions[ElementIds.Star].GetCharacterProc = OnStarDraw;
        _data.ElementDefinitions[ElementIds.Star].Name = "Star";

        _data.ElementDefinitions[ElementIds.Key].Character = 12; // ♀
        _data.ElementDefinitions[ElementIds.Key].Pushable = true;
        _data.ElementDefinitions[ElementIds.Key].TouchProc = OnKeyTouch;
        _data.ElementDefinitions[ElementIds.Key].EditorCategory = Constants.CategoryItem;
        _data.ElementDefinitions[ElementIds.Key].EditorShortcut = 'K';
        _data.ElementDefinitions[ElementIds.Key].Name = "Key";

        _data.ElementDefinitions[ElementIds.Ammo].Character = 132; // ä
        _data.ElementDefinitions[ElementIds.Ammo].Color = 0x03;
        _data.ElementDefinitions[ElementIds.Ammo].Pushable = true;
        _data.ElementDefinitions[ElementIds.Ammo].TouchProc = OnAmmoTouch;
        _data.ElementDefinitions[ElementIds.Ammo].EditorCategory = Constants.CategoryItem;
        _data.ElementDefinitions[ElementIds.Ammo].EditorShortcut = 'A';
        _data.ElementDefinitions[ElementIds.Ammo].Name = "Ammo";

        _data.ElementDefinitions[ElementIds.Gem].Character = 4; // ♦
        _data.ElementDefinitions[ElementIds.Gem].Pushable = true;
        _data.ElementDefinitions[ElementIds.Gem].TouchProc = OnGemTouch;
        _data.ElementDefinitions[ElementIds.Gem].Destructible = true;
        _data.ElementDefinitions[ElementIds.Gem].EditorCategory = Constants.CategoryItem;
        _data.ElementDefinitions[ElementIds.Gem].EditorShortcut = 'G';
        _data.ElementDefinitions[ElementIds.Gem].Name = "Gem";

        _data.ElementDefinitions[ElementIds.Passage].Character = 240; // ≡
        _data.ElementDefinitions[ElementIds.Passage].Color = Constants.ColorWhiteOnChoice;
        _data.ElementDefinitions[ElementIds.Passage].Cycle = 0;
        _data.ElementDefinitions[ElementIds.Passage].VisibleInDark = true;
        _data.ElementDefinitions[ElementIds.Passage].TouchProc = OnPassageTouch;
        _data.ElementDefinitions[ElementIds.Passage].EditorCategory = Constants.CategoryItem;
        _data.ElementDefinitions[ElementIds.Passage].EditorShortcut = 'P';
        _data.ElementDefinitions[ElementIds.Passage].Name = "Passage";
        _data.ElementDefinitions[ElementIds.Passage].ParameterBoardName = "Room thru passage?";

        _data.ElementDefinitions[ElementIds.Door].Character = 10; // ◙
        _data.ElementDefinitions[ElementIds.Door].Color = Constants.ColorWhiteOnChoice;
        _data.ElementDefinitions[ElementIds.Door].TouchProc = OnDoorTouch;
        _data.ElementDefinitions[ElementIds.Door].EditorCategory = Constants.CategoryItem;
        _data.ElementDefinitions[ElementIds.Door].EditorShortcut = 'D';
        _data.ElementDefinitions[ElementIds.Door].Name = "Door";

        _data.ElementDefinitions[ElementIds.Scroll].Character = 232; // Φ
        _data.ElementDefinitions[ElementIds.Scroll].Color = 0x0F;
        _data.ElementDefinitions[ElementIds.Scroll].TouchProc = OnScrollTouch;
        _data.ElementDefinitions[ElementIds.Scroll].TickProc = OnScrollTick;
        _data.ElementDefinitions[ElementIds.Scroll].Pushable = true;
        _data.ElementDefinitions[ElementIds.Scroll].Cycle = 1;
        _data.ElementDefinitions[ElementIds.Scroll].EditorCategory = Constants.CategoryItem;
        _data.ElementDefinitions[ElementIds.Scroll].EditorShortcut = 'S';
        _data.ElementDefinitions[ElementIds.Scroll].Name = "Scroll";
        _data.ElementDefinitions[ElementIds.Scroll].ParameterTextName = "Edit text of scroll";

        _data.ElementDefinitions[ElementIds.Duplicator].Character = 250; // ·
        _data.ElementDefinitions[ElementIds.Duplicator].Color = 0x0F;
        _data.ElementDefinitions[ElementIds.Duplicator].Cycle = 2;
        _data.ElementDefinitions[ElementIds.Duplicator].TickProc = OnDuplicatorTick;
        _data.ElementDefinitions[ElementIds.Duplicator].HasGetCharacterProc = true;
        _data.ElementDefinitions[ElementIds.Duplicator].GetCharacterProc = OnDuplicatorDraw;
        _data.ElementDefinitions[ElementIds.Duplicator].EditorCategory = Constants.CategoryItem;
        _data.ElementDefinitions[ElementIds.Duplicator].EditorShortcut = 'U';
        _data.ElementDefinitions[ElementIds.Duplicator].Name = "Duplicator";
        _data.ElementDefinitions[ElementIds.Duplicator].ParameterDirectionName = "Source direction?";
        _data.ElementDefinitions[ElementIds.Duplicator].Parameter2Name = "Duplication rate?;SF";

        _data.ElementDefinitions[ElementIds.Torch].Character = 157; // ¥
        _data.ElementDefinitions[ElementIds.Torch].Color = 0x06;
        _data.ElementDefinitions[ElementIds.Torch].VisibleInDark = true;
        _data.ElementDefinitions[ElementIds.Torch].TouchProc = OnTorchTouch;
        _data.ElementDefinitions[ElementIds.Torch].EditorCategory = Constants.CategoryItem;
        _data.ElementDefinitions[ElementIds.Torch].EditorShortcut = 'T';
        _data.ElementDefinitions[ElementIds.Torch].Name = "Torch";

        _data.ElementDefinitions[ElementIds.SpinningGun].Character = 24; // ↑
        _data.ElementDefinitions[ElementIds.SpinningGun].Cycle = 2;
        _data.ElementDefinitions[ElementIds.SpinningGun].TickProc = OnSpinningGunTick;
        _data.ElementDefinitions[ElementIds.SpinningGun].HasGetCharacterProc = true;
        _data.ElementDefinitions[ElementIds.SpinningGun].GetCharacterProc = OnSpinningGunDraw;
        _data.ElementDefinitions[ElementIds.SpinningGun].EditorCategory = Constants.CategoryCreature;
        _data.ElementDefinitions[ElementIds.SpinningGun].EditorShortcut = 'G';
        _data.ElementDefinitions[ElementIds.SpinningGun].Name = "Spinning gun";
        _data.ElementDefinitions[ElementIds.SpinningGun].Parameter1Name = "Intelligence?";
        _data.ElementDefinitions[ElementIds.SpinningGun].Parameter2Name = "Firing rate?";
        _data.ElementDefinitions[ElementIds.SpinningGun].ParameterBulletTypeName = "Firing type?";

        _data.ElementDefinitions[ElementIds.Ruffian].Character = 5; // ♣
        _data.ElementDefinitions[ElementIds.Ruffian].Color = 0x0D;
        _data.ElementDefinitions[ElementIds.Ruffian].Destructible = true;
        _data.ElementDefinitions[ElementIds.Ruffian].Pushable = true;
        _data.ElementDefinitions[ElementIds.Ruffian].Cycle = 1;
        _data.ElementDefinitions[ElementIds.Ruffian].TickProc = OnRuffianTick;
        _data.ElementDefinitions[ElementIds.Ruffian].TouchProc = OnDamagingTouch;
        _data.ElementDefinitions[ElementIds.Ruffian].EditorCategory = Constants.CategoryCreature;
        _data.ElementDefinitions[ElementIds.Ruffian].EditorShortcut = 'R';
        _data.ElementDefinitions[ElementIds.Ruffian].Name = "Ruffian";
        _data.ElementDefinitions[ElementIds.Ruffian].Parameter1Name = "Intelligence?";
        _data.ElementDefinitions[ElementIds.Ruffian].Parameter2Name = "Resting time?";
        _data.ElementDefinitions[ElementIds.Ruffian].ScoreValue = 2;

        _data.ElementDefinitions[ElementIds.Bear].Character = 153; // Ö
        _data.ElementDefinitions[ElementIds.Bear].Color = 0x06;
        _data.ElementDefinitions[ElementIds.Bear].Destructible = true;
        _data.ElementDefinitions[ElementIds.Bear].Pushable = true;
        _data.ElementDefinitions[ElementIds.Bear].Cycle = 3;
        _data.ElementDefinitions[ElementIds.Bear].TickProc = OnBearTick;
        _data.ElementDefinitions[ElementIds.Bear].TouchProc = OnDamagingTouch;
        _data.ElementDefinitions[ElementIds.Bear].EditorCategory = Constants.CategoryCreature;
        _data.ElementDefinitions[ElementIds.Bear].EditorShortcut = 'B';
        _data.ElementDefinitions[ElementIds.Bear].Name = "Bear";
        _data.ElementDefinitions[ElementIds.Bear].CategoryName = "Creatures";
        _data.ElementDefinitions[ElementIds.Bear].Parameter1Name = "Sensitivity?";
        _data.ElementDefinitions[ElementIds.Bear].ScoreValue = 1;

        _data.ElementDefinitions[ElementIds.Slime].Character = (byte)'*';
        _data.ElementDefinitions[ElementIds.Slime].Color = Constants.ColorChoiceOnBlack;
        _data.ElementDefinitions[ElementIds.Slime].Destructible = false;
        _data.ElementDefinitions[ElementIds.Slime].Cycle = 3;
        _data.ElementDefinitions[ElementIds.Slime].TickProc = OnSlimeTick;
        _data.ElementDefinitions[ElementIds.Slime].TouchProc = OnSlimeTouch;
        _data.ElementDefinitions[ElementIds.Slime].EditorCategory = Constants.CategoryCreature;
        _data.ElementDefinitions[ElementIds.Slime].EditorShortcut = 'V';
        _data.ElementDefinitions[ElementIds.Slime].Name = "Slime";
        _data.ElementDefinitions[ElementIds.Slime].Parameter2Name = "Movement speed?;FS";

        _data.ElementDefinitions[ElementIds.Shark].Character = (byte)'^';
        _data.ElementDefinitions[ElementIds.Shark].Color = 0x07;
        _data.ElementDefinitions[ElementIds.Shark].Destructible = false;
        _data.ElementDefinitions[ElementIds.Shark].Cycle = 3;
        _data.ElementDefinitions[ElementIds.Shark].TickProc = OnSharkTick;
        _data.ElementDefinitions[ElementIds.Shark].EditorCategory = Constants.CategoryCreature;
        _data.ElementDefinitions[ElementIds.Shark].EditorShortcut = 'Y';
        _data.ElementDefinitions[ElementIds.Shark].Name = "Shark";
        _data.ElementDefinitions[ElementIds.Shark].Parameter1Name = "Intelligence?";

        _data.ElementDefinitions[ElementIds.ConveyorCw].Character = (byte)'/';
        _data.ElementDefinitions[ElementIds.ConveyorCw].Cycle = 3;
        _data.ElementDefinitions[ElementIds.ConveyorCw].TickProc = OnConveyorCwTick;
        _data.ElementDefinitions[ElementIds.ConveyorCw].HasGetCharacterProc = true;
        _data.ElementDefinitions[ElementIds.ConveyorCw].GetCharacterProc = OnConveyorCwDraw;
        _data.ElementDefinitions[ElementIds.ConveyorCw].EditorCategory = Constants.CategoryItem;
        _data.ElementDefinitions[ElementIds.ConveyorCw].EditorShortcut = '1';
        _data.ElementDefinitions[ElementIds.ConveyorCw].Name = "Clockwise";
        _data.ElementDefinitions[ElementIds.ConveyorCw].CategoryName = "Conveyors";

        _data.ElementDefinitions[ElementIds.ConveyorCcw].Character = (byte)'\\';
        _data.ElementDefinitions[ElementIds.ConveyorCcw].Cycle = 2;
        _data.ElementDefinitions[ElementIds.ConveyorCcw].HasGetCharacterProc = true;
        _data.ElementDefinitions[ElementIds.ConveyorCcw].GetCharacterProc = OnConveyorCcwDraw;
        _data.ElementDefinitions[ElementIds.ConveyorCcw].TickProc = OnConveyorCcwTick;
        _data.ElementDefinitions[ElementIds.ConveyorCcw].EditorCategory = Constants.CategoryItem;
        _data.ElementDefinitions[ElementIds.ConveyorCcw].EditorShortcut = '2';
        _data.ElementDefinitions[ElementIds.ConveyorCcw].Name = "Counter";

        _data.ElementDefinitions[ElementIds.Solid].Character = 219; // █
        _data.ElementDefinitions[ElementIds.Solid].EditorCategory = Constants.CategoryTerrain;
        _data.ElementDefinitions[ElementIds.Solid].CategoryName = "Walls";
        _data.ElementDefinitions[ElementIds.Solid].EditorShortcut = 'S';
        _data.ElementDefinitions[ElementIds.Solid].Name = "Solid";

        _data.ElementDefinitions[ElementIds.Normal].Character = 178; // ▓
        _data.ElementDefinitions[ElementIds.Normal].EditorCategory = Constants.CategoryTerrain;
        _data.ElementDefinitions[ElementIds.Normal].EditorShortcut = 'N';
        _data.ElementDefinitions[ElementIds.Normal].Name = "Normal";

        _data.ElementDefinitions[ElementIds.Line].Character = 206; // ╬
        _data.ElementDefinitions[ElementIds.Line].HasGetCharacterProc = true;
        _data.ElementDefinitions[ElementIds.Line].GetCharacterProc = OnLineDraw;
        _data.ElementDefinitions[ElementIds.Line].Name = "Line";

        _data.ElementDefinitions[ElementIds.BlinkRayNs].Character = 186; // ║

        _data.ElementDefinitions[ElementIds.BlinkRayEw].Character = 205;  // ═

        _data.ElementDefinitions[ElementIds.Ricochet].Character = (byte)'*';
        _data.ElementDefinitions[ElementIds.Ricochet].Color = 0x0A;
        _data.ElementDefinitions[ElementIds.Ricochet].EditorCategory = Constants.CategoryTerrain;
        _data.ElementDefinitions[ElementIds.Ricochet].EditorShortcut = 'R';
        _data.ElementDefinitions[ElementIds.Ricochet].Name = "Ricochet";

        _data.ElementDefinitions[ElementIds.Breakable].Character = 177; // ▒
        _data.ElementDefinitions[ElementIds.Breakable].Destructible = false;
        _data.ElementDefinitions[ElementIds.Breakable].EditorCategory = Constants.CategoryTerrain;
        _data.ElementDefinitions[ElementIds.Breakable].EditorShortcut = 'B';
        _data.ElementDefinitions[ElementIds.Breakable].Name = "Breakable";

        _data.ElementDefinitions[ElementIds.Boulder].Character = 254; // ■
        _data.ElementDefinitions[ElementIds.Boulder].Pushable = true;
        _data.ElementDefinitions[ElementIds.Boulder].TouchProc = OnPushableTouch;
        _data.ElementDefinitions[ElementIds.Boulder].EditorCategory = Constants.CategoryTerrain;
        _data.ElementDefinitions[ElementIds.Boulder].EditorShortcut = 'O';
        _data.ElementDefinitions[ElementIds.Boulder].Name = "Boulder";

        _data.ElementDefinitions[ElementIds.SliderNs].Character = 18; // ┌
        _data.ElementDefinitions[ElementIds.SliderNs].TouchProc = OnPushableTouch;
        _data.ElementDefinitions[ElementIds.SliderNs].EditorCategory = Constants.CategoryTerrain;
        _data.ElementDefinitions[ElementIds.SliderNs].EditorShortcut = '1';
        _data.ElementDefinitions[ElementIds.SliderNs].Name = "Slider (NS)";

        _data.ElementDefinitions[ElementIds.SliderEw].Character = 29; // ↔
        _data.ElementDefinitions[ElementIds.SliderEw].TouchProc = OnPushableTouch;
        _data.ElementDefinitions[ElementIds.SliderEw].EditorCategory = Constants.CategoryTerrain;
        _data.ElementDefinitions[ElementIds.SliderEw].EditorShortcut = '2';
        _data.ElementDefinitions[ElementIds.SliderEw].Name = "Slider (EW)";

        _data.ElementDefinitions[ElementIds.Transporter].Character = 197; // ┼
        _data.ElementDefinitions[ElementIds.Transporter].TouchProc = OnTransporterTouch;
        _data.ElementDefinitions[ElementIds.Transporter].HasGetCharacterProc = true;
        _data.ElementDefinitions[ElementIds.Transporter].GetCharacterProc = OnTransporterDraw;
        _data.ElementDefinitions[ElementIds.Transporter].Cycle = 2;
        _data.ElementDefinitions[ElementIds.Transporter].TickProc = OnTransporterTick;
        _data.ElementDefinitions[ElementIds.Transporter].EditorCategory = Constants.CategoryTerrain;
        _data.ElementDefinitions[ElementIds.Transporter].EditorShortcut = 'T';
        _data.ElementDefinitions[ElementIds.Transporter].Name = "Transporter";
        _data.ElementDefinitions[ElementIds.Transporter].ParameterDirectionName = "Direction?";

        _data.ElementDefinitions[ElementIds.Pusher].Character = 16; // ►
        _data.ElementDefinitions[ElementIds.Pusher].Color = Constants.ColorChoiceOnBlack;
        _data.ElementDefinitions[ElementIds.Pusher].HasGetCharacterProc = true;
        _data.ElementDefinitions[ElementIds.Pusher].GetCharacterProc = OnPusherDraw;
        _data.ElementDefinitions[ElementIds.Pusher].Cycle = 4;
        _data.ElementDefinitions[ElementIds.Pusher].TickProc = OnPusherTick;
        _data.ElementDefinitions[ElementIds.Pusher].EditorCategory = Constants.CategoryCreature;
        _data.ElementDefinitions[ElementIds.Pusher].EditorShortcut = 'P';
        _data.ElementDefinitions[ElementIds.Pusher].Name = "Pusher";
        _data.ElementDefinitions[ElementIds.Pusher].ParameterDirectionName = "Push direction?";

        _data.ElementDefinitions[ElementIds.Bomb].Character = 11; // ♂
        _data.ElementDefinitions[ElementIds.Bomb].HasGetCharacterProc = true;
        _data.ElementDefinitions[ElementIds.Bomb].GetCharacterProc = OnBombDraw;
        _data.ElementDefinitions[ElementIds.Bomb].Pushable = true;
        _data.ElementDefinitions[ElementIds.Bomb].Cycle = 6;
        _data.ElementDefinitions[ElementIds.Bomb].TickProc = OnBombTick;
        _data.ElementDefinitions[ElementIds.Bomb].TouchProc = OnBombTouch;
        _data.ElementDefinitions[ElementIds.Bomb].EditorCategory = Constants.CategoryItem;
        _data.ElementDefinitions[ElementIds.Bomb].EditorShortcut = 'B';
        _data.ElementDefinitions[ElementIds.Bomb].Name = "Bomb";

        _data.ElementDefinitions[ElementIds.Energizer].Character = 127; // ⌂
        _data.ElementDefinitions[ElementIds.Energizer].Color = 0x05;
        _data.ElementDefinitions[ElementIds.Energizer].TouchProc = OnEnergizerTouch;
        _data.ElementDefinitions[ElementIds.Energizer].EditorCategory = Constants.CategoryItem;
        _data.ElementDefinitions[ElementIds.Energizer].EditorShortcut = 'E';
        _data.ElementDefinitions[ElementIds.Energizer].Name = "Energizer";

        _data.ElementDefinitions[ElementIds.BlinkWall].Character = 206; // ╬
        _data.ElementDefinitions[ElementIds.BlinkWall].Cycle = 1;
        _data.ElementDefinitions[ElementIds.BlinkWall].TickProc = OnBlinkWallTick;
        _data.ElementDefinitions[ElementIds.BlinkWall].HasGetCharacterProc = true;
        _data.ElementDefinitions[ElementIds.BlinkWall].GetCharacterProc = OnBlinkWallDraw;
        _data.ElementDefinitions[ElementIds.BlinkWall].EditorCategory = Constants.CategoryTerrain;
        _data.ElementDefinitions[ElementIds.BlinkWall].EditorShortcut = 'L';
        _data.ElementDefinitions[ElementIds.BlinkWall].Name = "Blink wall";
        _data.ElementDefinitions[ElementIds.BlinkWall].Parameter1Name = "Starting time";
        _data.ElementDefinitions[ElementIds.BlinkWall].Parameter2Name = "Period";
        _data.ElementDefinitions[ElementIds.BlinkWall].ParameterDirectionName = "Wall direction";

        _data.ElementDefinitions[ElementIds.Fake].Character = 178; // ▓
        _data.ElementDefinitions[ElementIds.Fake].EditorCategory = Constants.CategoryTerrain;
        _data.ElementDefinitions[ElementIds.Fake].PlaceableOnTop = true;
        _data.ElementDefinitions[ElementIds.Fake].Walkable = true;
        _data.ElementDefinitions[ElementIds.Fake].TouchProc = OnFakeTouch;
        _data.ElementDefinitions[ElementIds.Fake].EditorShortcut = 'A';
        _data.ElementDefinitions[ElementIds.Fake].Name = "Fake";

        _data.ElementDefinitions[ElementIds.Invisible].Character = 32;
        _data.ElementDefinitions[ElementIds.Invisible].EditorCategory = Constants.CategoryTerrain;
        _data.ElementDefinitions[ElementIds.Invisible].TouchProc = OnInvisibleTouch;
        _data.ElementDefinitions[ElementIds.Invisible].EditorShortcut = 'I';
        _data.ElementDefinitions[ElementIds.Invisible].Name = "Invisible";

        _data.ElementDefinitions[ElementIds.Object].Character = 2; // ☻
        _data.ElementDefinitions[ElementIds.Object].EditorCategory = Constants.CategoryCreature;
        _data.ElementDefinitions[ElementIds.Object].Cycle = 3;
        _data.ElementDefinitions[ElementIds.Object].HasGetCharacterProc = true;
        _data.ElementDefinitions[ElementIds.Object].GetCharacterProc = OnObjectDraw;
        _data.ElementDefinitions[ElementIds.Object].TickProc = OnObjectTick;
        _data.ElementDefinitions[ElementIds.Object].TouchProc = OnObjectTouch;
        _data.ElementDefinitions[ElementIds.Object].EditorShortcut = 'O';
        _data.ElementDefinitions[ElementIds.Object].Name = "Object";
        _data.ElementDefinitions[ElementIds.Object].Parameter1Name = "Character?";
        _data.ElementDefinitions[ElementIds.Object].ParameterTextName = "Edit Program";

        _data.ElementDefinitions[ElementIds.MessageTimer].TickProc = OnMessageTimerTick;

        _data.ElementDefinitions[ElementIds.BoardEdge].TouchProc = OnBoardEdgeTouch;

        _data.EditorPatterns = new List<byte>()
        {
            ElementIds.Solid
            , ElementIds.Normal
            , ElementIds.Breakable
            , ElementIds.Empty
            , ElementIds.Line
        };
    }

    private void InitializePreviousTiles()
    {
        for (var y = 0; y < Constants.BoardHeight + 2; y++)
        {
            for (var x = 0; x < Constants.BoardWidth + 2; x++)
            {
                _previousTiles[x, y] = (null, null);
            }
        }
    }

    private void LoadTiles(short boardId)
    {
        var x = 1;
        var y = 1;
        foreach (var runLengthEncodedTile in _data.World.Boards[boardId]!.RunLengthEncodedTiles)
        {
            for (var count = 0; count < runLengthEncodedTile.Count; count++)
            {
                _data.Board.Tiles[x++, y] = runLengthEncodedTile.Tile != null ? new Tile() { Element = runLengthEncodedTile.Tile.Element, Color = runLengthEncodedTile.Tile.Color } : new Tile();
                if (x > 60)
                {
                    x = 1;
                    y++;
                }
            }
        }
    }

    private IEnumerable<DataRunLengthEncodedTile> RunLengthEncodeTiles(Tile[,] tiles)
    {
        var runLengthEncodedTiles = new List<DataRunLengthEncodedTile>();
        if (tiles.GetLength(0) != Constants.BoardWidth + 2 || tiles.GetLength(1) != Constants.BoardHeight + 2)
        {
            throw new ArgumentException($"tiles must be an [{Constants.BoardWidth + 2},{Constants.BoardHeight + 2}] sized array.", nameof(tiles));
        }

        var runLengthEncodedTile = new DataRunLengthEncodedTile();
        var x = 1;
        var y = 1;
        runLengthEncodedTile.Count = 1;
        runLengthEncodedTile.Tile.Element = tiles[1, 1].Element;
        runLengthEncodedTile.Tile.Color = tiles[1, 1].Color;
        do
        {
            x++;
            if (x > Constants.BoardWidth)
            {
                x = 1;
                y++;
            }

            if (tiles[x, y].Color == runLengthEncodedTile.Tile.Color
                && tiles[x, y].Element == runLengthEncodedTile.Tile.Element
                && y <= Constants.BoardHeight
               )
            {
                runLengthEncodedTile.Count++;
            }
            else
            {
                runLengthEncodedTiles.Add(runLengthEncodedTile);
                runLengthEncodedTile = new DataRunLengthEncodedTile
                {
                    Count = 1
                    , Tile =
                    {
                        Element = tiles[x, y].Element
                        , Color = tiles[x, y].Color
                    }
                };
            }
        } while (y <= Constants.BoardHeight);

        return runLengthEncodedTiles;
    }

    private GameStatusElement? GetStatusElementAt(short x, short y)
    {
        return _data.Board.StatusElements.FirstOrDefault(s => s.Location.X == x && s.Location.Y == y);
    }

    // ä
    private void OnAmmoTouch(short x, short y, short sourceStatusElementId, ref short deltaX, ref short deltaY)
    {
        _data.World.Info.Ammo += 5;
        _data.Board.Tiles[x, y].Element = ElementIds.Empty;

        BoardDrawTile(x, y);
        UpdateSidebar();
        _sounds.Queue(2, 48, 1, 49, 1, 50, 1);

        if (_data.IsMessageAmmoNotShown)
        {
            _data.IsMessageAmmoNotShown = false;
            DisplayMessage(200, "Ammunition - 5 shots per container.");
        }
    }

    // Ö
    private void OnBearTick(short statusElementId)
    {
    }

    private char OnBlinkWallDraw(short x, short y)
    {
        return '╬';
    }

    private void OnBlinkWallTick(short statusElementId)
    {
        var stat = _data.Board.StatusElements[statusElementId];
        if (stat.Parameters[2] == 0)
        {
            stat.Parameters[2] = (byte)(stat.Parameters[0] + 1);
        }
        if (stat.Parameters[2] == 1)
        {
            var ix = (short)(stat.Location.X + stat.Step.X);
            var iy = (short)(stat.Location.Y + stat.Step.Y);

            var el = stat.Step.X != 0
                ? ElementIds.BlinkRayEw
                : ElementIds.BlinkRayNs;

            while (
                _data.Board.Tiles[ix, iy].Element == el
                && _data.Board.Tiles[ix, iy].Color == _data.Board.Tiles[stat.Location.X, stat.Location.Y].Color
            )
            {
                _data.Board.Tiles[ix, iy].Element = ElementIds.Empty;
                BoardDrawTile(ix, iy);
                ix += stat.Step.X;
                iy += stat.Step.Y;
                stat.Parameters[2] = (byte)(stat.Parameters[1] * 2 + 1);
            }

            if (stat.Parameters[2] == 1)
            {
                if (stat.Location.X + stat.Step.X == ix
                    && stat.Location.Y + stat.Step.Y == iy
                   )
                {
                    var hitBoundary = false;
                    do
                    {
                        if (_data.Board.Tiles[ix, iy].Element != ElementIds.Empty
                            && _data.ElementDefinitions[_data.Board.Tiles[ix, iy].Element].Destructible
                           )
                        {
                            BoardDamageTile(ix, iy);
                        }

                        if (_data.Board.Tiles[ix, iy].Element == ElementIds.Player)
                        {
                            var playerStatId = GetStatIdAt(ix, iy);
                            if (stat.Step.X != 0)
                            {
                                if (_data.Board.Tiles[ix, iy - 1].Element == ElementIds.Empty)
                                {
                                    MoveStat(playerStatId, ix, (short)(iy - 1));
                                }
                                else if (_data.Board.Tiles[ix, iy + 1].Element == ElementIds.Empty)
                                {
                                    MoveStat(playerStatId, ix, (short)(iy + 1));
                                }
                            }
                            else
                            {
                                if (_data.Board.Tiles[ix + 1, iy].Element == ElementIds.Empty)
                                {
                                    MoveStat(playerStatId, (short)(ix + 1), iy);
                                }
                                else if (_data.Board.Tiles[ix - 1, iy].Element == ElementIds.Empty)
                                {
                                    MoveStat(playerStatId, (short)(ix - 1), iy);
                                }
                            }
                        }

                        if (_data.Board.Tiles[ix, iy].Element == ElementIds.Empty)
                        {
                            _data.Board.Tiles[ix, iy].Element = (byte)el;
                            _data.Board.Tiles[ix, iy].Color = _data.Board.Tiles[stat.Location.X, stat.Location.Y].Color;
                            BoardDrawTile(ix, iy);
                        }
                        else
                        {
                            hitBoundary = true;
                        }

                        ix += stat.Step.X;
                        iy += stat.Step.Y;
                    } while (false == hitBoundary);

                    stat.Parameters[2] = (byte)(stat.Parameters[1] * 2 + 1);
                }

            }
            else
            {
                stat.Parameters[2]++;
            }
        }
        else
        {
            stat.Parameters[2]--;
        }
    }

    private void OnBoardEdgeTouch(short x, short y, short sourceStatusElementId, ref short deltaX, ref short deltaY)
    {
        short neighborId;
        var entryX = _data.Board.StatusElements[0].Location.X;
        var entryY = _data.Board.StatusElements[0].Location.Y;

        if (deltaY == -1)
        {
            neighborId = 0;
            entryY = Constants.BoardHeight;
        }
        else if (deltaY == 1)
        {
            neighborId = 1;
            entryY = 1;
        }
        else if (deltaX == -1)
        {
            neighborId = 2;
            entryX = Constants.BoardWidth;
        }
        else
        {
            neighborId = 3;
            entryX = 1;
        }

        if (_data.Board.Properties.NeighborBoards[neighborId] != 0)
        {
            var boardId = _data.World.Info.CurrentBoard;
            BoardChange(_data.Board.Properties.NeighborBoards[neighborId]);
            if (_data.Board.Tiles[entryX, entryY].Element != ElementIds.Player)
            {
                _data.ElementDefinitions[_data.Board.Tiles[entryX, entryY].Element].TouchProc!(entryX, entryY, sourceStatusElementId, ref _input.DeltaX, ref _input.DeltaY);
            }

            if (_data.ElementDefinitions[_data.Board.Tiles[entryX, entryY].Element].Walkable
                || _data.Board.Tiles[entryX, entryY].Element == ElementIds.Player
               )
            {
                if (_data.Board.Tiles[entryX, entryY].Element != ElementIds.Player)
                {
                    MoveStat(0, entryX, entryY);
                }

                TransitionDrawBoardChange();
                deltaX = 0;
                deltaY = 0;
                BoardEnter();
            }
            else
            {
                BoardChange(boardId);
            }
        }

    }

    private char OnBombDraw(short x, short y)
    {
        return '?';
    }

    private void OnBombTick(short statusElementId)
    {
    }

    private void OnBombTouch(short x, short y, short sourceStatusElementId, ref short deltaX, ref short deltaY)
    {
    }

    // °
    private void OnBulletTick(short statusElementId)
    {
        var stat = _data.Board.StatusElements[statusElementId];

        var firstTry = true;

        TryMove:
        var ix = (short)(stat.Location.X + stat.Step.X);
        var iy = (short)(stat.Location.Y + stat.Step.Y);
        var iElem = _data.Board.Tiles[ix, iy].Element;

        if (_data.ElementDefinitions[iElem].Walkable || iElem == ElementIds.Water)
        {
            MoveStat(statusElementId, ix, iy);
            return;
        }

        if (iElem == ElementIds.Ricochet && firstTry)
        {
            stat.Step.X = (short)-stat.Step.X;
            stat.Step.Y = (short)-stat.Step.Y;
            _sounds.Queue(1, 249, 1);
            firstTry = false;
            goto TryMove;
        }

        if (iElem == ElementIds.Breakable
            || (_data.ElementDefinitions[iElem].Destructible
                && (iElem == ElementIds.Player
                    || stat.Parameters[0] == 0
                    )
                )
            )
        {
            if (_data.ElementDefinitions[iElem].ScoreValue != 0)
            {
                _data.World.Info.Score += _data.ElementDefinitions[iElem].ScoreValue;
                UpdateSidebar();
            }

            BoardAttack(statusElementId, ix, iy);
            return;
        }

        if (_data.Board.Tiles[stat.Location.X + stat.Step.Y, stat.Location.Y + stat.Step.X].Element == ElementIds.Ricochet && firstTry)
        {
            ix = stat.Step.X;
            stat.Step.X = (short)-stat.Step.Y;
            stat.Step.Y = (short)-ix;
            _sounds.Queue(1, 249, 1);
            firstTry = false;
            goto TryMove;
        }

        if (_data.Board.Tiles[stat.Location.X - stat.Step.Y, stat.Location.Y - stat.Step.X].Element == ElementIds.Ricochet && firstTry)
        {
            ix = stat.Step.X;
            stat.Step.X = stat.Step.Y;
            stat.Step.Y = ix;
            _sounds.Queue(1, 249, 1);
            firstTry = false;
            goto TryMove;
        }

        RemoveStat(statusElementId);
        _data.CurrentStatTicked--;
        if (iElem is ElementIds.Object or ElementIds.Scroll)
        {
            var iStat = GetStatIdAt(ix, iy);
            _oop.Send((short)-iStat, "SHOT", false);
        }
    }

    // Θ
    private void OnCentipedeHeadTick(short statId)
    {
        var stat = _data.Board.StatusElements[statId];
        var player = _data.Board.StatusElements[0];
        if (stat.Location.X == player.Location.X && _random.Next(10) < stat.Parameters[0])
        {
            stat.Step.Y = Signum((short)(player.Location.Y - stat.Location.Y));
            stat.Step.X = 0;
        }
        else if (stat.Location.Y == player.Location.Y && _random.Next(10) < stat.Parameters[0])
        {
            stat.Step.X = Signum((short)(player.Location.X - stat.Location.X));
            stat.Step.Y = 0;
        }
        else if (_random.Next(10) < stat.Parameters[1] || (stat.Step.X == 0 && stat.Step.Y == 0))
        {
            var randomDirection = CalcDirectionRnd();
            stat.Step.X = randomDirection.X;
            stat.Step.Y = randomDirection.Y;
        }

        if (false == _data.ElementDefinitions[_data.Board.Tiles[stat.Location.X + stat.Step.X, stat.Location.Y + stat.Step.Y].Element].Walkable
            && _data.Board.Tiles[stat.Location.X + stat.Step.X, stat.Location.Y + stat.Step.Y].Element != ElementIds.Player
           )
        {
            var ix = stat.Step.X;
            var iy = stat.Step.Y;
            var temp = (short)((_random.Next(2) * 2 - 1) * stat.Step.Y);
            stat.Step.Y = (short)((_random.Next(2) * 2 - 1) * stat.Step.X);
            stat.Step.X = temp;
            if (false == _data.ElementDefinitions[_data.Board.Tiles[stat.Location.X + stat.Step.X, stat.Location.Y + stat.Step.Y].Element].Walkable
                && _data.Board.Tiles[stat.Location.X + stat.Step.X, stat.Location.Y + stat.Step.Y].Element != ElementIds.Player
               )
            {
                stat.Step.X = (short)-stat.Step.X;
                stat.Step.Y = (short)-stat.Step.Y;
                if (false == _data.ElementDefinitions[_data.Board.Tiles[stat.Location.X + stat.Step.X, stat.Location.Y + stat.Step.Y].Element].Walkable
                    && _data.Board.Tiles[stat.Location.X + stat.Step.X, stat.Location.Y + stat.Step.Y].Element != ElementIds.Player
                   )
                {
                    if (_data.ElementDefinitions[_data.Board.Tiles[stat.Location.X - ix, stat.Location.Y - iy].Element].Walkable
                        || _data.Board.Tiles[stat.Location.X - ix, stat.Location.Y - iy].Element == ElementIds.Player
                       )
                    {
                        stat.Step.X = (short)-ix;
                        stat.Step.Y = (short)-iy;
                    }
                    else
                    {
                        stat.Step.X = 0;
                        stat.Step.Y = 0;
                    }
                }
            }
        }

        if (stat.Step.X == 0 && stat.Step.Y == 0)
        {
            _data.Board.Tiles[stat.Location.X, stat.Location.Y].Element = ElementIds.CentipedeSegment;
            stat.Leader = -1;
            while (_data.Board.StatusElements[statId].Follower > 0)
            {
                var temp = _data.Board.StatusElements[statId].Follower;
                _data.Board.StatusElements[statId].Follower = _data.Board.StatusElements[statId].Leader;
                _data.Board.StatusElements[statId].Leader = temp;
                statId = temp;
            }
            _data.Board.StatusElements[statId].Follower = _data.Board.StatusElements[statId].Leader;
            _data.Board.Tiles[_data.Board.StatusElements[statId].Location.X, _data.Board.StatusElements[statId].Location.Y].Element = ElementIds.CentipedeHead;
        }
        else if (_data.Board.Tiles[stat.Location.X + stat.Step.X, stat.Location.Y + stat.Step.Y].Element == ElementIds.Player)
        {
            if (stat.Follower != -1)
            {
                _data.Board.Tiles[_data.Board.StatusElements[stat.Follower].Location.X, _data.Board.StatusElements[stat.Follower].Location.Y].Element = ElementIds.CentipedeHead;
                _data.Board.StatusElements[stat.Follower].Step.X = stat.Step.X;
                _data.Board.StatusElements[stat.Follower].Step.Y = stat.Step.Y;
                BoardDrawTile(_data.Board.StatusElements[stat.Follower].Location.X, _data.Board.StatusElements[stat.Follower].Location.Y);
            }
            BoardAttack(statId, (short)(stat.Location.X + stat.Step.X), (short)(stat.Location.Y + stat.Step.Y));
        }
        else
        {
            MoveStat(statId, (short)(stat.Location.X + stat.Step.X), (short)(stat.Location.Y + stat.Step.Y));
            do
            {
                stat = _data.Board.StatusElements[statId];
                var tx = (short)(stat.Location.X - stat.Step.X);
                var ty = (short)(stat.Location.Y - stat.Step.Y);
                var ix = stat.Step.X;
                var iy = stat.Step.Y;
                if (stat.Follower < 0)
                {
                    if (_data.Board.Tiles[tx - ix, ty - iy].Element == ElementIds.CentipedeSegment
                        && GetStatusElementAt((short)(tx - ix), (short)(ty - iy))!.Leader < 0
                        )
                    {
                        stat.Follower = GetStatIdAt((short)(tx - ix), (short)(ty - iy));
                    }
                    else if (_data.Board.Tiles[tx - iy, ty - ix].Element == ElementIds.CentipedeSegment
                             && GetStatusElementAt((short)(tx - iy), (short)(ty - ix))!.Leader < 0
                            )
                    {
                        stat.Follower = GetStatIdAt((short)(tx - iy), (short)(ty - ix));
                    }
                    else if (_data.Board.Tiles[tx + iy, ty + ix].Element == ElementIds.CentipedeSegment
                             && GetStatusElementAt((short)(tx + iy), (short)(ty + ix))!.Leader < 0
                            )
                    {
                        stat.Follower = GetStatIdAt((short)(tx + iy), (short)(ty + ix));
                    }
                }

                if (stat.Follower > 0)
                {
                    _data.Board.StatusElements[stat.Follower].Leader = statId;
                    _data.Board.StatusElements[stat.Follower].Parameters[0] = stat.Parameters[0];
                    _data.Board.StatusElements[stat.Follower].Parameters[1] = stat.Parameters[1];
                    _data.Board.StatusElements[stat.Follower].Step.X = (short)(tx - _data.Board.StatusElements[stat.Follower].Location.X);
                    _data.Board.StatusElements[stat.Follower].Step.Y = (short)(ty - _data.Board.StatusElements[stat.Follower].Location.Y);
                    MoveStat(stat.Follower, tx, ty);
                }
                statId = stat.Follower;
            } while (statId != -1);
        }
    }

    // O
    private void OnCentipedeSegmentTick(short statusElementId)
    {
        var stat = _data.Board.StatusElements[statusElementId];
        if (stat.Leader < 0)
        {
            if (stat.Leader < -1)
            {
                _data.Board.Tiles[stat.Location.X, stat.Location.Y].Element = ElementIds.CentipedeHead;
            }
            else
            {
                stat.Leader--;
            }
        }
    }

    private void OnConveyorTick(short x, short y, short direction)
    {
        short iMin;
        short iMax;
        var tiles = new Tile[8];
        for (var index = 0; index < tiles.Length; index++)
        {
            tiles[index] = new Tile();
        }
        if (direction == 1)
        {
            iMin = 0;
            iMax = 8;
        }
        else
        {
            iMin = 7;
            iMax = -1;
        }

        var canMove = true;
        var i = iMin;
        do
        {
            tiles[i].Element = _data.Board.Tiles[x + _diagonalDeltas[i].X, y + _diagonalDeltas[i].Y].Element;
            tiles[i].Color = _data.Board.Tiles[x + _diagonalDeltas[i].X, y + _diagonalDeltas[i].Y].Color;
            if (tiles[i].Element == ElementIds.Empty)
            {
                canMove = true;
            }
            else if (false == _data.ElementDefinitions[tiles[i].Element].Pushable)
            {
                canMove = false;
            }

            i += direction;
        } while (i != iMax);

        i = iMin;
        do
        {
            var tile = tiles[i];
            if (canMove)
            {
                if (_data.ElementDefinitions[tile.Element].Pushable)
                {
                    var ix = (short)(x + _diagonalDeltas[(i - direction + 8) % 8].X);
                    var iy = (short)(y + _diagonalDeltas[(i - direction + 8) % 8].Y);
                    if (_data.ElementDefinitions[tile.Element].Cycle > -1)
                    {
                        var tmpTile = new Tile()
                        {
                            Element = _data.Board.Tiles[x + _diagonalDeltas[i].X, y + _diagonalDeltas[i].Y].Element
                            , Color = _data.Board.Tiles[x + _diagonalDeltas[i].X, y + _diagonalDeltas[i].Y].Color
                        };
                        var iStat = GetStatIdAt((short)(x + _diagonalDeltas[i].X), (short)(y + _diagonalDeltas[i].Y));
                        _data.Board.Tiles[x + _diagonalDeltas[i].X, y + _diagonalDeltas[i].Y].Element = tiles[i].Element;
                        _data.Board.Tiles[x + _diagonalDeltas[i].X, y + _diagonalDeltas[i].Y].Color = tiles[i].Color;
                        _data.Board.Tiles[ix, iy].Element = ElementIds.Empty;
                        MoveStat(iStat, ix, iy);
                        _data.Board.Tiles[x + _diagonalDeltas[i].X, y + _diagonalDeltas[i].Y].Element = tmpTile.Element;
                        _data.Board.Tiles[x + _diagonalDeltas[i].X, y + _diagonalDeltas[i].Y].Color = tmpTile.Color;
                    }
                    else
                    {
                        _data.Board.Tiles[ix, iy].Element = tiles[i].Element;
                        _data.Board.Tiles[ix, iy].Color = tiles[i].Color;
                        BoardDrawTile(ix, iy);
                    }

                    if (false == _data.ElementDefinitions[tiles[(i + direction + 8) % 8].Element].Pushable)
                    {
                        _data.Board.Tiles[x + _diagonalDeltas[i].X, y + _diagonalDeltas[i].Y].Element = ElementIds.Empty;
                        BoardDrawTile((short)(x + _diagonalDeltas[i].X), (short)(y + _diagonalDeltas[i].Y));
                    }
                }
                else
                {
                    canMove = false;
                }
            }
            else if (tile.Element == ElementIds.Empty)
            {
                canMove = true;
            }
            else if (false == _data.ElementDefinitions[tile.Element].Pushable)
            {
                canMove = false;
            }
            i += direction;
        } while (i != iMax);
    }

    // \ ─ / │ 
    private char OnConveyorCcwDraw(short x, short y)
    {
        switch ((_data.CurrentTick / _data.ElementDefinitions[ElementIds.ConveyorCcw].Cycle) % 4)
        {
            case 3: return '│';
            case 2: return '/';
            case 1: return '─';
            default: return '\\';
        }
    }

    // \ ─ / │ 
    private void OnConveyorCcwTick(short statusElementId)
    {
        var stat = _data.Board.StatusElements[statusElementId];
        BoardDrawTile(stat.Location.X, stat.Location.Y);
        OnConveyorTick(stat.Location.X, stat.Location.Y, -1);
    }

    // │ / ─ \
    private char OnConveyorCwDraw(short x, short y)
    {
        switch ((_data.CurrentTick / _data.ElementDefinitions[ElementIds.ConveyorCw].Cycle) % 4)
        {
            case 0: return '│';
            case 1: return '/';
            case 2: return '─';
            default: return '\\';
        }
    }

    // │ / ─ \
    private void OnConveyorCwTick(short statusElementId)
    {
        var stat = _data.Board.StatusElements[statusElementId];
        BoardDrawTile(stat.Location.X, stat.Location.Y);
        OnConveyorTick(stat.Location.X, stat.Location.Y, 1);
    }

    private void OnDamagingTouch(short x, short y, short sourceStatusElementId, ref short deltaX, ref short deltaY)
    {
        BoardAttack(sourceStatusElementId, x, y);
    }

    // ◙
    private void OnDoorTouch(short x, short y, short sourceStatusElementId, ref short deltaX, ref short deltaY)
    {
        var key = (_data.Board.Tiles[x, y].Color / 16) % 8;

        if (true /*_data.World.Info.Keys[key]*/) // TODO - remove debu true
        {
            _data.Board.Tiles[x, y].Element = ElementIds.Empty;
            BoardDrawTile(x, y);
            _data.World.Info.Keys[key] = false;
            UpdateSidebar();
            DisplayMessage(200, $"The {ColorNames[key - 1]} door is now open.");
            _sounds.Queue(3,  48, 1, 55, 1, 59, 1, 48, 1, 55, 1, 59, 1, 64, 4);
        }
        else
        {
            DisplayMessage(200, $"The {ColorNames[key - 1]} door is locked!");
            _sounds.Queue(3, 23, 1, 16, 1);
        }
    }

    private char OnDuplicatorDraw(short x, short y)
    {
        switch (GetStatusElementAt(x, y)?.Parameters[0])
        {
            case 1: return '·';
            case 2: return '∙';
            case 3: return '°';
            case 4: return 'o';
            case 5: return 'O';
            default: return '·';
        }
    }

    // · ∙ ° o O
    private void OnDuplicatorTick(short statusElementId)
    {
        var stat = _data.Board.StatusElements[statusElementId];
        var (stepX, stepY) = (stat.Step.X, stat.Step.Y);
        var (x, y) = (stat.Location.X, stat.Location.Y);
        if (stat.Parameters[0] <= 4)
        {
            stat.Parameters[0]++;
            BoardDrawTile(x, y);
        }
        else
        {
            stat.Parameters[0] = 0;
            if (_data.Board.Tiles[x - stepX, y - stepY].Element == ElementIds.Player)
            {
                _data.ElementDefinitions[_data.Board.Tiles[x + stepX, y + stepY].Element]
                    .TouchProc?.Invoke((short)(x + stepX), (short)(y + stepY), 0, ref _input.DeltaX, ref _input.DeltaY);
            }
            else
            {
                if (_data.Board.Tiles[x - stepX, y - stepY].Element != ElementIds.Empty)
                {
                    PushablePush((short)(x - stepX), (short)(y - stepY), (short)-stepX, (short)-stepY);
                }

                if (_data.Board.Tiles[x - stepX, y - stepY].Element == ElementIds.Empty)
                {
                    var sourceStatId = GetStatIdAt((short)(x + stepX), (short)(y + stepY));
                    if (sourceStatId > 0)
                    {
                        if (_data.Board.StatusElements.Count < Constants.MaximumStatusElements + 24)
                        {
                            AddStat(
                                (short)(x - stepX), (short)(y - stepY)
                                , _data.Board.Tiles[x + stepX, y + stepY].Element
                                , _data.Board.Tiles[x + stepX, y + stepY].Color
                                , _data.Board.StatusElements[sourceStatId].Cycle
                                , _data.Board.StatusElements[sourceStatId]
                            );
                            BoardDrawTile((short)(x - stepX), (short)(y - stepY));
                        }
                    }
                    else if (sourceStatId != 0)
                    {
                        _data.Board.Tiles[x - stepX, y - stepY] = _data.Board.Tiles[x + stepX, y + stepY];
                        BoardDrawTile((short)(x - stepX), (short)(y - stepY));
                    }
                    _sounds.Queue(3, 48, 2, 50, 2, 52, 2, 53, 2, 55, 2);
                }
                else
                {
                    _sounds.Queue(3, 24, 1, 22, 1);
                }
            }
            stat.Parameters[0] = 0;
            BoardDrawTile(x, y);
        }
        stat.Cycle = (short)((9 - stat.Parameters[1]) * 3);
    }

    private void OnEnergizerTouch(short x, short y, short sourceStatusElementId, ref short deltaX, ref short deltaY)
    {
    }

    private void OnFakeTouch(short x, short y, short sourceStatusElementId, ref short deltaX, ref short deltaY)
    {
        if (_data.IsMessageFakeNotShown)
        {
            DisplayMessage(150, "A fake wall - secret passage!");
        }
        _data.IsMessageFakeNotShown = false;
    }

    // ░
    private void OnForestTouch(short x, short y, short sourceStatusElementId, ref short deltaX, ref short deltaY)
    {
        _data.Board.Tiles[x, y].Element = ElementIds.Empty;
        BoardDrawTile(x, y);

        _sounds.Queue(3, 57, 1);

        if (_data.IsMessageForestNotShown)
        {
            DisplayMessage(200, "A path is cleared through the forest.");
        }

        _data.IsMessageForestNotShown = false;
    }

    // ♦
    private void OnGemTouch(short x, short y, short sourceStatusElementId, ref short deltaX, ref short deltaY)
    {
        _data.World.Info.Gems++;
        _data.World.Info.Health++;
        _data.World.Info.Score += 10;

        _data.Board.Tiles[x, y].Element = ElementIds.Empty;
        UpdateSidebar();
        _sounds.Queue(2, 64, 1, 55, 1, 52, 1, 48, 1);

        if (_data.IsMessageGemNotShown)
        {
            _data.IsMessageGemNotShown = false;
            DisplayMessage(200, "Gems give you Health!");
        }
    }

    private void OnInvisibleTouch(short x, short y, short sourceStatusElementId, ref short deltaX, ref short deltaY)
    {
        _data.Board.Tiles[x, y].Element = ElementIds.Normal;
        BoardDrawTile(x, y);

        _sounds.Queue(3, 18, 1, 16, 1);
        DisplayMessage(100, "You are blocked by an invisible wall.");
    }

    // ♀
    private void OnKeyTouch(short x, short y, short sourceStatusElementId, ref short deltaX, ref short deltaY)
    {
        var key = _data.Board.Tiles[x, y].Color % 8;

        if (_data.World.Info.Keys[key])
        {
            DisplayMessage(200, $"You already have a {ColorNames[key - 1]} key!");
        }
        else
        {
            _data.World.Info.Keys[key] = true;
            _data.Board.Tiles[x, y].Element = ElementIds.Empty;
            UpdateSidebar();
            DisplayMessage(200, $"You now have the {ColorNames[key - 1]} key.");
            _sounds.Queue(2, 64, 1, 68, 1, 71, 1, 64, 1, 68, 1, 71, 1, 64, 1, 68, 1, 71, 1, 80, 2);
        }
    }

    private char OnLineDraw(short x, short y)
    {
        var line = 0;
        var value = 1;
        foreach (var delta in _neighborDeltas)
        {
            var element = _data.Board.Tiles[x + delta.X, y + delta.Y].Element;
            if (element == ElementIds.Line || element == ElementIds.BoardEdge)
            {
                line += value;
            }
            value *= 2;
        }

        return _lineDrawCharacters[line];
    }

    // α
    private void OnLionTick(short statusElementId)
    {
        Coordinate delta;
        var stat = _data.Board.StatusElements[statusElementId];
        
        if (stat.Parameters[0] < _random.Next(10))
        {
            delta = CalcDirectionRnd();
        }
        else
        {
            delta = CalcDirectionSeek(stat.Location.X, stat.Location.Y);
        }

        if (_data.ElementDefinitions[_data.Board.Tiles[stat.Location.X + delta.X, stat.Location.Y + delta.Y].Element].Walkable)
        {
            MoveStat(statusElementId, (short)(stat.Location.X + delta.X), (short)(stat.Location.Y + delta.Y));
        }
        else if (_data.Board.Tiles[stat.Location.X + delta.X, stat.Location.Y + delta.Y].Element == ElementIds.Player)
        {
            BoardAttack(statusElementId, (short)(stat.Location.X + delta.X), (short)(stat.Location.Y + delta.Y));
        }
    }

    private void OnMessageTimerTick(short statusElementId)
    {
        var stat = _data.Board.StatusElements[statusElementId];
        if (stat.Location.X == 0)
        {
            _consoleInteractor.DrawText((short)((Constants.BoardWidth - _data.Board.Properties.Message.Length) / 2), Constants.BoardHeight - 1, $" {_data.Board.Properties.Message} ", (byte)(9 + (stat.Parameters[1] % 7)));
            stat.Parameters[1]--;
            if (stat.Parameters[1] <= 0)
            {
                RemoveStat(statusElementId);
                _data.CurrentStatTicked--;
                BoardDrawBorder();
                _data.Board.Properties.Message = "";
            }
        }
    }

    private void OnMonitorTick(short statusElementId)
    {
        if ($"{(char)27}AEHNPQRSW|".Contains(char.ToUpperInvariant(_input.KeyPressed.KeyChar)))
        {
            _data.IsGamePlayExitRequested = true;
        }
    }

    private char OnObjectDraw(short x, short y)
    {
        var statusElement = GetStatusElementAt(x, y);
        return (
            statusElement != null
                ? _characters[statusElement.Parameters[0]]
                : _characters[_data.Board.Tiles[x, y].Element]
        );
    }

    private void OnObjectTick(short statusElementId)
    {
        var stat = _data.Board.StatusElements[statusElementId];
        if (stat.CodePosition >= 0)
        {
            _oop.Execute(statusElementId, ref stat.CodePosition, "Interaction");
        }

        if (stat.Step.X != 0 || stat.Step.Y != 0)
        {
            if (_data.ElementDefinitions[_data.Board.Tiles[stat.Location.X + stat.Step.X, stat.Location.Y + stat.Step.Y].Element].Walkable)
            {
                MoveStat(statusElementId, (short)(stat.Location.X + stat.Step.X), (short)(stat.Location.Y + stat.Step.Y));
            }
            else
            {
                _oop.Send((short)-statusElementId, "THUD", false);
            }
        }
    }

    private void OnObjectTouch(short x, short y, short sourceStatusElementId, ref short deltaX, ref short deltaY)
    {
        var statId = GetStatIdAt(x, y);
        _oop.Send((short)-statId, "TOUCH", false);
    }

    // ≡
    private void OnPassageTouch(short x, short y, short sourceStatusElementId, ref short deltaX, ref short deltaY)
    {
        BoardPassageTeleport(x, y);
        deltaX = 0;
        deltaY = 0;
    }

    // ☻
    private void OnPlayerTick(short statusElementId)
    {
        var stat = _data.Board.StatusElements[statusElementId];
        if (_data.World.Info.EnergizerTicks > 0)
        {
            _data.ElementDefinitions[ElementIds.Player].Character = (byte)
                (_data.ElementDefinitions[ElementIds.Player].Character == 2
                        ? 1 /* ☺ */
                        : 2 /* ☻ */
                );
            _data.Board.Tiles[stat.Location.X, stat.Location.Y].Color = (byte)
                (_data.CurrentTick % 2 != 0
                    ? 0x0F
                    : (_data.CurrentTick % 7 + 1) * 16 + 0x0F
                );
        }
        else if (_data.Board.Tiles[stat.Location.X, stat.Location.Y].Color != 0x1f || _data.ElementDefinitions[ElementIds.Player].Character != 2/* ☻ */)
        {
            _data.Board.Tiles[stat.Location.X, stat.Location.Y].Color = 0x1F;
            _data.ElementDefinitions[ElementIds.Player].Character = 2/* ☻ */;
        }
        BoardDrawTile(stat.Location.X, stat.Location.Y);

        if (_data.World.Info.Health <= 0)
        {
            _input.DeltaX = 0;
            _input.DeltaY = 0;

            if (GetStatIdAt(0, 0) == -1)
            {
                DisplayMessage(32000, " Game over  -  Press ESCAPE");
            }

            _data.TickTimeDuration = 0;
            _sounds.BlockQueueing = true;
        }

        if (_input.WasShiftPressedWithLastKey() || _input.KeyPressed.Key == ConsoleKey.Spacebar)
        {
            if (_input.WasShiftPressedWithLastKey() && (_input.DeltaX != 0 || _input.DeltaY != 0))
            {
                _data.PlayerDirection.X = _input.DeltaX;
                _data.PlayerDirection.Y = _input.DeltaY;
            }

            if (_data.PlayerDirection.X != 0 || _data.PlayerDirection.Y != 0)
            {
                if (_data.Board.Properties.MaxShots == 0)
                {
                    if (_data.IsMessageNoShootingNotShown)
                    {
                        DisplayMessage(200, "Can't shoot in this place!");
                    }
                    _data.IsMessageNoShootingNotShown = false;
                }
                else if (_data.World.Info.Ammo == 0)
                {
                    if (_data.IsMessageOutOfAmmoNotShown)
                    {
                        DisplayMessage(200, "You don't have any ammo!");
                    }
                    _data.IsMessageOutOfAmmoNotShown = false;
                }
                else
                {
                    var bulletCount = _data.Board.StatusElements
                        .Count(s => _data.Board.Tiles[s.Location.X, s.Location.Y].Element == ElementIds.Bullet
                                    && s.Parameters[0] == Constants.ShotSourcePlayer
                              );
                    if (bulletCount < _data.Board.Properties.MaxShots)
                    {
                        if (BoardShoot(ElementIds.Bullet, stat.Location.X, stat.Location.Y, _data.PlayerDirection.X, _data.PlayerDirection.Y, Constants.ShotSourcePlayer))
                        {
                            _data.World.Info.Ammo--;
                            UpdateSidebar();
                            _sounds.Queue(2, 64, 1, 48, 1, 32, 1);
                            _input.DeltaX = 0;
                            _input.DeltaY = 0;
                        }
                    }
                }
            }
        }
        else if (_input.DeltaX != 0 || _input.DeltaY != 0)
        {
            _data.PlayerDirection.X = _input.DeltaX;
            _data.PlayerDirection.Y = _input.DeltaY;
            _data.ElementDefinitions[_data.Board.Tiles[stat.Location.X + _input.DeltaX, stat.Location.Y + _input.DeltaY].Element].TouchProc!(
                (short)(stat.Location.X + _input.DeltaX), (short)(stat.Location.Y + _input.DeltaY), 0, ref _input.DeltaX, ref _input.DeltaY
            );
            if (_input.DeltaX != 0 || _input.DeltaY != 0)
            {
                if (_sounds.IsEnabled && false == _sounds.IsPlaying)
                {
                    _sounds.Sound(110);
                }

                if (_data.ElementDefinitions[_data.Board.Tiles[stat.Location.X + _input.DeltaX, stat.Location.Y + _input.DeltaY].Element].Walkable)
                {
                    if (_sounds.IsEnabled && false == _sounds.IsPlaying)
                    {
                        _sounds.NoSound();
                    }
                    MoveStat(0, (short)(stat.Location.X + _input.DeltaX), (short)(stat.Location.Y + _input.DeltaY));
                }
                else if (_sounds.IsEnabled && false == _sounds.IsPlaying)
                {
                    _sounds.NoSound();
                }
            }
        }

        switch (_input.KeyPressed.Key)
        {
            case ConsoleKey.T:
                if (_data.World.Info.TorchTicks <= 0)
                {
                    if (_data.World.Info.Torches > 0)
                    {
                        if (_data.Board.Properties.IsDark)
                        {
                            _data.World.Info.Torches--;
                            _data.World.Info.TorchTicks = Constants.TorchDuration;
                            DrawPlayerSurroundings(stat.Location.X, stat.Location.Y, 0);
                            UpdateSidebar();
                        }
                        else
                        {
                            if (_data.IsMessageRoomNotDarkNotShown)
                            {
                                DisplayMessage(200, "Don't need torch - room is not dark!");
                                _data.IsMessageRoomNotDarkNotShown = false;
                            }
                        }
                    }
                    else
                    {
                        if (_data.IsMessageOutOfTorchesNotShown)
                        {
                            DisplayMessage(200, "You don't have any torches!");
                            _data.IsMessageOutOfTorchesNotShown = false;
                        }
                    }
                }
                break;
            case ConsoleKey.Escape:
            case ConsoleKey.Q:
                GamePromptEndPlay();
                break;
            case ConsoleKey.S:
                //TODO: GameWorldSave("Save game:", _data.SavedGameFileName, ".SAV");
                break;
            case ConsoleKey.P:
                if (_data.World.Info.Health > 0)
                {
                    _data.IsGamePaused = true;
                }
                break;
            case ConsoleKey.B:
                _sounds.IsEnabled = !_sounds.IsEnabled;
                _sounds.ClearQueue();
                UpdateSidebar();
                _input.KeyPressed = new ConsoleKeyInfo(' ', ConsoleKey.Spacebar, false, false, false);
                break;
            case ConsoleKey.H:
                _textWindow.DisplayFile("GAME.HLP", "Playing ZZT");
                break;
            case ConsoleKey.F:
                _textWindow.DisplayFile("ORDER.HLP", "Order form");
                break;
        }
        if (_input.KeyPressed.KeyChar == '?')
        {
            //TODO: GameDebugPrompt();
            _input.KeyPressed = new ConsoleKeyInfo();
        }

        if (_data.World.Info.TorchTicks > 0)
        {
            _data.World.Info.TorchTicks--;
            if (_data.World.Info.TorchTicks <= 0)
            {
                DrawPlayerSurroundings(stat.Location.X, stat.Location.Y, 0);
                _sounds.Queue(3, 48, 1, 32, 1, 16, 1);
            }

            if (_data.World.Info.TorchTicks % 40 == 0)
            {
                UpdateSidebar();
            }
        }

        if (_data.World.Info.EnergizerTicks > 0)
        {
            _data.World.Info.EnergizerTicks--;
            if (_data.World.Info.EnergizerTicks == 10)
            {
                _sounds.Queue(9, 32, 3, 26, 3, 23, 3, 22, 3, 21, 3, 19, 3, 16, 3);
            }
            else if (_data.World.Info.EnergizerTicks <= 0)
            {
                _data.Board.Tiles[stat.Location.X, stat.Location.Y].Color = _data.ElementDefinitions[ElementIds.Player].Color;
                BoardDrawTile(stat.Location.X, stat.Location.Y);
            }
        }

        if (_data.Board.Properties.TimeLimitInSeconds > 0 && _data.World.Info.Health > 0)
        {
            if (_sounds.HasTimeElapsed(ref _data.World.Info.TimePassedInHundredthsOfSeconds, 100))
            {
                _data.World.Info.TimePassedInSeconds++;
                if (_data.Board.Properties.TimeLimitInSeconds - 10 == _data.World.Info.TimePassedInSeconds)
                {
                    DisplayMessage(200, "Running out of time!");
                    _sounds.Queue(3, 64, 6, 69, 6, 64, 6, 53, 6, 64, 6, 69, 6, 64, 10);
                }
                else if (_data.World.Info.TimePassedInSeconds > _data.Board.Properties.TimeLimitInSeconds)
                {
                    DamageStat(0);
                }
                UpdateSidebar();
            }
        }
    }

    private void OnPushableTouch(short x, short y, short sourceStatusElementId, ref short deltaX, ref short deltaY)
    {
        PushablePush(x, y, deltaX, deltaY);
        _sounds.Queue(2, 21, 1);
    }

    private char OnPusherDraw(short x, short y)
    {
        var stat = _data.Board.StatusElements[GetStatIdAt(x, y)];

        if (stat.Step.X == 1)
        {
            return '►';
        }
        if (stat.Step.X == -1)
        {
            return '◄';
        }
        if (stat.Step.Y == -1)
        {
            return '▲';
        }
        return '▼';
    }

    private void OnPusherTick(short statusElementId)
    {
        var stat = _data.Board.StatusElements[statusElementId];
        var startX = stat.Location.X;
        var startY = stat.Location.Y;

        if (false == _data.ElementDefinitions[_data.Board.Tiles[startX + stat.Step.X, startY + stat.Step.Y].Element].Walkable)
        {
            PushablePush((short)(startX + stat.Step.X), (short)(startY + stat.Step.Y), stat.Step.X, stat.Step.Y);
        }

        statusElementId = GetStatIdAt(startX, startY);
        stat = _data.Board.StatusElements[statusElementId];
        if (_data.ElementDefinitions[_data.Board.Tiles[stat.Location.X + stat.Step.X, stat.Location.Y + stat.Step.Y].Element].Walkable)
        {
            MoveStat(statusElementId, (short)(stat.Location.X + stat.Step.X), (short)(stat.Location.Y + stat.Step.Y));
            _sounds.Queue(2, 21, 1);

            if (_data.Board.Tiles[stat.Location.X - (stat.Step.X * 2), stat.Location.Y - (stat.Step.Y * 2)].Element == ElementIds.Pusher)
            {
                var i = GetStatIdAt((short)(stat.Location.X - (stat.Step.X * 2)), (short)(stat.Location.Y - (stat.Step.Y * 2)));
                var s = _data.Board.StatusElements[i];
                if (s.Step.X == stat.Step.X && s.Step.Y == stat.Step.Y)
                {
                    _data.ElementDefinitions[ElementIds.Pusher].TickProc!(i);
                }
            }
        }

    }

    // ♣
    private void OnRuffianTick(short statusElementId)
    {
    }

    // Φ
    private void OnScrollTick(short statusElementId)
    {
    }

    // Φ
    private void OnScrollTouch(short x, short y, short sourceStatusElementId, ref short deltaX, ref short deltaY)
    {
    }

    // ^
    private void OnSharkTick(short statusElementId)
    {
    }

    // *
    private void OnSlimeTick(short statusElementId)
    {
    }

    // *
    private void OnSlimeTouch(short x, short y, short sourceStatusElementId, ref short deltaX, ref short deltaY)
    {
    }

    // ↑ → ↓ ←
    private char OnSpinningGunDraw(short x, short y)
    {
        switch (_data.CurrentTick % 8)
        {
            case 0 or 1: return '↑';
            case 2 or 3: return '→';
            case 4 or 5: return '↓';
        }

        return '←';
    }

    // ↑ → ↓ ←
    private void OnSpinningGunTick(short statusElementId)
    {
        var stat = _data.Board.StatusElements[statusElementId];
        BoardDrawTile(stat.Location.X, stat.Location.Y);
        var element = ElementIds.Bullet;
        if (stat.Parameters[1] > 0x80)
        {
            element = ElementIds.Star;
        }
        if (_random.Next(9) < stat.Parameters[1] % 0x80)
        {
            var shot = false;
            if (_random.Next(9) <= stat.Parameters[0])
            {
                shot = AbsoluteDifference(stat.Location.X, _data.Board.StatusElements[0].Location.X) <= 2
                       && BoardShoot(
                           (byte)element
                           , stat.Location.X
                           , stat.Location.Y
                           , 0
                           , Signum((short)(_data.Board.StatusElements[0].Location.Y - stat.Location.Y))
                           , Constants.ShotSourceEnemy
                        );
                if (false == shot && AbsoluteDifference(stat.Location.Y, _data.Board.StatusElements[0].Location.Y) <= 2)
                {
                    shot = BoardShoot(
                        (byte)element
                        , stat.Location.X
                        , stat.Location.Y
                        , Signum((short)(_data.Board.StatusElements[0].Location.X - stat.Location.X))
                        , 0
                        , Constants.ShotSourceEnemy
                    );
                }
            }
            else
            {
                var direction = CalcDirectionRnd();
                shot = BoardShoot(
                    (byte)element
                    , stat.Location.X
                    , stat.Location.Y
                    , direction.X
                    , direction.Y
                    , Constants.ShotSourceEnemy
                );
            }
        }
    }

    private char OnStarDraw(short x, short y)
    {
        _data.Board.Tiles[x, y].Color++;
        if (_data.Board.Tiles[x, y].Color > 15)
        {
            _data.Board.Tiles[x, y].Color = 9;
        }

        return _starAnimChars[_data.CurrentTick % 4];
    }

    // S
    private void OnStarTick(short statusElementId)
    {
    }

    // π
    private void OnTigerTick(short statusElementId)
    {
        var stat = _data.Board.StatusElements[statusElementId];
        var player = _data.Board.StatusElements[0];
        var element = (byte)ElementIds.Bullet;

        if (stat.Parameters[1] >= 0x80)
        {
            element = ElementIds.Star;
        }

        if (_random.Next(10) * 3 <= (stat.Parameters[1] % 0x80))
        {
            var shot = AbsoluteDifference(stat.Location.X, player.Location.X) <= 2
                       && BoardShoot(element, stat.Location.X, stat.Location.Y, 0, Signum((short)(player.Location.Y - stat.Location.Y)), Constants.ShotSourceEnemy);
            if (false == shot)
            {
                if (AbsoluteDifference(stat.Location.Y, player.Location.Y) <= 2)
                {
                    BoardShoot(element, stat.Location.X, stat.Location.Y, Signum((short)(player.Location.X - stat.Location.X)), 0, Constants.ShotSourceEnemy);
                }
            }
        }

        OnLionTick(statusElementId);
    }

    // ¥
    private void OnTorchTouch(short x, short y, short sourceStatusElementId, ref short deltaX, ref short deltaY)
    {
        _data.World.Info.Torches++;
        _data.Board.Tiles[x, y].Element = ElementIds.Empty;

        BoardDrawTile(x, y);
        UpdateSidebar();

        if (_data.IsMessageTorchNotShown)
        {
            DisplayMessage(200, "Torch - used for lighting in the underground.");
        }
        _data.IsMessageTorchNotShown = false;
        _sounds.Queue(3, 48, 1, 57, 1, 52, 2);
    }

    private char OnTransporterDraw(short x, short y)
    {
        return '?';
    }

    private void OnTransporterTick(short statusElementId)
    {
    }

    private void OnTransporterTouch(short x, short y, short sourceStatusElementId, ref short deltaX, ref short deltaY)
    {
    }

    // ░
    private void OnWaterTouch(short x, short y, short sourceStatusElementId, ref short deltaX, ref short deltaY)
    {
    }

    private void BoardDrawBorder()
    {
        for (short ix = 1; ix <= Constants.BoardWidth; ix++)
        {
            BoardDrawTile(ix, 1);
            BoardDrawTile(ix, Constants.BoardHeight);
        }
        for (short iy = 1; iy <= Constants.BoardHeight; iy++)
        {
            BoardDrawTile(1, iy);
            BoardDrawTile(Constants.BoardWidth, iy);
        }
    }

    private byte SidebarPromptSlider(bool editable, short x, short y, string prompt, byte currentValue)
    {
        var startChar = '1';
        var endChar = '9';
        if (prompt[^3] == ';')
        {
            startChar = prompt[^2];
            endChar = prompt[^1];
            prompt = prompt[..^3];
        }
        SidebarClearLine(y);
        _consoleInteractor.DrawText(x, y, prompt, (byte)(0x1E + (editable ? 1 : 0)));
        SidebarClearLine((short)(y + 1));
        SidebarClearLine((short)(y + 2));
        _consoleInteractor.DrawText(x, y + 2, $"{startChar}....:....{endChar}", 0x1E);

        if (editable)
        {
            do
            {
                _consoleInteractor.DrawCharacter(x + currentValue, y + 1, '▼', 0x9F);
                _input.Update();
                if (_input.KeyPressed.Key is >= ConsoleKey.D1 and <= ConsoleKey.D9)
                {
                    currentValue = (byte)(_input.KeyPressed.KeyChar - '1' + 1);
                    SidebarClearLine((short)(y + 1));
                }
                else
                {
                    var newValue = (byte)(currentValue + _input.DeltaX);
                    if (currentValue != newValue & newValue is >= 1 and <= 9)
                    {
                        currentValue = newValue;
                        SidebarClearLine((short)(y + 1));
                    }
                }
            } while (_input.KeyPressed.Key != ConsoleKey.Enter && _input.KeyPressed.Key != ConsoleKey.Escape);
        }

        _consoleInteractor.DrawCharacter(x + currentValue, y + 1, '▼', 0x1F);
        return currentValue;
    }

    private bool SidebarPromptYesNo(string message)
    {
        SidebarClearLine(3);
        SidebarClearLine(4);
        SidebarClearLine(5);
        _consoleInteractor.DrawText(63, 5, message, 0x1F);
        _consoleInteractor.DrawCharacter(63 + message.Length, 5, '_', 0x9E);

        do
        {
            _input.ReadWaitKey();
        } while (false == new [] {ConsoleKey.Escape, ConsoleKey.N, ConsoleKey.Y}.Contains(_input.KeyPressed.Key));
        SidebarClearLine(5);

        return _input.KeyPressed.Key == ConsoleKey.Y;
    }

    private void TransitionDrawBoardChange()
    {
        TransitionDrawToFill('█', 0x05);
        TransitionDrawToBoard();
    }

    private void TransitionDrawToFill(char character, byte color)
    {
        foreach (var coordinate in _data.TransitionTable)
        {
            _consoleInteractor.DrawCharacter(coordinate.X - 1, coordinate.Y - 1, character, color);
        }
    }

    private void TransitionDrawToBoard()
    {
        foreach (var coordinate in _data.TransitionTable)
        {
            BoardDrawTile(coordinate.X, coordinate.Y);
        }
    }

    private void GamePromptEndPlay()
    {
        if (_data.World.Info.Health <= 0)
        {
            _data.IsGamePlayExitRequested = true;
            BoardDrawBorder();
        }
        else
        {
            _data.IsGamePlayExitRequested = SidebarPromptYesNo("End this game? ");
            if (_input.KeyPressed.Key == ConsoleKey.Escape)
            {
                _data.IsGamePlayExitRequested = false;
            }
        }

        _input.KeyPressed = new ConsoleKeyInfo();
    }

    private void DrawPlayerSurroundings(short x, short y, short bombPhase)
    {
        for (var ix = (short)(x - Constants.TorchDistanceX - 1); ix <= x + Constants.TorchDistanceX + 1; ix++)
        {
            if (ix is >= 1 and <= Constants.BoardWidth)
            {
                for (var iy = (short)(y - Constants.TorchDistanceY - 1); iy <= y + Constants.TorchDistanceY + 1; iy++)
                {
                    if (iy is >= 1 and <= Constants.BoardHeight)
                    {
                        var tile = _data.Board.Tiles[ix, iy];
                        if (bombPhase > 0 && (Math.Sqrt(ix - x) + Math.Sqrt(iy - y) * 2) < Constants.TorchDistanceSquareRoot)
                        {
                            if (bombPhase == 1)
                            {
                                if (_data.ElementDefinitions[tile.Element].ParameterTextName.Length > 0)
                                {
                                    var iStat = GetStatIdAt(ix, iy);
                                    if (iStat > 0)
                                    {
                                        //TODO: OopSend(-iStat, "BOMBED", false);
                                    }
                                }

                                if (_data.ElementDefinitions[tile.Element].Destructible || tile.Element == ElementIds.Star)
                                {
                                    BoardDamageTile(ix, iy);
                                }

                                if (tile.Element is ElementIds.Empty or ElementIds.Breakable)
                                {
                                    tile.Element = ElementIds.Breakable;
                                    tile.Color = (byte)(0x09 + _random.Next(7));
                                    BoardDrawTile(ix, iy);
                                }
                            }
                            else
                            {
                                if (tile.Element == ElementIds.Breakable)
                                {
                                    tile.Element = ElementIds.Empty;
                                }
                            }
                        }
                        BoardDrawTile(ix, iy);
                    }
                }
            }
        }
    }

    private void HighScoresAdd(short score)
    {
        var listPos = (short)0;
        while (listPos < Constants.HighScoreCount && score < _data.HighScoreList[listPos].Score)
        {
            listPos++;
        }

        if (listPos < Constants.HighScoreCount && score > 0)
        {
            var textWindowState = new TextWindowState();
            for (var i = Constants.HighScoreCount - 2; i >= listPos; i--)
            {
                _data.HighScoreList[i + 1].Name.Text = _data.HighScoreList[i].Name.Text;
                _data.HighScoreList[i + 1].Name.TextLength = _data.HighScoreList[i].Name.TextLength;
                _data.HighScoreList[i + 1].Score = _data.HighScoreList[i].Score;
            }
            _data.HighScoreList[listPos].Score = score;
            _data.HighScoreList[listPos].Name.Text = "-- You! --";
            HighScoresInitTextWindow(ref textWindowState);
            textWindowState.LinePos = listPos;
            textWindowState.Title = $"New high score for {_data.World.Info.Name.Text}";
            _textWindow.DrawOpen(textWindowState);
            _textWindow.Draw(textWindowState, false, false);

            PopupPromptString("Congratulations!  Enter your name:", out var name);
            _data.HighScoreList[listPos].Name.Text = name;
            _editor.HighScoresSave();

            _textWindow.DrawClose();
            TransitionDrawToBoard();
            _textWindow.Free(textWindowState);
        }
    }

    private void HighScoresInitTextWindow(ref TextWindowState state)
    {
        _textWindow.InitializeState(state);
        _textWindow.Append(state, "Score  Name");
        _textWindow.Append(state, "-----  ----------------------------------");
        for (var i = 0; i < Constants.HighScoreCount; i++)
        {
            if (_data.HighScoreList[i].Name.TextLength > 0)
            {
                _textWindow.Append(state, $"{_data.HighScoreList[i].Score:D5}  {_data.HighScoreList[i].Name.Text}");
            }
        }
    }

    private void PopupPromptString(string question, out string buffer)
    {
        _consoleInteractor.DrawText(3, 18, _textWindow.Top, 0x4F);
        _consoleInteractor.DrawText(3, 19, _textWindow.Text, 0x4F);
        _consoleInteractor.DrawText(3, 20, _textWindow.Separator, 0x4F);
        _consoleInteractor.DrawText(3, 21, _textWindow.Text, 0x4F);
        _consoleInteractor.DrawText(3, 22, _textWindow.Text, 0x4F);
        _consoleInteractor.DrawText(3, 23, _textWindow.Bottom, 0x4F);
        _consoleInteractor.DrawText(4 + (_textWindow.WindowWidth - question.Length) / 2, 19, question, 0x4F);
        buffer = "";
        PromptString(10, 22, 0x4F, 0x4E, (short)(_textWindow.WindowWidth - 16), Constants.PromptAny, ref buffer);
        for (var y = 18; y <= 23; y++)
        {
            for (var x = 3; x <= _textWindow.WindowWidth + 3; x++)
            {
                BoardDrawTile((short)(x + 1), (short)(y + 1));
            }
        }
    }

    private void PromptString(short x, short y, byte arrowColor, byte color, short width, byte mode, ref string buffer)
    {
        var oldBuffer = buffer;
        var firstKeyPress = true;

        do
        {
            for (var i = 0; i <= width - 1; i++)
            {
                _consoleInteractor.DrawCharacter(x + i, y, ' ', color);
                _consoleInteractor.DrawCharacter(x + i, y - 1, ' ', arrowColor);
            }
            _consoleInteractor.DrawCharacter(x + width, y - 1, ' ', arrowColor);
            _consoleInteractor.DrawCharacter(x + buffer.Length, y - 1, '▼', (byte)((arrowColor / 0x10) * 16 + 0x0F));
            _consoleInteractor.DrawText(x, y, buffer, color);

            _input.ReadWaitKey();

            if (buffer.Length < width && (_input.KeyPressed.Key is >= ConsoleKey.Spacebar and < (ConsoleKey)128))
            {
                if (firstKeyPress)
                {
                    buffer = "";
                }

                switch (mode)
                {
                    case Constants.PromptNumeric:
                        if (_input.KeyPressed.Key is >= ConsoleKey.D0 and <= ConsoleKey.D9)
                        {
                            buffer += _input.KeyPressed.KeyChar;
                        }

                        break;
                    case Constants.PromptAny:
                        buffer += _input.KeyPressed.KeyChar;
                        break;
                    case Constants.PromptAlphanum:
                        if ("ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789_".Contains(char.ToUpperInvariant(_input.KeyPressed.KeyChar)))
                        {
                            buffer += _input.KeyPressed.KeyChar;
                        }

                        break;
                }
            }
            else if (_input.KeyPressed.Key is ConsoleKey.LeftArrow or ConsoleKey.Backspace)
            {
                buffer = buffer[..^1];
            }

            firstKeyPress = false;
        } while (_input.KeyPressed.Key is not (ConsoleKey.Enter or ConsoleKey.Escape));

        if (_input.KeyPressed.Key == ConsoleKey.Escape)
        {
            buffer = oldBuffer;
        }
    }

    /// <summary>
    /// Change &lt; 0 to -1, &gt; 0 to +1, leaves 0 as 0
    /// </summary>
    /// <param name="value"></param>
    /// <returns></returns>
    private static short Signum(short value)
    {
        return (short)(value < 0 ? -1 : value > 0 ? 1 : 0);
    }

    private void BoardAttack(short attackerStatId, short x, short y)
    {
        if (attackerStatId == 0 && _data.World.Info.EnergizerTicks > 0)
        {
            _data.World.Info.Score += _data.ElementDefinitions[_data.Board.Tiles[x, y].Element].ScoreValue;
            UpdateSidebar();
        }
        else
        {
            DamageStat(attackerStatId);
        }

        if (attackerStatId > 0 && attackerStatId <= _data.CurrentStatTicked)
        {
            _data.CurrentStatTicked++;
        }

        if (_data.Board.Tiles[x, y].Element == ElementIds.Player && _data.World.Info.EnergizerTicks > 0) {
            var attackerLocation = _data.Board.StatusElements[attackerStatId].Location;
            _data.World.Info.Score += _data.ElementDefinitions[_data.Board.Tiles[attackerLocation.X, attackerLocation.Y].Element].ScoreValue;
            UpdateSidebar();
        }
        else
        {
            BoardDamageTile(x, y);
            _sounds.Queue(2, $"{(char)16}{(char)1}");
        }
    }

    private short AbsoluteDifference(short a, short b)
    {
        return (short)Math.Abs(a - b);
    }

    private void BoardPassageTeleport(short x, short y)
    {
        var col = _data.Board.Tiles[x, y].Color;

        BoardChange(_data.Board.StatusElements[GetStatIdAt(x, y)].Parameters[2]);

        var newX = (short)0;
        var newY = (short)0;
        for (short ix = 1; ix <= Constants.BoardWidth; ix++)
        {
            for (short iy = 1; iy <= Constants.BoardHeight; iy++)
            {
                if (_data.Board.Tiles[ix, iy].Element == ElementIds.Passage && _data.Board.Tiles[ix, iy].Color == col)
                {
                    newX = ix;
                    newY = iy;
                }
            }

        }

        _data.Board.Tiles[_data.Board.StatusElements[0].Location.X, _data.Board.StatusElements[0].Location.Y].Element = ElementIds.Empty;
        _data.Board.Tiles[_data.Board.StatusElements[0].Location.X, _data.Board.StatusElements[0].Location.Y].Color = 0;
        if (newX != 0)
        {
            _data.Board.StatusElements[0].Location.X = newX;
            _data.Board.StatusElements[0].Location.Y = newY;
        }

        _data.IsGamePaused = true;
        _sounds.Queue(4, 48, 1, 52, 1, 55, 1, 49, 1, 53, 1, 56, 1, 50, 1, 54, 1, 57, 1, 51, 1, 55, 1, 58, 1, 52, 1, 56, 1, 64, 1);
        TransitionDrawBoardChange();
        BoardEnter();
    }

    private void BoardEnter()
    {
        _data.Board.Properties.PlayerEnter.X = _data.Board.StatusElements[0].Location.X;
        _data.Board.Properties.PlayerEnter.Y = _data.Board.StatusElements[0].Location.Y;

        if (_data.Board.Properties.IsDark && _data.IsMessageHintTorchNotShown)
        {
            DisplayMessage(200, "Room is dark - you need to light a torch!");
            _data.IsMessageHintTorchNotShown = false;
        }

        _data.World.Info.TimePassedInSeconds = 0;
        UpdateSidebar();
    }

    private void Move(short oldX, short oldY, short newX, short newY)
    {
        var statId = GetStatIdAt(oldX, oldY);

        if (statId >= 0)
        {
            MoveStat(statId, newX, newY);
        }
        else
        {
            _data.Board.Tiles[newX, newY].Element = _data.Board.Tiles[oldX, oldY].Element;
            _data.Board.Tiles[newX, newY].Color = _data.Board.Tiles[oldX, oldY].Color;
            BoardDrawTile(newX, newY);
            _data.Board.Tiles[oldX, oldY].Element = ElementIds.Empty;
            BoardDrawTile(oldX, oldY);
        }
    }

    private void TransporterMove(short x, short y, short deltaX, short deltaY)
    {
        var stat = _data.Board.StatusElements[GetStatIdAt((short)(x + deltaX), (short)(y + deltaY))];
        if (deltaX == stat.Step.X && deltaY == stat.Step.Y)
        {
            var ix = stat.Location.X;
            var iy = stat.Location.Y;
            var newX = (short)-1;
            var newY = (short)0;
            var finishSearch = false;
            var isValidDest = true;
            do
            {
                ix += deltaX;
                iy += deltaY;
                var tile = _data.Board.Tiles[ix, iy];
                if (tile.Element == ElementIds.BoardEdge)
                {
                    finishSearch = true;
                }
                else if (isValidDest)
                {
                    isValidDest = false;

                    if (false == _data.ElementDefinitions[tile.Element].Walkable)
                    {
                        PushablePush(ix, iy, deltaX, deltaY);
                    }

                    if (_data.ElementDefinitions[tile.Element].Walkable)
                    {
                        finishSearch = true;
                        newX = ix;
                        newY = iy;
                    }
                    else
                    {
                        newX = -1;
                    }
                }

                if (tile.Element == ElementIds.Transporter)
                {
                    var iStat = GetStatIdAt(ix, iy);
                    if (_data.Board.StatusElements[iStat].Step.X == -deltaX && _data.Board.StatusElements[iStat].Step.Y == -deltaY)
                    {
                        isValidDest = true;
                    }
                }
            } while (false == finishSearch);
            if (newX != -1)
            {
                Move((short)(stat.Location.X - deltaX), (short)(stat.Location.Y - deltaY), newX, newY);
                _sounds.Queue(3, 48, 1, 66, 1, 52, 1, 70, 1, 56, 1, 74, 1, 64, 1, 82, 1);
            }
        }
    }
}
