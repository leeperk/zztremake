﻿using ZztRemake.Interactors;

namespace ZztRemake;

public class Program
{
    private static int Main(string[] args)
    {
        var random = new Random();
        var sounds = new Sounds(random);
        using (IConsoleInteractor consoleInteractor = new ConsoleInteractor(sounds))
        {
            if (false == consoleInteractor.InitializeConsoleWindow())
            {
                return -1;
            }

            sounds.Initialize();

            if (args.Length >= 1 && args[0].ToLowerInvariant() == "/debug")
            {
                consoleInteractor.OpenDebugConsole();
            }

            var input = new Input();
            var textWindow = new TextWindow(consoleInteractor, input);
            var game = new Game(consoleInteractor, sounds, random, input, textWindow);
            game.Start(args);

            consoleInteractor.CloseDebugConsole();
            consoleInteractor.DeinitializeConsoleWindow();
        }

        return 0;
    }
}

