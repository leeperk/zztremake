namespace ZztRemake.Models;

public class Tile
{
    public byte Element { get; set; }
    public byte Color { get; set; }
}