﻿namespace ZztRemake.Models;

public static class Constants
{
    public const int MaximumStatusElements = 150;
    public const int MaximumElements = 54;
    public const int MaximumBoards = 100;
    public const int MaximumFlags = 10;
    public const int MaximumTextWindowLines = 1024;
    public const int MaximumResourceDataFiles = 24;
    public const int TextWindowAnimationSpeed = 25;
    public const int BoardWidth = 60;
    public const int BoardHeight = 25;
    public const int WorldFileHeaderSize = 512;
    public const int HighScoreCount = 30;
    public const int TorchDuration = 200;
    public const int TorchDistanceX = 8;
    public const int TorchDistanceY = 5;
    public const int TorchDistanceSquareRoot = 50;
    public const int CategoryItem = 1;
    public const int CategoryCreature = 2;
    public const int CategoryTerrain = 3;
    public const int ColorSpecialMinimum = 0xF0;
    public const int ColorChoiceOnBlack = 0xFF;
    public const int ColorWhiteOnChoice = 0xFE;
    public const int ColorChoiceOnChoice = 0xFD;
    public const int ShotSourcePlayer = 0;
    public const int ShotSourceEnemy = 1;
    public const int PromptNumeric = 0;
    public const int PromptAlphanum = 1;
    public const int PromptAny = 2;

}
