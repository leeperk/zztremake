using System.Text;

namespace ZztRemake.Models;

public class DataStatusElement
{
    public byte LocationX { get; set; }
    public byte LocationY { get; set; }
    public Int16 StepX { get; set; }
    public Int16 StepY { get; set; }
    public Int16 Cycle { get; set; }
    public byte[] Parameters { get; set; }
    public Int16 Follower { get; set; }
    public Int16 Leader { get; set; }
    public byte UnderId { get; set; }
    public byte UnderColor { get; set; }
    public Int32 Pointer { get; set; }
    public Int16 CurrentInstruction { get; set; }
    public Int16 Length { get; set; }
    public byte[] CodeBytes { get; set; }
    public string CodeText => Length >= 0 ? Encoding.Default.GetString(CodeBytes, 0, Length) : "";

    public DataStatusElement()
    {
        CodeBytes = Array.Empty<byte>();
        Parameters = new byte[3];
    }
}