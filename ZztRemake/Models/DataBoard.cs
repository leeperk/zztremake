namespace ZztRemake.Models;

public class DataBoard
{
    public Int16 Size { get; set; }
    public StringBytes Name { get; set; }
    public List<DataRunLengthEncodedTile> RunLengthEncodedTiles { get; }
    public DataBoardProperties Properties { get; }
    public List<DataStatusElement> StatusElements { get; }

    public DataBoard()
    {
        Name = new StringBytes(50);
        RunLengthEncodedTiles = new List<DataRunLengthEncodedTile>();
        Properties = new DataBoardProperties();
        StatusElements = new List<DataStatusElement>();
    }
}