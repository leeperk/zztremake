﻿namespace ZztRemake.Models;
public class EditorStatSetting
{
    public byte[] Parameters { get; set; }
    public Coordinate Step { get; set; }

    public EditorStatSetting()
    {
        Parameters = new byte[3];
        Step = new Coordinate(0, 0);
    }
}
