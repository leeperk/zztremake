﻿namespace ZztRemake.Models;

public class RegisteredTickHandler
{
    public ushort FrequencyInNumberOf20SecondTicks { get; set; }
    public Action<ushort> TickHandler { get; set; }

    public RegisteredTickHandler(ushort frequencyInNumberOf20SecondTicks, Action<ushort> tickHandler)
    {
        FrequencyInNumberOf20SecondTicks = frequencyInNumberOf20SecondTicks;
        TickHandler = tickHandler;
    }
}
