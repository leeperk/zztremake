﻿namespace ZztRemake.Models;
public class ResourceDataHeader
{
    
    public short EntryCount { get; set; }
    public StringBytes[] Name { get; set; }
    public int[] FileOffset { get; set; }

    public ResourceDataHeader()
    {
        Name = new StringBytes[Constants.MaximumResourceDataFiles];
        for (var index = 0; index < Constants.MaximumResourceDataFiles; index++)
        {
            Name[index] = new StringBytes(50);
        }
        FileOffset = new int[Constants.MaximumResourceDataFiles];
    }
}
