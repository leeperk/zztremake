namespace ZztRemake.Models;

public delegate void TouchProcType(short x, short y, short sourceStatusElementId, ref short deltaX, ref short deltaY);

public class ElementDefinition
{
    public byte Id { get; set; }
    public string Name { get; set; }
    public byte Character { get; set; }
    public byte Color { get; set; }
    public bool Destructible { get; set; }
    public bool Pushable { get; set; }
    public bool VisibleInDark { get; set; }
    public bool PlaceableOnTop { get; set; }
    public bool Walkable { get; set; }
    public bool HasGetCharacterProc { get; set; }
    public Func<short, short, char>? GetCharacterProc { get; set; }
    public short Cycle { get; set; }
    public Action<short>? TickProc { get; set; }
    public TouchProcType? TouchProc { get; set; }
    public short EditorCategory { get; set; }
    public char EditorShortcut { get; set; }
    public string CategoryName { get; set; }
    public string Parameter1Name { get; set; }
    public string Parameter2Name { get; set; }
    public string ParameterBulletTypeName { get; set; }
    public string ParameterBoardName { get; set; }
    public string ParameterDirectionName { get; set; }
    public string ParameterTextName { get; set; }
    public short ScoreValue { get; set; }

    public ElementDefinition()
    {
        Name = "";
        CategoryName = "";
        Parameter1Name = "";
        Parameter2Name = "";
        ParameterBulletTypeName = "";
        ParameterBoardName = "";
        ParameterDirectionName = "";
        ParameterTextName = "";

    }
}
