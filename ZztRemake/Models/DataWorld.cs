﻿namespace ZztRemake.Models;

public class DataWorld
{
    public DataWorldInfo Info { get; }
    public short CountOfBoardsExcludingFirstBoard { get; set; }
    public DataBoard?[] Boards { get; set; }
    public List<EditorStatSetting> EditorStatSettings { get; set; }


    public DataWorld()
    {
        Info = new DataWorldInfo();
        Boards = new DataBoard[Constants.MaximumBoards + 1];
        EditorStatSettings = new List<EditorStatSetting>();
        for (var count = 0; count < Constants.MaximumElements; count++)
        {
            EditorStatSettings.Add(new EditorStatSetting());
        }
    }
}