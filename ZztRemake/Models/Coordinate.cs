﻿namespace ZztRemake.Models;

public class Coordinate
{
    public short X { get; set; }
    public short Y { get; set; }

    public Coordinate(short x, short y)
    {
        X = x;
        Y = y;
    }
}
