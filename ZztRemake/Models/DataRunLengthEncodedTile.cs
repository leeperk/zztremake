namespace ZztRemake.Models;

public class DataRunLengthEncodedTile
{
    public byte Count { get; set; }
    public Tile Tile { get; set; }

    public DataRunLengthEncodedTile()
    {
        Tile = new Tile();
    }
}