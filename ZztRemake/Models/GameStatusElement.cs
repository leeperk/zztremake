using Newtonsoft.Json;

namespace ZztRemake.Models;

public class GameStatusElement
{
    public Coordinate Location { get; set; }
    public Coordinate Step { get; set; }
    public short Cycle { get; set; }
    public byte[] Parameters { get; set; }
    public short Follower { get; set; }
    public short Leader { get; set; }
    public Tile UnderTile { get; set; }
    public short CodePosition;
    public string Code { get; set; }

    public GameStatusElement()
    {
        Location = new Coordinate(0, 0);
        Step = new Coordinate(0, 0);
        Parameters = new byte[] { 0, 0, 0 };
        UnderTile = new Tile();
        CodePosition = 0;
        Code = "";
    }

    public override string ToString()
    {
        return JsonConvert.SerializeObject(this, Formatting.Indented);
    }

    public GameStatusElement Clone()
    {
        return new GameStatusElement()
        {
            Location = new Coordinate(Location.X, Location.Y)
            , Step = new Coordinate(Step.X, Step.Y)
            , Cycle = Cycle
            , Parameters = new[] { Parameters[0], Parameters[1], Parameters[2] }
            , Follower = Follower
            , Leader = Leader
            , UnderTile = new Tile() { Element = UnderTile.Element, Color = UnderTile.Color }
            , CodePosition = CodePosition
            , Code = Code
        };
    }

}