namespace ZztRemake.Models;

public class GameBoardProperties
{
    public byte MaxShots { get; set; }
    public bool IsDark { get; set; }
    public byte[] NeighborBoards { get; set; }
    public bool ShouldRestartOnZap { get; set; }
    public string Message { get; set; }
    public Coordinate PlayerEnter { get; set; }
    public short TimeLimitInSeconds { get; set; }

    public GameBoardProperties()
    {
        NeighborBoards = new byte[4];
        Message = "";
        PlayerEnter = new Coordinate(0, 0);
    }
}