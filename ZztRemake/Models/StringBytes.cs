using System.Text;

namespace ZztRemake.Models;

public class StringBytes
{
    public byte TextLength { get; set; }
    public byte[] Bytes { get; set; }

    public string Text
    {
        get => Encoding.Default.GetString(Bytes, 0, TextLength);
        set
        {
            Bytes = Encoding.Default.GetBytes(value);
            TextLength = (byte)value.Length;
        }
    }

    public StringBytes(byte size)
    {
        Bytes = new byte[size];
    }
}