namespace ZztRemake.Models;

public class DataWorldInfo
{
    public short WorldType { get; set; }
    public short Ammo;
    public short Gems;
    public short Health;
    public short Score;
    public short Torches;
    public short TimePassedInSeconds;
    public short TimePassedInHundredthsOfSeconds;
    public bool[] Keys { get; set; }
    public short CurrentBoard { get; set; }
    public short TorchTicks { get; set; }
    public short EnergizerTicks { get; set; }
    public StringBytes Name { get; }
    public StringBytes[] Flags { get; }
    public bool IsSave { get; set; }

    public DataWorldInfo()
    {
        Keys = new bool[8];
        Name = new StringBytes(20);
        Flags = new StringBytes[Constants.MaximumFlags];
        for (var index = 0; index < Constants.MaximumFlags; index++)
        {
            Flags[index] = new StringBytes(20);
        }
    }

}