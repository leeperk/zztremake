namespace ZztRemake.Models;

public class GameBoard
{
    public string Name { get; set; }
    public Tile[,] Tiles { get; set; }
    public GameBoardProperties Properties { get; set; }
    public List<GameStatusElement> StatusElements { get; set; }

    public GameBoard()
    {
        Name = "";
        Tiles = new Tile[62, 27];
        Properties = new GameBoardProperties();
        StatusElements = new List<GameStatusElement>();
    }
	
}