﻿using ZztRemake.Interactors;

namespace ZztRemake.Models;

public class BlinkCharacter
{
    public short X { get; set; }
    public short Y { get; set; }
    public char Character { get; set; }
    public byte Color { get; set; }

    public BlinkCharacter(short x, short y, char character, byte color)
    {
        X = x;
        Y = y;
        
        Character = character;
        Color = color;
    }
}
