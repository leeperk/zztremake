﻿namespace ZztRemake.Models;

public class ZztData
{
    public Coordinate[] TransitionTable { get; set; }
    public string LoadedGameFileName { get; set; }
    public string SavedGameFileName { get; set; }
    public string SavedBoardFileName { get; set; }
    public string StartupWorldFileName { get; set; }
    public DataWorld World { get; set; }
    public GameBoard Board { get; set; }
    public Coordinate PlayerDirection { get; set; }
    public bool IsMessageAmmoNotShown { get; set; }
    public bool IsMessageOutOfAmmoNotShown { get; set; }
    public bool IsMessageNoShootingNotShown { get; set; }
    public bool IsMessageTorchNotShown { get; set; }
    public bool IsMessageOutOfTorchesNotShown { get; set; }
    public bool IsMessageRoomNotDarkNotShown { get; set; }
    public bool IsMessageHintTorchNotShown { get; set; }
    public bool IsMessageForestNotShown { get; set; }
    public bool IsMessageFakeNotShown { get; set; }
    public bool IsMessageGemNotShown { get; set; }
    public bool IsMessageEnergizerNotShown { get; set; }
    public bool IsGameTitleExitRequested { get; set; }
    public bool IsGamePlayExitRequested { get; set; }
    public short GameStateElement { get; set; }
    public short ReturnBoardId { get; set; }
    public byte TickSpeed { get; set; }
    public List<ElementDefinition> ElementDefinitions { get; set; }
    public List<byte> EditorPatterns { get; set; }
    public short TickTimeDuration { get; set; }
    public short CurrentTick { get; set; }
    public short CurrentStatTicked { get; set; }
    public bool IsGamePaused { get; set; }
    public short TickTimeCounter;
    public bool ShouldForceDarknessOff { get; set; }
    public char OopChar { get; set; }
    public string OopWord { get; set; }
    public short OopValue { get; set; }
    public bool DebugEnabled { get; set; }
    public HighScoreEntry[] HighScoreList { get; set; }
    public string ConfigRegistration { get; set; }
    public string ConfigWorldFile { get; set; }
    public bool IsEditorEnabled { get; set; }
    public string GameVersion { get; set; }
    public bool DidJustStart { get; set; }
    public Dictionary<string, string> WorldFileDescriptions { get; set; }
    public GameStatusElement StatusElementTemplateDefault { get; set; }

    public ZztData()
    {
        LoadedGameFileName = "";
        SavedGameFileName = "";
        SavedBoardFileName = "";
        StartupWorldFileName = "";
        TransitionTable = new Coordinate[Constants.BoardWidth * Constants.BoardHeight];
        for (var index = 0; index < TransitionTable.Length; index++)
        {
            TransitionTable[index] = new Coordinate(0, 0);
        }
        World = new DataWorld();
        Board = new GameBoard();
        PlayerDirection = new Coordinate(0, 0);
        ElementDefinitions = new List<ElementDefinition>();
        EditorPatterns = new List<byte>();
        OopWord = "";
        HighScoreList = new HighScoreEntry[Constants.HighScoreCount];
        for (var index = 0; index < Constants.HighScoreCount; index++)
        {
            HighScoreList[index] = new HighScoreEntry();
        }
        ConfigRegistration = "";
        ConfigWorldFile = "";
        GameVersion = "";
        WorldFileDescriptions = new Dictionary<string, string>();
        StatusElementTemplateDefault = new GameStatusElement()
        {
            Location = new Coordinate(0, 0)
            , Step = new Coordinate(0, 0)
            , Cycle = 0
            , Parameters = new byte[] { 0, 0, 0 }
            , Follower = -1
            , Leader = -1
        };
    }
}
