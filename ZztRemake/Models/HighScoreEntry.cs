﻿namespace ZztRemake.Models;

public class HighScoreEntry
{
    public StringBytes Name { get; set; }
    public short Score { get; set; }

    public HighScoreEntry()
    {
        Name = new StringBytes(0);
    }
}
