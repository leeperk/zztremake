namespace ZztRemake.Models;

public class DataBoardProperties
{
    public byte MaxPlayShots { get; set; }
    public bool IsDark { get; set; }
    public byte[] NeighborBoards { get; set; }
    public bool RestartOnZap { get; set; }
    public StringBytes Message { get; set; }
    public Coordinate PlayerEnter { get; set; }
    public short TimeLimit { get; set; }
    public short StatElementCount { get; set; }

    public DataBoardProperties()
    {
        NeighborBoards = new byte[4];
        Message = new StringBytes(58);
        PlayerEnter = new Coordinate(0, 0);
    }
}