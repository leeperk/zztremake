﻿namespace ZztRemake.Models;

public class TextWindowState
{
    public bool Selectable { get; set; }
    public short LinePos { get; set; }
    public List<string> Lines { get; set; }
    public string Hyperlink { get; set; }
    public string Title { get; set; }
    public string LoadedFilename { get; set; }
    public string[] ScreenCopy { get; set; }

    public TextWindowState()
    {
        Lines = new List<string>();
        Hyperlink = "";
        Title = "";
        LoadedFilename = "";
        ScreenCopy = new string[25];
    }
}
