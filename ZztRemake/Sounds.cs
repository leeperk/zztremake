﻿using System.Text;
using NAudio.Wave;
using NAudio.Wave.SampleProviders;
using ZztRemake.Interactors;
using ZztRemake.Models;

namespace ZztRemake;

public class Sounds : IDisposable
{
    private readonly Random _random;
    private bool _disposed;
    private short _currentPriority;
    private readonly ushort[] _frequencyTable;
    private byte _durationMultiplier;
    private byte _durationCounter;
    private string _buffer;
    private short _bufferPos;
    private ushort _timer20TimesPerSecondTicks;
    private readonly ushort[][] _drumTable;
    private readonly WaveOutEvent _waveOutEvent;
    private readonly SignalGenerator _signalGenerator;
    private readonly HighPrecisionTimer _timer;
    private readonly List<RegisteredTickHandler> _registeredTickHandlers;

    public bool IsEnabled { get; set; }
    public bool BlockQueueing { get; set; }
    public bool IsPlaying { get; set; }

    public Sounds(Random random)
    {
        _random = random;
        _disposed = false;
        _frequencyTable = new ushort[256];
        _drumTable = new ushort[10][];
        for (var index = 0; index <= 9; index++)
        {
            _drumTable[index] = new ushort[256];
        }

        _buffer = "";
        InitializeFrequencyTable();
        InitializeDrumTable();
        IsEnabled = true;
        BlockQueueing = false;
        _durationMultiplier = 1;
        IsPlaying = false;
        _waveOutEvent = new WaveOutEvent();
        _signalGenerator = new SignalGenerator()
        {
            Gain = 0.15
            , Type = SignalGeneratorType.Square
        };
        _waveOutEvent.Init(_signalGenerator);
        ClearQueue();
        _timer20TimesPerSecondTicks = 0;
        _registeredTickHandlers = new List<RegisteredTickHandler>();
        _timer = new HighPrecisionTimer() { Interval = 50 };  // 50 milliseconds == 1000/50 times per second == 20 times per second
    }

    ~Sounds() => Dispose();

    protected virtual void Dispose(bool disposing)
    {
        if (_disposed)
        {
            return;
        }

        if (disposing)
        {
            _timer.Stop();
            _timer.Dispose();
            _waveOutEvent.Stop();
            _waveOutEvent.Dispose();
        }

        _disposed = true;
    }

    public void Dispose()
    {
        Dispose(true);
        GC.SuppressFinalize(this);
    }

    public void Initialize()
    {
        _timer.Elapsed += TimerHandler;
        _timer.Start();
    }

    public bool HasTimeElapsed(ref short counter, short duration)
    {
        // 20 times per second ticks * 5 == (20 * 5) times per second ticks == 100 times per second ticks
        var oneHundredTimesPerSecondTicks = (short)(_timer20TimesPerSecondTicks * 5);
        var oneHundredTimesPerSecondDifference = (ushort)(oneHundredTimesPerSecondTicks - counter);

        if (oneHundredTimesPerSecondDifference >= duration)
        {
            counter = oneHundredTimesPerSecondTicks;
            return true;
        }

        return false;

    }

    public void RegisterTickHandler(ushort frequencyInNumberOf20SecondTicks, Action<ushort> tickHandler)
    {
        _registeredTickHandlers.Add(new RegisteredTickHandler(frequencyInNumberOf20SecondTicks, tickHandler));
    }

    public void Queue(short priority, string pattern)
    {
        if (false == BlockQueueing
            && (false == IsPlaying
                || (priority >= _currentPriority && _currentPriority != -1)
                || priority == -1)
           )
        {
            if (priority >= 0 || false == IsPlaying)
            {
                _currentPriority = priority;
                _buffer = pattern;
                _bufferPos = 0;
                _durationCounter = 1;
            }
            else
            {
                _buffer = _buffer[_bufferPos..];
                _bufferPos = 0;
                if (_buffer.Length + pattern.Length < 255)
                {
                    _buffer = $"{_buffer}{pattern}";
                }
            }
            IsPlaying = true;
        }
    }

    public void Queue(short priority, params byte[] values)
    {
        var builder = new StringBuilder("");
        Queue(priority, string.Join("", values.Select(v => (char)v)));
    }

    public string CreatePattern(params byte[] values)
    {
        var builder = new StringBuilder("");
        foreach (var b in values)
        {
            builder.Append((char)b);
        }
        return builder.ToString();
    }

    public void ClearQueue()
    {
        _buffer = "";
        IsPlaying = false;
        _waveOutEvent.Stop();
    }

    public void Sound(byte frequency)
    {
        _signalGenerator.Frequency = frequency;
        _waveOutEvent.Play();
    }

    public void NoSound()
    {
        _waveOutEvent.Stop();
    }

    public string Parse(string input)
    {
        var advanceInput = () =>
        {
            input = input[1..];
        };
        var output = new StringBuilder("");
        var noteOctave = 3;
        var noteDuration = 1;

        while (input.Length > 0)
        {
            var noteTone = -1;
            switch (char.ToUpperInvariant(input[0]))
            {
                case 'T':
                    noteDuration = 1;
                    advanceInput();
                    break;
                case 'S':
                    noteDuration = 2;
                    advanceInput();
                    break;
                case 'I':
                    noteDuration = 4;
                    advanceInput();
                    break;
                case 'Q':
                    noteDuration = 8;
                    advanceInput();
                    break;
                case 'H':
                    noteDuration = 16;
                    advanceInput();
                    break;
                case 'W':
                    noteDuration = 32;
                    advanceInput();
                    break;
                case '.':
                    noteDuration = (noteDuration * 3) / 2;
                    advanceInput();
                    break;
                case '3':
                    noteDuration /= 3;
                    advanceInput();
                    break;
                case '+':
                    if (noteOctave < 6)
                    {
                        noteOctave++;
                    }
                    advanceInput();
                    break;
                case '-':
                    if (noteOctave > 1)
                    {
                        noteOctave--;
                    }
                    advanceInput();
                    break;
                case >= 'A' and <= 'G':
                    switch (char.ToUpperInvariant(input[0]))
                    {
                        case 'C':
                            noteTone = 0;
                            break;
                        case 'D':
                            noteTone = 2;
                            break;
                        case 'E':
                            noteTone = 4;
                            break;
                        case 'F':
                            noteTone = 5;
                            break;
                        case 'G':
                            noteTone = 7;
                            break;
                        case 'A':
                            noteTone = 9;
                            break;
                        case 'B':
                            noteTone = 11;
                            break;
                    }
                    advanceInput();
                    if (input.Length > 0)
                    {
                        switch (char.ToUpperInvariant(input[0]))
                        {
                            case '!':
                                noteTone--;
                                break;
                            case '#':
                                noteTone++;
                                break;
                        }
                    }

                    output.Append((char)(noteOctave * 0x10 + noteTone))
                        .Append((char)noteDuration);
                    break;
                case 'X':
                    output.Append('\0').Append((char)noteDuration);
                    advanceInput();
                    break;
                case >= '0' and <= '9':
                    output.Append((char)(input[0] + 0xF0 - '0'))
                        .Append((char)noteDuration);
                    advanceInput();
                    break;
                default:
                    advanceInput();
                    break;
            }
        }

        return output.ToString();
    }

    private void TimerHandler(object? sender, EventArgs ev)
    {
        _timer20TimesPerSecondTicks++;
        foreach (var registeredTickHandler in _registeredTickHandlers)
        {
            if (_timer20TimesPerSecondTicks % registeredTickHandler.FrequencyInNumberOf20SecondTicks == 0)
            {
                registeredTickHandler.TickHandler(_timer20TimesPerSecondTicks);
            }
        }

        if (false == IsEnabled)
        {
            IsPlaying = false;
            _waveOutEvent.Stop();
        }
        else if (IsPlaying)
        {
            _durationCounter--;
            if (_durationCounter <= 0)
            {
                _waveOutEvent.Stop();
                if (_bufferPos >= _buffer.Length)
                {
                    _waveOutEvent.Stop();
                    IsPlaying = false;
                }
                else
                {
                    if (_buffer[_bufferPos] == 0)
                    {
                        _waveOutEvent.Stop();
                    }
                    else if (_buffer[_bufferPos] < 240)
                    {
                        _signalGenerator.Frequency = _frequencyTable[_buffer[_bufferPos]];
                        _waveOutEvent.Play();
                    }
                    else
                    {
                        PlayDrum(_drumTable[_buffer[_bufferPos] - 240]);
                    }

                    _bufferPos++;
                    _durationCounter = (byte)(_durationMultiplier * _buffer[_bufferPos]);
                    _bufferPos++;
                }
            }
        }
    }

    private void PlayDrum(ushort[] drum)
    {
        var drumIndex = 0;
        while (drumIndex < drum.Length)
        {
            _signalGenerator.Frequency = drum[drumIndex];
            _waveOutEvent.Stop();
            _waveOutEvent.Play();
            HighPrecisionTimer.Delay(1);
            drumIndex++;
        }
        _waveOutEvent.Stop();
    }

    private void InitializeFrequencyTable()
    {
        var freqC1 = 32.0d;
        var ln2 = Math.Log(2.0);
        var noteStep = Math.Exp(ln2 / 12.0);

        for (var octave = 1; octave <= 15; octave++)
        {
            var noteBase = Math.Exp(octave * ln2) * freqC1;
            for (var note = 0; note <= 11; note++)
            {
                _frequencyTable[octave * 16 + note] = (ushort)Math.Truncate(noteBase);
                noteBase *= noteStep;
            }
        }
    }

    private void InitializeDrumTable()
    {
        _drumTable[0] = new ushort[] { 3200 };
        for (var i = 1; i <= 9; i++)
        {
            _drumTable[i] = new ushort[15];
        }
        for (var i = 1; i <= 14; i++)
        {
            _drumTable[1][i] = (ushort)(i * 100 + 1000);
            _drumTable[2][i] = (ushort)((i % 2) * 1600 + 1600 + (i % 4) * 1600);
            _drumTable[4][i] = (ushort)(_random.Next(5000) + 500);
            if (i <= 7)
            {
                _drumTable[5][i * 2 - 1] = 1600;
                _drumTable[5][i * 2] = (ushort)(_random.Next(1600) + 800);
            }
            _drumTable[6][i] = (ushort)(((i % 2) * 880) +880 + ((i % 3) * 440));
            _drumTable[7][i] = (ushort)(700 - (i * 12));
            _drumTable[8][i] = (ushort)((i * 20 + 1200) - _random.Next(i * 40));
            _drumTable[9][i] = (ushort)(_random.Next(440) + 220);
        }
    }
}
