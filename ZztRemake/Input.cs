﻿namespace ZztRemake;

public class Input
{
    public short DeltaX;
    public short DeltaY;
    public ConsoleKeyInfo KeyPressed { get; set; }

    private readonly List<ConsoleKeyInfo> _keyBuffer;

    public Input()
    {
        _keyBuffer = new List<ConsoleKeyInfo>();
    }

    public void Update()
    {
        DeltaX = 0;
        DeltaY = 0;

        if (Console.KeyAvailable)
        {
            _keyBuffer.Add(Console.ReadKey(true));
            while (Console.KeyAvailable)
            {
                Console.ReadKey(true);
            }        
        }
        if (_keyBuffer.Count > 0)
        {
            KeyPressed = _keyBuffer[0];
            _keyBuffer.RemoveAt(0);

            switch (KeyPressed.Key)
            {
                case ConsoleKey.UpArrow or ConsoleKey.D8 or ConsoleKey.NumPad8:
                    DeltaX  = 0;
                    DeltaY = -1;
                    break;
                case ConsoleKey.LeftArrow or ConsoleKey.D4 or ConsoleKey.NumPad4:
                    DeltaX = -1;
                    DeltaY = 0;
                    break;
                case ConsoleKey.RightArrow or ConsoleKey.D6 or ConsoleKey.NumPad6:
                    DeltaX = 1;
                    DeltaY = 0;
                    break;
                case ConsoleKey.DownArrow or ConsoleKey.D2 or ConsoleKey.NumPad2:
                    DeltaX = 0;
                    DeltaY = 1;
                    break;
            }
        }
        else
        {
            KeyPressed = new ConsoleKeyInfo();
        }
    }

    public void ReadWaitKey()
    {
        do
        {
            Update();
        } while (KeyPressed.Key == 0);
    }

    public bool WasShiftPressedWithLastKey() => (KeyPressed.Modifiers & ConsoleModifiers.Shift) > 0;
}

