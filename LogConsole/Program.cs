﻿using System.Diagnostics;
using System.IO.Pipes;
using System.Reflection;
using System.Text;

namespace LogConsole;

public class Program
{
    private static int Main(string[] args)
    {
        using (var pipe = new NamedPipeServerStream("ZztLog", PipeDirection.InOut, 1, PipeTransmissionMode.Message))
        {
            Console.WriteLine("Waiting for client to connect...");
            pipe.WaitForConnection();
            Console.WriteLine("Client connected.");
            var message = getMessageFromPipe(pipe);
            while (message.ToLowerInvariant() != "quit" && pipe.IsConnected)
            {
                Console.WriteLine(message);
                message = getMessageFromPipe(pipe);
            }
            Console.WriteLine("Server closing.");
        }

        return 0;
    }

    private static string getMessageFromPipe(NamedPipeServerStream pipe)
    {
        var builder = new StringBuilder();
        var buffer = new byte[5];
        do
        {
            var bytesRead = pipe.Read(buffer, 0, buffer.Length);
            var chunk = Encoding.UTF8.GetString(buffer.Take(bytesRead).ToArray());
            builder.Append(chunk);
            buffer = new byte[buffer.Length];
        } while (false == pipe.IsMessageComplete);

        return builder.ToString();
    }
}
